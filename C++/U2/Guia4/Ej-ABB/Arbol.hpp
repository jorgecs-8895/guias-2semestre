/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Arbol.- ********************/

/*
* file Arbol.hpp
*/

//* Arbol.hpp - Declaración de la clase Arbol.

#ifndef ARBOL_HPP
#define ARBOL_HPP

// Llamar e incluir librerías necesarias
#include <cstdlib>	// <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <fstream>
#include <iostream>
#include <cstdio>

using namespace std;


// Estructura del(los) nodo(s).
typedef struct _NodoArbol{
	int datoArbol;
	struct _NodoArbol *saDerecho;
	struct _NodoArbol *saIzqdo;
	struct _NodoArbol *nodoPadre;
} Nodo;

// Clase Arbol: Genera árbol binario de búsqueda y sus métodos (funciones).
class Arbol{
	// Miembros privados
	private:    // Atributos
		// bool isBusqueda{false};
		int valorRaiz;
		int contador{0};
		Nodo* binaryTree{nullptr};
		Nodo* minValue{};
		Nodo* nodoNew{nullptr};


	// Miembros públicos
	public:    // Constructores - Destructores y Métodos
	// Constructores - Destructores
		Arbol();   // Constructor por defecto
		~Arbol();  // Destructor
	// Métodos
		Nodo* generarNodoArbol(int valArbol, Nodo *nodoPadre);
		void ingresarDato(Nodo *&binaryTree, int valArbol, Nodo* nodoPadre);
		void eliminarDato(Nodo *binaryTree, int valArbol);
		void cambiarNodo(Nodo *binaryTree, Nodo* nodoNew);
		void destructorNodo(Nodo* nodo);
		void eliminarNodoArbol(Nodo *eliminarDato);
		Nodo* getMinimo(Nodo* binaryTree);
		// void modificarDato(Nodo *&binaryTree, int valArbol);
		bool buscarDato(Nodo *binaryTree, int valArbol);
		void mostrarArbol(Nodo *binaryTree, int contador);
		void imprimirPreorden(Nodo *binaryTree);
		void imprimirInorden(Nodo *binaryTree);
		void imprimirPostorden(Nodo *binaryTree);
		void recorrerArbol(Nodo *binaryTree, ofstream &files);
		void generarGrafo(Nodo *binaryTree);
};

#endif
