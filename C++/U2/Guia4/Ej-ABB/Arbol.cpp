/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Arbol.- ********************/

/*
* file Arbol.cpp
*/

//* Arbol.cpp - Definición de los métodos de la clase Arbol.

// Llamar e incluir librerías necesarias
#include <cstdlib> 	// <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <fstream>
#include <iostream>
#include <cstdio>

using namespace std;
#include "Arbol.hpp"


// Prototipos de funciones
Nodo* generarNodoArbol(int , Nodo*);
void ingresarDato(Nodo *&, int , Nodo*);
void eliminarDato(Nodo*, int );
void cambiarNodo(Nodo*, Nodo*);
void destructorNodo(Nodo*);
void eliminarNodoArbol(Nodo*);
Nodo* getMinimo(Nodo*);
// void modificarDato(Nodo *&, int );
bool buscarDato(Nodo*, int );
void mostrarArbol(Nodo*, int );
void imprimirPreorden(Nodo*);
void imprimirInorden(Nodo*);
void imprimirPostorden(Nodo*);
void recorrerArbol(Nodo*, ofstream &);
void generarGrafo(Nodo*);


// Método de Constructores - Destructores.
Arbol::Arbol(){
	// bool isBusqueda{false};
	int valorRaiz;
	int contador{0};
	Nodo* binaryTree{nullptr};
	Nodo* minValue{};
	Nodo* nodoNew{nullptr};

};

Arbol::~Arbol(){
	// bool isBusqueda{false};
	int valorRaiz;
	int contador{0};
	delete binaryTree;
	delete minValue;
	delete nodoNew;
};

// Método que genera y crea un árbol binario.
Nodo* Arbol::generarNodoArbol(int valArbol, Nodo *nodoPadre){
	Nodo *tmp;
	tmp = new Nodo();

	tmp->datoArbol = valArbol;
	tmp->saIzqdo = tmp->saDerecho = nullptr;
	tmp->nodoPadre = nodoPadre;
	return tmp;
};

// Método que permite el ingreso de datos numéricos al árbol.
void Arbol::ingresarDato(Nodo *&binaryTree, int valArbol, Nodo *nodoPadre){
	if (binaryTree == nullptr) {	// Si el arbol está vacío, entonces el primer valor ingresado será el padre
		Nodo *tmp;
		tmp = generarNodoArbol(valArbol, nodoPadre);
		binaryTree = tmp;
		//! 'DEPRECATED' cout << "\n\t----- Se ha insertado el número:  < " << valArbol << " >  como padre " << endl;
	} else {						// De lo contrario, el programa rellenará los subarboles según corresponda
		valorRaiz = binaryTree->datoArbol;		// La comparación de valoRaiz determina en que lado del arbol se introducirá el dato
		if (valArbol > valorRaiz) {
			ingresarDato(binaryTree->saDerecho, valArbol, binaryTree);
		} else if (valArbol < valorRaiz) {
			ingresarDato(binaryTree->saIzqdo, valArbol, binaryTree);
		} else {	// Controla y evita la duplicación de datos ingresados por el usuario
			cout << "\n\t Error, el valor  « " << valorRaiz << " »  ya existe en el árbol. Favor, intente nuevamente. " << endl;
		}
	};
};


// Método que elimina dato en el árbol.
void Arbol::eliminarDato(Nodo *binaryTree, int valArbol) {
	Nodo* tmp;
	tmp = nullptr;

	if (binaryTree == nullptr) { // Si el arbol está vacio, entonces devuelve nullptr
		return;
	} else if (binaryTree->datoArbol > valArbol) {	// Si valArbol es menor al datoArbol, entonces omite el subarbol derecho y se elimina el dato del subarbol izquierdo.
		eliminarDato(binaryTree->saIzqdo, valArbol);
	} else if (binaryTree->datoArbol < valArbol) {	// De lo contrario, omite el subarbol izquierdo y elimina el dato del subarbol derecho.
		eliminarDato(binaryTree->saDerecho, valArbol);
	} else {
		eliminarNodoArbol(binaryTree);
	}
};

// Método que reemplaza los nodos post eliminación de datos
void Arbol::cambiarNodo(Nodo *binaryTree, Nodo* nodoNew){
	if (binaryTree->nodoPadre) { // Generar sustituciones de datos en cualquier posibilidad
		if (binaryTree->datoArbol == binaryTree->nodoPadre->saIzqdo->datoArbol){ // Si el dato está en el subarbol izquierdo
			binaryTree->nodoPadre->saIzqdo = nodoNew;
		} else if (binaryTree->datoArbol == binaryTree->nodoPadre->saDerecho->datoArbol){ // Si el dato está en el subarbol derecho
			binaryTree->nodoPadre->saDerecho = nodoNew;
		}
	} if (nodoNew) {
		nodoNew->nodoPadre = binaryTree->nodoPadre;
	}
};

// Método que destruye el nodo.
void Arbol::destructorNodo(Nodo *nodo){
	nodo->saIzqdo = nodo->saDerecho = nullptr;
	delete nodo;
};

// Método que elimina nodo del árbol.
void Arbol::eliminarNodoArbol(Nodo *eliminarDato) {
	if (eliminarDato->saIzqdo && eliminarDato->saDerecho) {		// Evaluar existencia de hijos
		minValue = getMinimo(eliminarDato->saDerecho);			// El nodo minValue captura el menos valor entre los datos que están en subarbol derecho.
		eliminarDato->datoArbol = minValue->datoArbol;
		eliminarNodoArbol(minValue);
	} else if (eliminarDato->saIzqdo){							// Solo si tiene dato en hijo izquierdo
		cambiarNodo(eliminarDato, eliminarDato->saIzqdo);
		destructorNodo(eliminarDato);
	} else if (eliminarDato->saDerecho){						// Solo si tienen dato en hijo derecho
		cambiarNodo(eliminarDato, eliminarDato->saDerecho);
		destructorNodo(eliminarDato);
	} else { 													// Si no tiene datos/hijos
		cambiarNodo(eliminarDato, nullptr);
		destructorNodo(eliminarDato);
	}
};

/* //TODO Método que modifica valor de un dato y reemplaza por otro.
void Arbol::modificarDato(Nodo *&binaryTree, int valArbol) {

}; */

// Obtiene el menor valor mínimo dentro del Arbol
Nodo* Arbol::getMinimo(Nodo *binaryTree) {
	if (binaryTree == nullptr) { 	// No retorna nada si el arbol está vacio.
		return nullptr;
	} if (binaryTree->saIzqdo){		// Si hay valores en el subarbol izquierdo.
		return getMinimo(binaryTree->saIzqdo);		// Obtiene el menor valor
	} else {						// De lo contrario, retorna el arbol.
		return binaryTree;
	}
};

// Método que busca el dato específico en el árbol - FORMA NODO.
//! DEPRECATED
/* Nodo* Arbol::buscarDato(Nodo *binaryTree, int valArbol) {
	if (binaryTree == nullptr){		// No retorna nada si el arbol está vacio.
		return nullptr;
	} else if (binaryTree->datoArbol > valArbol) {		// Evalua la busqueda en los subarboles dependiendo de las condiciones
		return buscarDato(binaryTree->saDerecho, valArbol); // Retorna busqueda de datos en el subarbol derecho
	} else if (binaryTree->datoArbol < valArbol) {
		return buscarDato(binaryTree->saIzqdo, valArbol);	// Retorna busqueda de datos en el subarbol izquierdo
	} else {						// De lo contrario, retorna el arbol
		return binaryTree;
	}
}; */


// Método que busca el dato específico en el árbol - FORMA BOOLEANA.
bool Arbol::buscarDato(Nodo *binaryTree, int valArbol){
	if (binaryTree == nullptr) {
		return false;
	} else if (binaryTree->datoArbol == valArbol) {
		return true;
	} else if (binaryTree->datoArbol < valArbol){
		return buscarDato(binaryTree->saDerecho, valArbol);
	} else {
		return buscarDato(binaryTree->saIzqdo, valArbol);
	}
};


// Método que genera visualización del Árbol de manera vertical
void Arbol::mostrarArbol(Nodo *binaryTree, int contador) {
	// Variables banderas
	int i{0};

	if (binaryTree == nullptr) { // Si el arbol está vacio, no muestra nada
		return;
	} else {					// De lo contrario, genera un mapa.
		mostrarArbol(binaryTree->saDerecho, contador +1);
		for (i=0; i<contador; i++) {
			cout << "    ";
		} cout << binaryTree->datoArbol << endl;
		mostrarArbol(binaryTree->saIzqdo, contador +1);
	};
};

// Métodos que generan visualizaciones de los datos en Preorden, Inorden y Posorden.
	/* PREORDEN */
void Arbol::imprimirPreorden(Nodo *binaryTree) {
	if (binaryTree != nullptr) {
		cout << binaryTree->datoArbol << " - ";
		imprimirPreorden(binaryTree->saIzqdo);
		imprimirPreorden(binaryTree->saDerecho);
	};
};

	/* INORDEN */
void Arbol::imprimirInorden(Nodo *binaryTree) {
	if (binaryTree != nullptr) {
		imprimirInorden(binaryTree->saIzqdo);
		cout << binaryTree->datoArbol << " - ";
		imprimirInorden(binaryTree->saDerecho);
	};
};

	/* POSORDEN */
void Arbol::imprimirPostorden(Nodo *binaryTree) {
	if (binaryTree != nullptr) {
		imprimirPostorden(binaryTree->saIzqdo);
		imprimirPostorden(binaryTree->saDerecho);
		cout << binaryTree->datoArbol << " - ";
	};
};

// Método que recorre el árbol binario
void Arbol::recorrerArbol(Nodo *binaryTree, ofstream &files) {
	if (binaryTree != nullptr) {
		if (binaryTree->saIzqdo != nullptr) {
			files << "\n" << binaryTree->datoArbol << "->" << binaryTree->saIzqdo->datoArbol << ";";
		} else {
			files << "\n" << '"' << binaryTree->datoArbol << "i" << '"' << " [shape=point];";
			files << "\n" << binaryTree->datoArbol << "->" << '"' << binaryTree->datoArbol << "i" << '"';
		}

		if (binaryTree->saDerecho != nullptr) {
			files << "\n" << binaryTree->datoArbol << "->" << binaryTree->saDerecho->datoArbol << ";";
		} else {
			files << "\n" << '"' << binaryTree->datoArbol << "d" << '"' << " [shape=point];";
			files << "\n" << binaryTree->datoArbol << "->" << '"' << binaryTree->datoArbol << "d" << '"';
		}
		recorrerArbol(binaryTree->saIzqdo, files);
		recorrerArbol(binaryTree->saDerecho, files);
	}
};

// Método que genera grafo de la estructura
void Arbol::generarGrafo(Nodo *binaryTree) {
	ofstream files;

	files.open("grafo.txt");
	files << "digraph G {" << endl;
	files << "node [style=filled fillcolor=yellow];" << endl;
	recorrerArbol(binaryTree, files);
	files << "}" << endl;
	files.close();
	system("dot -Tpng -ografo.png grafo.txt &");
	system("eog grafo.png &");
};