/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA.- ********************/

/*
* file Main.cpp
*
* g++ Main.cpp -o ABB-Ejecutable
* make Main.cpp
*      Main.o
*      ABB-Ejecutable
*/

//* Main.cpp - Programa que utiliza la clase Arbol.

// Llamar e incluir librerías necesarias
#include <cstdlib> 	// <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <fstream>
#include <iostream>
#include <cstdio>

using namespace std;
#include "Arbol.cpp"


// Método menú de Arbol
void menuIngresoArbol() {
	// Inicializar punteros, objetos y variables de manera local
	Nodo *arbol = nullptr;
	Arbol *apuntadorArbol = new Arbol();

	string valorOp{"\0"};
	int integerOp{0};
	int datoArbol{0};
	int i{0};

	// Menú de opciones
	do{
		cout << "\t     ------------------------------------ " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES - Arbol ***** " << endl;
		cout << "\t     ------------------------------------ " << endl;
		cout << "\n  [1]  > Ingreso de dato(s). " << endl;
		cout << "  [2]  > Eliminar número(s) buscado(s)." << endl;
		cout << "  [3]  > Modificar número(s) del árbol." << endl;
		cout << "  [4]  > Mostrar la estructura del árbol y el contenido en Preorden, Inorden y Posorden (incluye generación de grafos)." << endl;
		cout << "  [0]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";
		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		// Acciones de cada opción de menú
		if (integerOp == 1) {			//* Ingreso de valores
			printf("\033c");
			cout << "  > Ingrese dato(s): ";
			cin >> datoArbol;
			apuntadorArbol->ingresarDato(arbol, datoArbol, nullptr);
			cin.ignore();
			cout << "\n" << endl;
		} else if (integerOp == 2) {	// Eliminar número buscado del arbol
			printf("\033c");
			cout << "  > Ingrese número a eliminar del Árbol Binario de Busqueda: ";
			cin >> datoArbol;
			cout << "\n\t***** ELIMINAR DATO  « "<< datoArbol << " »  BUSCADO *****" << endl;
			apuntadorArbol->eliminarDato(arbol, datoArbol);
			cout << "\n\t***** MOSTRAR DATOS RESTANTES *****" << endl;
			apuntadorArbol->mostrarArbol(arbol, datoArbol);
			// Generador de grafo.
			cout << "\n\n\n\t ····· GENERANDO NUEVO GRAFO ····· " << endl;
			apuntadorArbol->generarGrafo(arbol);
			cout << "\n" << endl;
		} else if (integerOp == 3) {	// Modificar elemento(s) del arbol.
			printf("\033c");
			cout << "  > Ingrese número a modificar del arbol: ";
			cin >> datoArbol;
			if (apuntadorArbol->buscarDato(arbol, datoArbol) != false){
				apuntadorArbol->eliminarDato(arbol, datoArbol);
				cout << "\n  > Favor, ingrese nuevo dato al arbol: ";
				cin >> datoArbol;
				apuntadorArbol->ingresarDato(arbol, datoArbol, nullptr);
				cout << "\n\t***** MOSTRAR DATOS RESTANTES *****" << endl;
				apuntadorArbol->mostrarArbol(arbol, datoArbol);
				// Generador de grafo.
				cout << "\n\n\n\t ····· GENERANDO NUEVO GRAFO ····· " << endl;
				apuntadorArbol->generarGrafo(arbol);
			} else {
				cout << "\t***** VALOR INVÁLIDO/INEXISTENTE, FAVOR INTENTE DE NUEVO *****" << endl;
			}
			cin.ignore();
			cout << "\n" << endl;
		} else if (integerOp == 4) {	//* Mostrar la estructura del árbol y el contenido en Preorden, Inorden y Posorden (incluye generación de grafos).
			printf("\033c");
			cout << "\t***** IMPRIMIR ESTRUCTURA DEL ÁRBOL *****\n" << endl;
			apuntadorArbol->mostrarArbol(arbol, datoArbol);

			cout << "\n\t***** MOSTRAR EL CONTENIDO DEL ÁRBOL EN PREORDEN, INORDEN Y POSORDEN *****" << endl;
			cout << "\n     ----- Preorden ----- " << endl;
			cout << "\n";
			apuntadorArbol->imprimirPreorden(arbol);
			cout << "\n\n\n     ----- Inorden ----- " << endl;
			cout << "\n";
			apuntadorArbol->imprimirInorden(arbol);
			cout << "\n\n\n     ----- Posorden ----- " << endl;
			cout << "\n";
			apuntadorArbol->imprimirPostorden(arbol);

			// Generador de grafo.
			cout << "\n\n\n\t ····· GENERANDO GRAFO ····· " << endl;
			apuntadorArbol->generarGrafo(arbol);

			cout << "\n" << endl;
		} else if (integerOp == 0){		//* Salida del programa
			printf("\033c");
			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;
			i++;
		} else {						//* Manejo de opciones inválidas
			printf("\033c");
			cout << "\t--------------------------------------------------" << endl;
			cout << "\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cout << "\t--------------------------------------------------\n" << endl;
		};
	} while (i<1);

	// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
	apuntadorArbol->~Arbol();
	delete arbol;
	delete apuntadorArbol;
	arbol = nullptr;
	apuntadorArbol = nullptr;
};

int main() {
	printf("\033c");
	// Se invoca la función menú
	menuIngresoArbol();
	return 0;
};