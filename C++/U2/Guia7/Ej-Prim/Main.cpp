/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA.- ********************/

/*
* file Main.cpp
*
* g++ Main.cpp -o Prim
* make Main.cpp
*      Main.o
*      Prim
*/

//* Main.cpp - Programa que utiliza la clase Prim.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;
#include "Prim.cpp"


//* Main
int main(int argc, char *argv[]) {
	printf("\033c");

	if (argc != 2) {	// Si el programa no recibe parámetros válidos (el nombre de archivo + valor que corresponde a cant. de nodos), entonces:

		cout << "\t --------------------------------------------------" << endl;
		cout << "\t ***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
		cout << "\t --------------------------------------------------\n" << endl;

		return -1;

	} else {		// De lo contrario, si se recibe la cantidad de parámetros válidos, el segundo se asume como la cantidad de nodos y se inserta en el programa automáticamente
		printf("\033c");

		// Inicializar punteros, objetos y variables de manera local
		int numeroNodos{atoi(argv[1])};

		if (numeroNodos <= 2){		// Si la cantidad de nodos no es válida, entonces:
			cout << "\t --------------------------------------------------" << endl;
			cout << "\t ***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cout << "\t --------------------------------------------------\n" << endl;
			return -1;
		} else {					// De lo contrario, se inicializa el programa
				// Inicializar punteros, objetos y variables de manera local
			Prim *apuntadorPrim = new Prim();
			int i{0};		// Variable contador
			int j{0};		// Variable contador
			string valorOp{"\0"};			// Variables auxiliares de selección de opciones
			int integerOp{0};				// Variables auxiliares de selección de opciones
			string vector[numeroNodos];		// Vector de matriz
			int **matriz;					// Array de punteros

			apuntadorPrim->inicializarVector(numeroNodos, vector);

				// Crea matriz de tamaño N×N de enteros.
			matriz = new int*[numeroNodos];
			for (i = 0; i < numeroNodos; i++) {
				matriz[i] = new int[numeroNodos];
			}

			apuntadorPrim->inicializarMatriz(matriz, numeroNodos);

				// Relleno de datos
			apuntadorPrim->ingresarAlias(matriz, numeroNodos, vector);
			apuntadorPrim->completarMatriz(matriz, numeroNodos, vector);

				// Menú de opciones
			do {
				cout << "\t     ------------------------------------------------ " << endl;
				cout << "\t     ***** MENÚ DE OPCIONES - Algoritmo de Prim ***** " << endl;
				cout << "\t     ------------------------------------------------ " << endl;
				cout << "\n  [1]  > Aplicar algoritmo de Prim y mostrar detalles (Incluye grafos). " << endl;
				cout << "  [0]  > Salir del programa." << endl;
				cout << "\n - Favor, elija una opción: " << endl << "  > ";

				cin >> valorOp;
				integerOp = atoi(valorOp.c_str());
				cin.ignore();

				// Acciones de cada opción de menú
				if (integerOp == 1) {				//* Aplica el algoritmo de Prim.
					printf("\033c");

					apuntadorPrim->imprimirMatriz(matriz, numeroNodos);
					apuntadorPrim->generarGrafoInicial(matriz, numeroNodos, vector);
					apuntadorPrim->algoritmoPrim(matriz, numeroNodos, vector);

					cout << "\n" << endl;
				} else if (integerOp == 0){		//* Salida del programa.
					printf("\033c");

					cout << "\t ----------------------------" << endl;
					cout << "\t ***** ¡SALIDA EXITOSA! *****" << endl;
					cout << "\t ----------------------------\n" << endl;

					j++;
				} else {						//* Manejo de opciones inválidas.
					printf("\033c");

					cout << "\t --------------------------------------------------" << endl;
					cout << "\t ***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
					cout << "\t --------------------------------------------------\n" << endl;
				};
			} while (j < 1);

				// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
			apuntadorPrim->~Prim();
			delete apuntadorPrim;
			apuntadorPrim = nullptr;
		}
	}

	return 0;
};