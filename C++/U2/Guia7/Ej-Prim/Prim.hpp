/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Grafos (Prim).- ********************/

/*
* file Prim.hpp
*/

//* Prim.hpp - Declaración de la clase Prim.

#ifndef PRIM_HPP
#define PRIM_HPP

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;


// Clase Prim: Genera grafo y sus métodos (funciones).
class Prim {
	// Miembros privados
	private:    // Atributos
		ofstream files;					// Archivo de generación de grafos (png - txt)
		int distanciaNodos{};			// Guarda el valor de distancias a los nodos

	// Miembros protegidos
	protected:
		int **matriz;					// Inicializamos variable matricial
		int nFilas{};					// Fila de la matriz
		int nColumnas{};				// Columna de la matriz
		int numeroNodos{};				// Variable que contiene la cantidad de nodos del grafo
		string nodosID;					// Almacena los nombres de los nodos

	// Miembros públicos
	public:

	/* Constructores - Destructores */
		// Constructores - Destructores
	Prim();   // Constructor por defecto
	~Prim();  // Destructor

	/* Métodos - Funciones */
		// Método que inicializa e imprime los vectores
	void inicializarVector(int numeroNodos, string* vector);
	// void imprimirVector(int numeroNodos, string* vector);	//! DEPRECATED

		// Método que inicializa la matriz
	void inicializarMatriz(int** matriz, int numeroNodos);

		// Método que asigna identificaciones a los nodos del grafo.
	void ingresarAlias(int** matriz, int numeroNodos, string* vector);
	void completarMatriz(int** matriz, int numeroNodos, string* vector);

		// Método que imprime matriz de adyacencia inicial.
	void imprimirMatriz(int** matriz, int numeroNodos);

		// Método que analiza menor distancia.
	int minDistancia(int numeroNodos, int* nodoPeso, bool* nodoElegido);

		// Método que implementa el Algoritmo de Prim e imprime el resultado.
	void imprimirAACM(int** matriz, int* nodoPadre, int numeroNodos, string* vector);
	void algoritmoPrim(int** matriz, int numeroNodos, string* vector);

		// Recorrer la estructura Grafos	//! DEPRECATED
	// void recorrerGrafoInicial(string* vector, ofstream &files);
	// void recorrerGrafoFinal(string* vector, ofstream &files);

		// Generar grafo de la estructura Grafos
	void generarGrafoInicial(int** matriz, int numeroNodos, string* vector);
	void generarGrafoFinal(int** matriz, int* nodoPadre, int* nodoPeso, int numeroNodos, string* vector);

};

#endif
