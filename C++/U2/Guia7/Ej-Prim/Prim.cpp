/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Grafo Prim.- ********************/

/*
* file Prim.cpp
*/

//* Prim.cpp - Definición de la clase Prim.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;
#include "Prim.hpp"


//* Prototipos de funciones
	// Inicializar vectores.
void inicializarVector(int, string*);
// void imprimirVector(int, string*);		//! DEPRECATED

	// Inicializar matriz.
void inicializarMatriz(int**, int);

	// Ingreso de datos.
void ingresarAlias(int, string*);
void completarMatriz(int, string*);

	// Impresión de matriz de adyacencia inicial.
void imprimirMatriz(int**, int);

	// Identificar menor distancia entre nodos
int minDistancia(int, int*, bool*);

	// Implementación del Algoritmo de Prim.
void imprimirACM(int**, int*, int, string*);
void algoritmoPrim(int**);

//! DEPRECATED
// 	// Recorrido del algoritmo para generación de grafos
// void recorrerGrafoInicial(string*, ofstream&);
// void recorrerGrafoFinal(string*, ofstream&);

	// Generar grafo de la estructura Grafos
void generarGrafoInicial(int**, int, string*);
void generarGrafoFinal(int**, int*, int*, int, string*);


// Constructor
Prim::Prim() {
	ofstream files;					// files de generación de grafos (png - txt)
	int **matriz;					// Inicializamos variable matricial
};

// Destructor
Prim::~Prim() {
		// Inicializar punteros, objetos y variables de manera local
	int i{0};					// Variable contador

	for (i = 0; i < numeroNodos; i++) {
		delete [] matriz[i]; 	// Cada fila de la matriz es otro array de punteros
								// Por eso son punteros a punteros
	}

	// Luego de limpiar las columnas, quitamos la fila única que quedó
	delete [] matriz;
};

// Método que inicializa un vector.
void Prim::inicializarVector(int numeroNodos, string* vector) {
	for (nColumnas = 0; nColumnas < numeroNodos; nColumnas++) {
		vector[nColumnas] = ' ';
	}
};

//! DEPRECATED
// // Método que imprime el vector.
// void Prim::imprimirVector(int numeroNodos, string* vector) {
//  	// Inicializar punteros, objetos y variables de manera local
//  	int i{0};		// Variable contador

// 	cout << endl;
// 	for (i = 0; i < numeroNodos; i++) {
// 		cout << "vector[" << i << "]: " << vector[i] << " " ;
// 	} cout << endl;
// };

// Método que inicializa una matriz de tamaño N×N.
void Prim::inicializarMatriz(int** matriz, int numeroNodos) {
	/** Permite inicializar la matriz de tamanio N×N,
	 * además recibe punteros y los inserta a la matriz */

 	// Inicializar punteros, objetos y variables de manera local
 	// int i{0};		// Variable contador

	for (nFilas = 0; nFilas < numeroNodos; nFilas++) {
		for (nColumnas = 0; nColumnas < numeroNodos; nColumnas++) {
			matriz[nFilas][nColumnas] = -1;
		}
	}
};

// Método que imprime la matriz generada.
void Prim::imprimirMatriz(int** matriz, int numeroNodos) {
	/** La siguiente funcion imprime la matriz de
	 * adyacencia y genera una salida por pantalla */
	// Imprimir matriz inicial

	cout << "\t  ───── ALGORITMO DE PRIM PARA GRÁFICAS NO DIRIGIDAS ─────\n" << endl;
	cout << "\t     ----- MATRIZ INICIAL -----\n" << endl;
	for (nFilas = 0; nFilas < numeroNodos; nFilas++) {
		for (nColumnas = 0; nColumnas < numeroNodos; nColumnas++) {
			cout << " " << matriz[nFilas][nColumnas] << "  ";
		} cout << endl;
	}
};

// Método que asigna identificaciones a los nodos del grafo.
void Prim::ingresarAlias(int** matriz, int numeroNodos, string* vector) {
	/** Este algoritmo permite identificar todos y cada uno
	 * de los nodos del grafo, asignandoles los alias mediante
	 * el ingreso de datos por consola de entrada. */

	printf("\033c");

	// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador

	cout << "\t  ───── ALGORITMO DE PRIM PARA GRÁFICAS NO DIRIGIDAS ─────\n" << endl;
	cout << "\t     ----- INGRESO DE DATOS DE LOS NODOS DEL GRAFO -----\n" << endl;
	while (i < numeroNodos) {
		cout << " Ingrese el nombre del Nodo " << i + 1 << ": ";
		cin >> nodosID;
		vector[i] = nodosID;
		i++;
	} cout << endl;
};

// Método que rellena la matriz de adyacencia de la estructura Grafo.
void Prim::completarMatriz(int** matriz, int numeroNodos, string* vector) {
	/** Este algoritmo permite completar la matriz de adyacencia
	 * con los datos anteriormente ingresados por el usuario */

	// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)

	// Rellenar la matriz con datos del grafo (vértices)
	for (i = 0; i < numeroNodos; i++) {
		cout << "\t     ····· NODO " << vector[i] << " ····· " << endl;
		for (j = 0; j < numeroNodos; j++) {
			if (i != j /* && i < j */) {	// Ingreso de distancias o conversión a 0 si refiere al mismo nodo
				cout << " Digite la distancia desde el nodo « " << vector[i] << " » hacia « " << vector[j] << " »: ";
				cin >> matriz[i][j];

			} else {		// Conversión a 0 (Diagonal de la matriz)
				matriz[i][j] = 0;
			}
		} cout << "\n" << endl;
	}
};

// Método que calcula la menor distancia entre nodos.
int Prim::minDistancia(int numeroNodos, int* nodoPeso, bool* nodoElegido) {
	/** Este método permite calcular e identificar
	 * las menores distancias entre nodos */

		// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)

	int minIndice, minPeso{9999};		// Valor de referencia muy grande que simboliza el infinito;

	for (i = 0; i < numeroNodos; i++) {
		if (nodoElegido[i] != true && nodoPeso[i] < minPeso) {
			minPeso = nodoPeso[i];
			minIndice = i;
		}
	}
	return minIndice;
};

//* Método que imprime el resultado de la aplicación del algoritmo de Prim.
void Prim::imprimirAACM(int** matriz, int* nodoPadre, int numeroNodos, string* vector) {
	/** Este algoritmo imprime el resultado de aplicar
	 * el algoritmo de Prim para encontrar el menor camino
	 * entre todos los nodos (árbol de minima expansión - coste mínimo) */

		// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	int menorCoste{0};		// Variable auxiliar

	cout << "\n\n\t     ----- ÁRBOL ABARCADOR DE COSTO MÍNIMO -----\n" << endl;
	cout << " Edge   :  Weight\n" << endl;
	for (i = 1; i < numeroNodos; i++) {
		// cout << vector[nodoPadre[i]] << " - " << vector[i] << "\t" << matriz[i][nodoPadre[i]] << endl;
		cout << vector[nodoPadre[i]] << " - " << vector[i] << endl;
		cout << nodoPadre[i] << " - " << i << "   :    " << matriz[i][nodoPadre[i]] << "\n" << endl;
		menorCoste += matriz[i][nodoPadre[i]];
	} cout << " > El menor coste total es de: « " << menorCoste << " » unidad(es)." << endl;
};

// Método que implementa el algoritmo de Prim.
void Prim::algoritmoPrim(int** matriz, int numeroNodos, string* vector) {
	/** Función que implementa el algoritmo
	 * de Prim para Gráficas no dirigidas */

	// Inicializar punteros, objetos y variables de manera local
	int i{0};											// Variable contador (filas)
	int j{0};											// Variable contador (columnas)
	int x{0};											// Variable contador
	int aux{0};											// Variable auxiliar
	int menorCoste{0};									// Variable auxiliar
	bool nodoElegido[numeroNodos];						// Booleano
	int nodoPeso[numeroNodos], nodoPadre[numeroNodos];	// Determinan peso de aristas y los nodos respectivos
	int v, menorPeso;									// Variables auxiliares sin inicializar

		// Inicializar el array
	for (i = 0; i < numeroNodos; i++) {
		nodoPeso[i] = 9999;			// Valor de referencia muy grande que simboliza el infinito
		nodoElegido[i] = false;
		nodoPadre[i] = -1;
	}

	nodoPeso[0] = 0;		// Se almacenan los vértices en el árbol de coste minimo, inicializando el 1er indice en 0.
	nodoPadre[0] = -1;		// Determina que primer nodo es siempre el inicio del Arbol de minima expansión

			// El Árbol de coste mínimo tiene un tamaño máximo de V-1 vértices
		for (x = 0; x < numeroNodos - 1; x++) {
			// Se llama a función que busca el menor peso del resto de vértices
		menorPeso = minDistancia(numeroNodos, nodoPeso, nodoElegido);

		nodoElegido[menorPeso] = true;		// Almacenar el vertice con menor peso en el ACM (Árbol de coste minimo).

			// En cada ciclo actualiza los estados del nodoPeso, nodoElegido es falso si el vértice aún no es incluido en el arbol
		for (v = 0; v < numeroNodos; v++) {
			if (matriz[menorPeso][v] != 0 && nodoElegido[v] == false && matriz[menorPeso][v] < nodoPeso[v]) {
				nodoPadre[v] = menorPeso;
				nodoPeso[v] = matriz[menorPeso][v];
			}
		}
	}

		// Imprimir detalle de Bordes (Edge) y Peso (Weight), además de instanciar generación de grafos.
	imprimirAACM(matriz, nodoPadre, numeroNodos, vector);

	cout << "\n\n\t     ----- GRAFOS INICIAL Y FINAL DE LA ESTRUCTURA DINÁMICA -----" << endl;
	generarGrafoFinal(matriz, nodoPadre, nodoPeso, numeroNodos, vector);
};

//! DEPRECATED
// Método que recorre el grafo
// void Prim::recorrerGrafoInicial(string* vector, ofstream &files) {
// 	/** El siguiente algoritmo recorre la
// 	 * estructura para generar el grafo inicial*/

// 		// Inicializar punteros, objetos y variables de manera local
// 	int i{0};		// Variable contador (filas)
// 	int j{0};		// Variable contador (columnas)

// 			// Recorrer la matriz original
// 	for (i = 0; i < numeroNodos; i++) {
// 		for (j = 0; j < numeroNodos; j++){
// 			if (i != j && i < j && matriz[i][j]) {
// 				files << vector[i] << " -- " << vector[j] << " ";
// 				files << "[label=" + to_string(matriz[i][j]) << "];" << endl;
// 			}
// 		}
// 	}
// };


//! DEPRECATED
// // Método que recorre el grafo
// void Prim::recorrerGrafoFinal(string* vector, ofstream &files) {
// 	/** El siguiente algoritmo recorre la
// 	 * estructura para generar el grafo final */

	// 	// Inicializar punteros, objetos y variables de manera local
	// int i{0};		// Variable contador (filas)
	// int j{0};		// Variable contador (columnas)

//		// Recorrer la matriz original
	// for (i = 1; i < numeroNodos; i++) {
	// 	files << vector[nodoPadre[i]] << " -- " << vector[i] << " ";
	// 	files << "[label=" + to_string(nodoPeso[i]) << "];" << endl;
	// }
// };

// Método que genera grafo de la estructura.
void Prim::generarGrafoInicial(int** matriz, int numeroNodos, string* vector) {
	/** Este algoritmo crea el grafo original de
	 *  la estructura del grafo no dirigido */

		// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)
	ofstream files;

	files.open("grafoOriginal.txt", ios::out);

	if (files.fail()){
		cout << "\t ---------------------------------------------------" << endl;
		cout << "\t ***** EL ARCHIVO NO EXISTE / NO SE PUDO ABRIR *****" << endl;
		cout << "\t ---------------------------------------------------\n" << endl;

		exit(1);
	} else {
		files << "graph G {" << endl;
		files << "graph [rankdir = LR labelloc=t label=Grafo_Inicial]" << endl;
		files << "node [style=filled fillcolor=lawngreen]" << endl << endl;

		// recorrerGrafoInicial(vector, files);		//! DEPRECATED

		for (i = 0; i < numeroNodos; i++) {
			for (j = 0; j < numeroNodos; j++){
				if (i != j && i < j && matriz[i][j]) {
					files << vector[i] << " -- " << vector[j] << " ";
					files << "[label=" + to_string(matriz[i][j]) << "];" << endl;
				}
			}
		}

		files << "}" << endl;
		files.close();

		system("dot -Tpng -ografoOriginal.png grafoOriginal.txt &");
		system("eog grafoOriginal.png &");
	}
};

// Método que genera grafo de la estructura.
void Prim::generarGrafoFinal(int** matriz, int* nodoPadre, int* nodoPeso, int numeroNodos, string* vector) {
	/** Este algoritmo crea el grafo final de
	 *  la estructura del grafo no dirigido */

		// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	ofstream files;

	files.open("grafoFinal.txt", ios::out);

	if (files.fail()){
		cout << "\t ---------------------------------------------------" << endl;
		cout << "\t ***** EL ARCHIVO NO EXISTE / NO SE PUDO ABRIR *****" << endl;
		cout << "\t ---------------------------------------------------\n" << endl;

		exit(1);
	} else {
		files << "graph G {" << endl;
		files << "graph [rankdir = LR labelloc=t label=Grafo_Final]" << endl;
		files << "node [style=filled fillcolor=lawngreen]" << endl << endl;

		// recorrerGrafoFinal(vector, files);		//! DEPRECATED

		for (i = 1; i < numeroNodos; i++) {
			files << vector[nodoPadre[i]] << " -- " << vector[i] << " ";
			files << "[label=" + to_string(nodoPeso[i]) << "];" << endl;
		}

		files << "}" << endl;
		files.close();

		system("dot -Tpng -ografoFinal.png grafoFinal.txt &");
		system("eog grafoFinal.png &");
	}
};