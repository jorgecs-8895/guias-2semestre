# Guía 7 - Unidad II

Los siguientes ejercicios fueron escritos en C/C++, y consisten en un programa cuya implementación se basa en el uso y manipulación de estructuras dinámicas, específicamente, de _Grafos no dirigidos_, utilizando para ello el Algoritmo de Prim.
- **Prim**: Un programa que solicita datos tanto de distancia como de nombres para los nodos, y mediante el uso del Algoritmo de Prim, encuentra el árbol abarcador de costo mínimo de la gráfica con _N_ vértices.



## Historia

Escrito y desarrollado por el estudiante de Ing. Civil en Bioinformática Jorge Carrillo Silva, utilizando para ello el lenguaje de programación C/C++.


## Para empezar

_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos

Es requisito cerciorarse de tener instalado C/C++ y su compilador correspondiente en el equipo donde ejecutará el proyecto. Es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones).


### Instalación y ejecución

_Para ejecutar (sin instalar previamente) el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git).
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U2/Guia7/", que contiene la carpeta "Ej-Prim", además del archivo 'README.md' y el Instructivo del proyecto.
3. Abrir una terminal en la carpeta contenedora del programa (Ej-Prim).
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para lanzar el programa, deberá ingresar el (los) siguiente(s) comando(s) en su terminal

- Dentro de la carpeta Ej-Prim
```
./Prim + "número de nodos"
```


_Para instalar el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git).
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U2/Guia7/", que contiene la carpeta "Ej-Prim", además del archivo 'README.md' y el Instructivo del proyecto.
3. Abrir una terminal (con permisos de Superusuario 'sudo ~ su') en la carpeta contenedora del programa (Ej-Prim).
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para instalar el programa deberá ingresar el siguiente comando.

```
make install
```

6. Para lanzar el programa, deberá ingresar el siguiente comando en su terminal

- Ej-Prim
```
./Prim + "número de nodos"
```


## Codificación

Soporta la codificación estándar UTF-8


## Construido con

* [Visual Studio Code](https://code.visualstudio.com) - IDE utilizado para el desarrollo del proyecto.


## Autores️

* **Jorge Cristóbal Carrillo Silva** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895)


## Licencia

Este proyecto está sujeto bajo la Licencia (GNU GPL v.3)

