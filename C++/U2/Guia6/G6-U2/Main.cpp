/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA.- ********************/

/*
* file Main.cpp
*
* g++ Main.cpp -o Grafos-Dijkstra
* make Main.cpp
*      Main.o
*      Grafos-Dijkstra
*/

//* Main.cpp - Programa que utiliza la clase Arbol.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;
#include "Dijkstra.cpp"


//* Main
int main(int argc, char *argv[]) {
	printf("\033c");

	if (argc != 2) {	// Si el programa no recibe parámetros válidos (el nombre de archivo + valor que corresponde a cant. de nodos), entonces:

		cout << "\t --------------------------------------------------" << endl;
		cout << "\t ***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
		cout << "\t --------------------------------------------------\n" << endl;

		return -1;

	} else {		// De lo contrario, si se recibe la cantidad de parámetros válidos, el segundo se asume como la cantidad de nodos y se inserta en el programa automáticamente
		printf("\033c");

		// Inicializar punteros, objetos y variables de manera local
		int numeroNodos{};
		numeroNodos = atoi(argv[1]);

		if (numeroNodos <= 2){		// Si la cantidad de nodos no es válida, entonces:
			cout << "\t --------------------------------------------------" << endl;
			cout << "\t ***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cout << "\t --------------------------------------------------\n" << endl;
			return -1;
		} else {					// De lo contrario, se inicializa el programa
			// Inicializar punteros, objetos y variables de manera local
			string alias[numeroNodos];
			Dijkstra *apuntadorDijkstra = new Dijkstra();

			// Crea matriz de tamaño nxn de enteros.
			int **matriz;
			matriz = new int*[numeroNodos];
			for(int i=0; i<numeroNodos; i++) {
				matriz[i] = new int[numeroNodos];
			}

			string valorOp{"\0"};
			int integerOp{0};
			int i{0};

			apuntadorDijkstra->ingresarAlias(numeroNodos, alias);
			apuntadorDijkstra->completarMatriz(numeroNodos, alias);

			// Menú de opciones
			do{
				cout << "\t     ---------------------------------------------------- " << endl;
				cout << "\t     ***** MENÚ DE OPCIONES - Algoritmo de Dijkstra ***** " << endl;
				cout << "\t     ---------------------------------------------------- " << endl;
				cout << "\n  [1]  > Aplicar algoritmo e imprimir el detalle y la Matriz generada" << endl;
				cout << "  [2]  > Generar grafo de la estructura dinámica (Grafos - Algoritmo de Dijkstra)." << endl;
				cout << "  [0]  > Salir del programa." << endl;
				cout << "\n - Favor, elija una opción: " << endl << "  > ";

				cin >> valorOp;
				integerOp = atoi(valorOp.c_str());
				cin.ignore();

				// Acciones de cada opción de menú
				if (integerOp == 1) {				//*
					printf("\033c");

					apuntadorDijkstra->imprimirMatriz(numeroNodos);
					apuntadorDijkstra->dijkstraAlgoritmo(numeroNodos, alias);

					cout << "\n" << endl;
				} else if (integerOp == 2) {	//* Generar grafo.
					printf("\033c");

						// Generador de grafo.
					cout << "\t ····· GENERANDO NUEVO GRAFO ····· " << endl;
					apuntadorDijkstra->generarGrafo(numeroNodos, alias);

					cout << "\n" << endl;
				} else if (integerOp == 0){		//* Salida del programa.
					printf("\033c");

					cout << "\t ----------------------------" << endl;
					cout << "\t ***** ¡SALIDA EXITOSA! *****" << endl;
					cout << "\t ----------------------------\n" << endl;
					i++;
				} else {						//* Manejo de opciones inválidas.
					printf("\033c");

					cout << "\t --------------------------------------------------" << endl;
					cout << "\t ***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
					cout << "\t --------------------------------------------------\n" << endl;
				};
			} while (i<1);

				// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
			apuntadorDijkstra->~Dijkstra();
			delete apuntadorDijkstra;
			apuntadorDijkstra = nullptr;
		}
	}

	return 0;
};