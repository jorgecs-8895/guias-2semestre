/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Grafos (Dijkstra).- ********************/

/*
* file Dijkstra.hpp
*/

//* Dijkstra.hpp - Declaración de la clase Dijkstra.

#ifndef DIJKSTRA_HPP
#define DIJKSTRA_HPP

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;


// Clase Dijkstra: Genera grafo y sus métodos (funciones).
class Dijkstra{
	// Miembros privados
	private:    // Atributos

	// Miembros protegidos
	protected:
		ofstream files;
		int **matriz;					// Inicializamos variable matricial
		int numeroNodos{};				// Variable que contiene la cantidad de nodos del grafo
		string nodosID;					// Almacena los nombres de los nodos
		int distanciaNodos{};			// Guarda el valor de distancias a los nodos
		bool existeNodos{false};		// Variable booleana (determina existencia de un nodo en el grafo)
		int minReferencias{2147483647};	// Se asigna ese valor, correspondiente al max. nro posible en el rango de ‘int‘ en C
		int posNodos{};					// Variable que almacena posiciones.

	// Miembros públicos
	public:

	/* Constructores - Destructores */
		// Constructores - Destructores
	Dijkstra();   // Constructor por defecto
	~Dijkstra();  // Destructor

	/* Métodos - Funciones */
		// Método que asigna identificaciones a los nodos del grafo.
	void ingresarAlias(int numeroNodos, string alias[]);
	void completarMatriz(int numeroNodos, string alias[]);

		// Método que imprime matriz de adyacencia inicial.
	void imprimirMatriz(int numeroNodos);

		// Método que comprueba existencia de nodos y menor distancia.
	bool compruebaNodos(int numeroNodos, int *posicionNodos, string idNodos, string alias[]);
	int minDistancia(int numeroNodos, int D[], int VS[]);

		// Método que implementa el Algoritmo de Dijkstra.
	void dijkstraAlgoritmo(int numeroNodos, string alias[]);

		// Generar grafo de la estructura Grafos
	void generarGrafo(int numeroNodos, string alias[]);
	void recorrerGrafo(int numeroNodos, string alias[], ofstream &files);
};

#endif
