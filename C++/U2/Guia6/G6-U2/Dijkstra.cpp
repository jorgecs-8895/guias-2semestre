/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Grafo Dijkstra.- ********************/

/*
* file Dijkstra.cpp
*/

//* Dijkstra.cpp - Definición de la clase Dijkstra.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;
#include "Dijkstra.hpp"


//* Prototipos de funciones
	// Ingreso de datos.
void ingresarAlias(int, string[]);
void completarMatriz(int, string[]);
	// Impresión de matriz de adyacencia inicial.
void imprimirMatriz(int);
	// Comprobar existencia de nodos
bool compruebaNodos(int, int*, string, string[]);
	// Identificar menor distancia entre nodos
int minDistancia(int, int[], int[]);
	// Implementación del Algoritmo de Dijkstra.
void dijkstraAlgoritmo(int, string[]);
	// Generar grafo de la estructura Grafos
// void generarGrafo(int**, int);


// Constructores - Destructores
Dijkstra::Dijkstra() {};

Dijkstra::~Dijkstra() {};

// Método que asigna identificaciones a los nodos del grafo.
void Dijkstra::ingresarAlias(int numeroNodos, string alias[]) {
	/** Este algoritmo permite identificar todos y cada uno
	 * de los nodos del grafo, asignandoles los alias mediante
	 * el ingreso de datos por consola de entrada. */

	printf("\033c");

	// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador

	cout << "\t ───── INGRESO DE DATOS DE LOS NODOS DEL GRAFO ─────\n" << endl;
	while (i < numeroNodos) {
		cout << " Favor, ingrese el nombre del Nodo " << i + 1 << ": ";
		cin >> nodosID;
		alias[i] = nodosID;
		i++;
	}
};

// Método que rellena la matriz de adyacencia de la estructura Grafo.
void Dijkstra::completarMatriz(int numeroNodos, string alias[]) {
	/** Este algoritmo permite completar la matriz de adyacencia
	 * con los datos anteriormente ingresados por el usuario */

	// Inicializar punteros, objetos y variables de manera local
	matriz = new int*[numeroNodos];

	int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)

	// Inicializar matriz
	for (i = 0; i < numeroNodos; i++) {
		matriz[i] = new int[numeroNodos];
	}

	// Rellenar la matriz
	for (i = 0; i < numeroNodos; i++) {
		cout << "\n\t     ····· NODO " << alias[i] << " ····· " << endl;
		for (j = 0; j < numeroNodos; j++) {
			if (i != j) {	// Ingreso de distancias o conversión a 0 si refiere al mismo nodo
				cout << " Digite la distancia desde el nodo «" << alias[i] << "» hacia «" << alias[j] << "» (LAS DISTANCIAS DEBEN SER MAYORES A 0): ";
				cin >> distanciaNodos;

				while (distanciaNodos <= 0 || distanciaNodos > 99999) { // Ciclo que controla que las condiciones de las aristas del Algoritmo cumplan las condiciones de ser positivas y mayores a 0.
					cout << "\n\t ××××× EL VALOR INGRESADO NO ES VÁLIDO. LA DISTANCIA DEBE SER UN NÚMERO POSITIVO Y MAYOR A 0. ×××××" << endl;
					cout << " Digite la distancia desde el nodo «" << alias[i] << "» hacia «" << alias[j] << "» (LAS DISTANCIAS DEBEN SER MAYORES A 0): ";
					cin >> distanciaNodos;
				} matriz[i][j] = distanciaNodos;

			} else {		// Conversión a 0 (Diagonal de la matriz)
				matriz[i][j] = 0;
			}
		} cout << "\n" << endl;
	}
};

// Método que imprime matriz de adyacencia inicial.
void Dijkstra::imprimirMatriz(int numeroNodos) {
	/** La siguiente funcion imprime la matriz de
	 * adyacencia y genera una salida por pantalla */

	// Imprimir matriz inicial
	cout << "\t     ----- MATRIZ INICIAL -----\n" << endl;
	for (int i = 0; i < numeroNodos; i++) {
		for (int j = 0; j < numeroNodos; j++) {
			cout << " " <<  matriz[i][j] << "  ";
		} cout << "\n" << endl;
	}
};

// Método que comprueba existencia de nodos.
bool Dijkstra::compruebaNodos(int numeroNodos, int *posicionNodos, string idNodos, string alias[]) {
	/** El siguiente algoritmo realiza las comprobaciones
	 * de la existencia de los nodos dentro del grafo */
	printf("\033c");

	// Inicializar punteros, objetos y variables de manera local
	int i{0};			// Variable contador (filas)
	// int j{0};		// Variable contador (columnas)

	while (i < numeroNodos) {
		if (nodosID == alias[i]) {
			*posicionNodos = i;
			existeNodos = true;
		} i++;
	}
	return existeNodos;
};

// Método que calcula la menor distancia entre nodos.
int Dijkstra::minDistancia(int numeroNodos, int D[], int VS[]) {
	/** Este método permite calcular e identificar
	 * las menores distancias entre nodos */

	// Inicializar punteros, objetos y variables de manera local
	// int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)

	while (j <= numeroNodos) {
		if (D[j] < minReferencias && D[j] != VS[j]) {
			minReferencias = D[j];
			posNodos = j;
		} j++;
	}
	return posNodos;
};

// Método que implementa el Algoritmo de Dijkstra.
void Dijkstra::dijkstraAlgoritmo(int numeroNodos, string alias[]) {
	/** Función que implementa el algoritmo de Dijkstra. */

	// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)
	// int aux{0};
	bool existeNodos{false};
	int posicionNodos{}, posVertices{};
	int D[numeroNodos], VS[numeroNodos];
	string nodoElegido;
	string S[numeroNodos];

	// Se selecciona el nodo origen para efectos de cálculo con el Algoritmo de Dijkstra
	cout << " Favor, seleccione el vértice de origen del grafo: ";
	cin >> nodoElegido;

	// Se inserta el parámetro ingresado en la función que comprueba la existencia del nodo en el programa
	existeNodos = compruebaNodos(numeroNodos, &posicionNodos, nodoElegido, alias);

	while (existeNodos == false) {	// Si la variable booleana es ‘false‘, entonces el ciclo pedirá reingresar el dato
		cout << "\n\t ××××× EL VALOR INGRESADO NO ES VÁLIDO O NO EXISTE, FAVOR, INTENTE NUEVAMENTE. ×××××" << endl;
		cout << " Favor, seleccione el vértice de origen del grafo: ";
		cin >> nodoElegido;

		// Se inserta el parámetro ingresado en la función que comprueba la existencia del nodo en el programa
		existeNodos = compruebaNodos(numeroNodos, &posicionNodos, nodoElegido, alias);
	}

	// Ciclo que asigna las posiciones del arreglo VS[] a 0, y que en el arreglo D[i] almacena las distancias más cortas entre nodos.
	for (i = 0; i < numeroNodos; i++) {
		VS[i] = 0;
		D[i] = matriz[posicionNodos][i];
	}

	// Asignaciones de las distancias en las filas a los nombres de las columnas de la matriz
	S[0] = alias[posicionNodos];
	VS[posicionNodos] = 1;

	// Ingreso a la matriz para agregar y actualizar posiciones
	for (i = 0; i < numeroNodos; i++) {
		posVertices = minDistancia(numeroNodos, D, VS);
		S[i] = alias[posVertices];						//Se agrega un nombre del nodo
		VS[posVertices] = 1;							//Listas de las posiciones que estan disponibles

		// Se actualiza el arreglo D[]
		for (j = 0; j < numeroNodos; j++) {
			if (D[posVertices] + matriz[posVertices][j] < D[j] && D[posVertices] + matriz[posVertices][j] != VS[j]) {
				D[j] = D[posVertices] + matriz[posVertices][j];
			}
		}
	}

	// Imprimir distancias entre los vértices (nodos)
	cout << "\t     ----- DISTANCIAS ENTRE NODOS -----\n" << endl;
	for (i = 0; i < numeroNodos; i++) {
		cout << " La distancia entre el nodo « " << S[0] << " » al nodo « " << alias[i] << " » es de: " << D[i] << " unidad(es)" << endl;
	}
};

// Método que recorre el grafo
void Dijkstra::recorrerGrafo(int numeroNodos, string alias[], ofstream &files) {
	// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)

	if (matriz != nullptr) {
		for (i = 0; i < numeroNodos; i++) {
			for (j = 0; j < numeroNodos; j++) {
				// evalua la diagonal principal.
					if (i != j) {
						if (matriz[i][j] > 0) {
							// fprintf(files, "%c%s%c [label=%d];\n", alias[i],"->", alias[j], matriz[i][j]);
						}
					}
				}
			}
	}
};


// Método que genera grafo de la estructura
void Dijkstra::generarGrafo(int numeroNodos, string alias[]) {
	/** Función que implementa el algoritmo de Dijkstra. */

	// Inicializar punteros, objetos y variables de manera local
	int i{0};		// Variable contador (filas)
	int j{0};		// Variable contador (columnas)

	ofstream files;

	files.open("grafo.txt");
	files << "digraph G {" << endl;
	files << "node [style=filled fillcolor=yellow];" << endl;
	recorrerGrafo(numeroNodos, alias, files);
	files << "}" << endl;
	files.close();
	system("dot -Tpng -ografo.png grafo.txt &");
	system("eog grafo.png &");
};