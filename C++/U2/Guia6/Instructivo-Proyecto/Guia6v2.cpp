#include <iostream>
#include <fstream>
using namespace std;

class Dijkstra{
	//Se instancia la Matriz y el archivo que luego generara el grafo
	private:
		int **Matriz;
		ofstream Archivo;
	//Constructor por defecto
	public:
		Dijkstra () {}
		
	//Funcion para llenar la matriz con valores
	int Llenar_Matriz(int Total_Nodos,string Nombres[]){
		int Variable_Return;
		int Distancia;
		if((Matriz=new int*[Total_Nodos]) == NULL){
			Variable_Return=1;
		}
		for(int i=0;i<Total_Nodos;i++){
			if((Matriz[i]=new int[Total_Nodos])==NULL){
				Variable_Return=1;
			}
		}
		for(int i=0;i<Total_Nodos;i++){
			cout << "\n";
			cout << "Trabajando con el Nodo : " << Nombres[i];
			cout << "\n" << endl;
			for(int j=0;j<Total_Nodos;j++){
				//Significa que la distancia entre dos nodos no puede ser 0
				if(i!=j){
					cout << "Ingrese la distancia al nodo " << Nombres[j] << ": ";
					cin >> Distancia;
					//Se valida que las distancias que ingrese el usuario sean positivas
					while(Distancia<=0 || Distancia>99999){
						cout << "\n";
						cout << "Distancia incorrecta. Ingrese una distancia positiva : ";
						cin >> Distancia;
					}
					Matriz[i][j]=Distancia;
				}
				//El nodo es el mismo por lo que la distancia es igual
				else{
					Matriz[i][j]=0;
				}
			}
		}
		return Variable_Return;
	}
	void Agregar_Nombres(int Total_Nodos,string Nombres[]){
		//Agregar un nombre a cada nodo para hacerlo mas representativo
		string Nombre;
		for(int i=0;i<Total_Nodos;i++){
			cout << "\n";
			cout << "Ingrese un nombre para el nodo " << i+1 << ": ";
			cin >> Nombre;
			Nombres[i]=Nombre;
		}
	}
	//Funcion para mostrar la matriz por la terminal
	void Mostrar_Matriz(int Total_Nodos){
		for(int i=0;i<Total_Nodos;i++){
			for(int j=0;j<Total_Nodos;j++){
				cout << Matriz[i][j] << "   ";
			}
			cout << "\n";
		}	
	}
	//Funcion para validad si un vertice existe en la lista de los nombres
	//En caso de existir se retornara un true con su respectiva posicion
	bool Validar_Vertice(string Nombre,int Total_Nodos,string Nombres[],int *Posicion){
		bool Existe_Vertice=false;
		for(int i=0;i<Total_Nodos;i++){
			//Si se encuentra el vertice seleccionado, se ratifica con un true y se obtiene su posicion
			if(Nombre==Nombres[i]){
				*Posicion=i;
				Existe_Vertice=true;
			}
		}
		return Existe_Vertice;
	}
	//Funcion para elegir un vertice con la menor distancia
	int Vertice_Minimo(int Total_Nodos,int D[],int VS[]){
		int Minimo=99999,Vertice;//Se elige 99999 para tener un valor de referencia muy grande
		for(int j=0;j<=Total_Nodos;j++){
			if(D[j]<Minimo && D[j]!=VS[j]){
				Minimo=D[j];
				Vertice=j;
			}
		}
		return Vertice;
	}
	//Algoritmo de Dijkstra
	void Algoritmo_Dijkstra(int Total_Nodos,string Nombres[]){
		int Posicion,D[Total_Nodos],VS[Total_Nodos],v;
		string Nodo_Origen,S[Total_Nodos];
		bool Existe_Nodo=false;
		
		//El usuario elige cual sera el nodo de origen
		cout << "Ingrese el nodo que sera tomado como origen : ";
		cin >> Nodo_Origen;
		Existe_Nodo=Validar_Vertice(Nodo_Origen,Total_Nodos,Nombres,&Posicion);
		//Se valida que el nodo que esta ingresando el usuario efectivamente este en la lista de nombres
		while(Existe_Nodo==false){
			cout << "\n";
			cout << "Nodo ingresado no existe. Prueba de nuevo : ";
			cin >> Nodo_Origen;
			Existe_Nodo=Validar_Vertice(Nodo_Origen,Total_Nodos,Nombres,&Posicion);
		}
		
		for(int i=0;i<Total_Nodos;i++){
			VS[i]=0;//Todas las posiciones estan disponibles
			D[i]=Matriz[Posicion][i];//En el arreglo D se guardaran las distancias mas cortas
		}
		
		S[0]=Nombres[Posicion];//Se asignan los nombres para luego indicar las distancias entre nodos
		VS[Posicion]=1;//El origen ya esta ocupado
		
		for(int i=0;i<Total_Nodos;i++){
			v=Vertice_Minimo(Total_Nodos,D,VS);
			S[i]=Nombres[v];//Se agrega un nombre del nodo
			VS[v]=1;//Listas de las posiciones que estan disponibles
			//Se actualiza el maximo y se actualiza D[]
			for(int j=0;j<Total_Nodos;j++){
				if(D[v]+Matriz[v][j] < D[j] && D[v]+Matriz[v][j]!=VS[j]){
					D[j]=D[v]+Matriz[v][j];
				}
			}
		}
		//Se imprimen las distancias
		cout << "\n";
		cout << "Distancias entre los nodos" << endl;
		for(int i=0;i<Total_Nodos;i++){
			cout << "\n";
			cout << "La distancia entre " << S[0] << "->" << Nombres[i] << " es : " << D[i];  
		}
		cout << "\n";
	}
};

int main(int argc,char *argv[]){
	//Se valida que el usuario en el comando tenga dos parametros, el nombre del archivo y el total de nodos
	if(argc!=2){
		cout << "Comando mal ejecutado, el programa no se podra inciar" << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma : " << endl;
		cout << "Ejemplo: ./Guia6 Numero(Mayor o igual que 2)";
		cout << "\n";
		exit(-1);
	}
	else{
		string Cantidad;
		int Total_Nodos;
		Cantidad=argv[1];
		Total_Nodos=stoi(Cantidad);
		//Se valida que el total de nodos se mayor o igual a 2
		if(Total_Nodos<=2){
			cout << "El programa no se podra inciar. Parametro con la cantidad de nodos es inferior a 2" << endl;
			cout << "Intentelo de nuevo con un Valor de N igual o mayor que 2";
			cout << "\n";
			exit(-1);
		}
		else{
			int Respuesta_Menu;
			string Nombres[Total_Nodos];
			Dijkstra *Caminos_Dijkstra = new Dijkstra();
			cout << "SE DA INICIO AL PROGRAMA" << endl;
			Caminos_Dijkstra->Agregar_Nombres(Total_Nodos,Nombres);
			Caminos_Dijkstra->Llenar_Matriz(Total_Nodos,Nombres);
			//Se ejecuta el menu con los respectivos metodos
			do{
				cout << "\n";
				cout << "MENU" << endl << endl;
				cout << "Imprimir Matriz     [1]" << endl;
				cout << "Distancias Dijkstra [2]" << endl;
				cout << "Generar grafo       [3]" << endl;
				cout << "Salir del programa  [0]" << endl << endl;
				cout << "Eliga una opcion del menu : ";
				cin >> Respuesta_Menu;
				while(Respuesta_Menu!=0 && Respuesta_Menu!=1 && Respuesta_Menu!=2 && Respuesta_Menu!=3){
					cout << "\n";
					cout << "Error. Eliga una opcion del menu : ";
					cin >> Respuesta_Menu;
				}
				switch(Respuesta_Menu){
					case 1: cout << "\n";
							Caminos_Dijkstra->Mostrar_Matriz(Total_Nodos);
							break;
					case 2: cout << "\n";
							cout << "Las distancias mas cortas generadas son : " << endl;
							cout << "\n";
							Caminos_Dijkstra->Algoritmo_Dijkstra(Total_Nodos,Nombres);
							break;
					case 3: 
							Caminos_Dijkstra->Generar_Grafo(Total_Nodos,Nombres);
							break;
				}
			}while(Respuesta_Menu!=0);
			if(Respuesta_Menu==0){
				cout << "\n";
				exit(-1);
			}
		}
	}
	return 0;
}
