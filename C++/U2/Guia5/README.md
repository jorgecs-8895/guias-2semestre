# Guía 5 - Unidad II

Los siguientes ejercicios fueron escritos en C/C++, y consisten en un programa cuya implementación se basa en el uso de estructuras dinámicas, específicamente, de _Árboles AVL (Adelson-Velskii-Landis)_, los que son árboles ordenados o de búsqueda que, además, cumplen la condición de balanceo para cada uno de los nodos.
- **AVL-Ejecutable**: Un programa que solicita ID's de proteínas y los ingresa (sin repetición) en un Árbol AVL, presenta un menú de opciones que le darán al usuario herramientas para la manipulación de datos en esta estructura dinámica.



## Historia

Escrito y desarrollado por el estudiante de Ing. Civil en Bioinformática Jorge Carrillo Silva, utilizando para ello el lenguaje de programación C/C++.


## Para empezar

_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos

Es requisito cerciorarse de tener instalado C/C++ y su compilador correspondiente en el equipo donde ejecutará el proyecto. Es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones).


### Instalación y ejecución

_Para ejecutar (sin instalar previamente) el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git).
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U2/Guia5/", que contiene la carpeta "Ej-AVL", además del archivo 'README.md' y el Instructivo del proyecto.
3. Abrir una terminal en la carpeta contenedora del programa (Ej-AVL).
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para lanzar el programa, deberá ingresar el (los) siguiente(s) comando(s) en su terminal

- Dentro de la carpeta Ej-AVL
```
./Ej-AVL
```


_Para instalar el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git).
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U2/Guia5/", que contiene la carpeta "Ej-AVL", además del archivo 'README.md' y el Instructivo del proyecto.
3. Abrir una terminal (con permisos de Superusuario 'sudo ~ su') en la carpeta contenedora del programa (Ej-AVL).
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para instalar el programa deberá ingresar el siguiente comando.

```
make install
```

6. Para lanzar el programa, deberá ingresar el siguiente comando en su terminal

- Ej-AVL
```
./Ej-AVL
```


## Codificación

Soporta la codificación estándar UTF-8


## Construido con

* [Visual Studio Code](https://code.visualstudio.com) - IDE utilizado para el desarrollo del proyecto.


## Autores️

* **Jorge Cristóbal Carrillo Silva** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895)


## Licencia

Este proyecto está sujeto bajo la Licencia (GNU GPL v.3)

