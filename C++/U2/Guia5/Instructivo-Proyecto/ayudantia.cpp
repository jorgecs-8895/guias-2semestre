#include <iostream>
#include <fstream>
using namespace std;

struct Nodo {
    string codigo_pdb;
    int FE;
    struct Nodo *derecha;
    struct Nodo *izquierda;
};

class ArbolAVL {
    private:
        ofstream archivo; // salida
    public:
        ArbolAVL () {}

        void insertar(Nodo *&arbol, bool &Bo, string codigo_pdb){}

        void eliminar() {}

        void buscar() {}

        void recorrer_arbol(Nodo *arbol) {
            if(arbol != NULL){
                archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->codigo_pdb << '"' << "[label=" << arbol->FE << "]" << ";";
                if(arbol->izquierda != NULL){
                    archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->izquierda->codigo_pdb << '"' << ";";
                }
                else{
                    archivo << "\n" << '"' << arbol->codigo_pdb << "i" << '"' << " [shape=point];"; 
                    archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->codigo_pdb << "i" << '"';
                }
                if(arbol->derecha != NULL){
                    archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->derecha->codigo_pdb << '"' << ";";
                }
                else{
                    archivo << "\n" << '"' << arbol->codigo_pdb << "d" << '"' << " [shape=point];";
                    archivo << "\n" << '"' << arbol->codigo_pdb << '"' << "->" << '"' << arbol->codigo_pdb << "d" << '"';
                }
                recorrer_arbol(arbol->izquierda);
                recorrer_arbol(arbol->derecha);
            }
        }

        void crear_grafo(Nodo *arbol) {
            archivo.open("grafo.txt");
            if(archivo.fail()){
                cout << "No se puedo abrir el archivo";
                exit(1);
            }
            archivo << "digraph G {" << endl;
            archivo << "node [style=filled fillcolor=yellow];";
            recorrer_arbol(arbol);
            archivo << "}";
            archivo.close();
            
            system("dot -Tpng -ografo.png grafo.txt &");
            system("eog grafo.png &");
        }
};

int main(void) {
    ifstream archivo; // leer
    string codigo_pdb;

    Nodo *arbol = NULL;
    bool BO = false;
    ArbolAVL *arbol_avl = new ArbolAVL();

    archivo.open("pdb.txt");
    if (archivo.fail()) {
        cout << "Error al abrir el archivo." << endl;
        exit(1);
    }

    while (!archivo.eof()) {
        getline(archivo, codigo_pdb);
        arbol_avl->insertar(arbol, BO, codigo_pdb);
    }
    arbol_avl->crear_grafo(arbol);


    return 0;
}
