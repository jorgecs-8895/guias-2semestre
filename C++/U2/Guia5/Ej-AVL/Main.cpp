/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA.- ********************/

/*
* file Main.cpp
*
* g++ Main.cpp -o AVL-Ejecutable
* make Main.cpp
*      Main.o
*      AVL-Ejecutable
*/

//* Main.cpp - Programa que utiliza la clase Arbol.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib> 	// <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <fstream>
#include <iostream>

using namespace std;
#include "AVL.cpp"


// Método menú de Arbol
void menuIngresoArbol() {
	// Inicializar punteros, objetos y variables de manera local
	NodoAVL *arbolAVL = nullptr;
	Arbol *apuntadorAVL = new Arbol();

	string valorOp{"\0"};
	int integerOp{0};
	string datoArbol{"\0"};
	bool isAltura{false};
	int i{0};

	// Menú de opciones
	do{
		cout << "\t     ---------------------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES - Arbol AVL ***** " << endl;
		cout << "\t     ---------------------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de dato(s) ~ ID de proteínas. " << endl;
		cout << "  [2]  > Eliminar ID." << endl;
		cout << "  [3]  > Modificar un ID buscado." << endl;
		cout << "  [4]  > Generar grafo de la estructura AVL." << endl;
		cout << "  [5]  > Leer ID's de proteínas a partir de archivos .txt." << endl;
		cout << "  [0]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";
		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		// Acciones de cada opción de menú
		if (integerOp == 1) {			//* Ingreso de valores
			printf("\033c");

			cout << "  > Ingrese ID de Proteínas: ";
			cin >> datoArbol;
			if (apuntadorAVL->buscarDato(arbolAVL, datoArbol)) {
				cout << "\n\t Error, el valor  « " << datoArbol << " »  ya existe en el árbol. Favor, intente nuevamente. " << endl;
			} else {
				apuntadorAVL->ingresarDato(arbolAVL, isAltura, datoArbol);
			} cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 2) {	//* Eliminar número buscado del arbol
			printf("\033c");

			cout << "  > Ingrese número a eliminar del Árbol AVL: ";
			cin >> datoArbol;
			cout << "\n\t***** ELIMINAR DATO  « "<< datoArbol << " »  BUSCADO *****" << endl;
			apuntadorAVL->eliminarDato(arbolAVL, isAltura, datoArbol);
				// Generador de grafo.
			cout << "\n\n\n\t ····· GENERANDO NUEVO GRAFO ····· " << endl;
			apuntadorAVL->generarGrafo(arbolAVL);

			cout << "\n" << endl;
		} else if (integerOp == 3) {	//* Modificar elemento(s) del arbol.
			printf("\033c");

			cout << "  > Ingrese número a modificar del arbol: ";
			cin >> datoArbol;
			if (apuntadorAVL->buscarDato(arbolAVL, datoArbol)){
				apuntadorAVL->eliminarDato(arbolAVL, isAltura, datoArbol);
				cout << "  > Favor, ingrese nuevo dato al arbol: ";
				cin >> datoArbol;
				apuntadorAVL->ingresarDato(arbolAVL, isAltura, datoArbol);
					// Generador de grafo.
				cout << "\n\n\n\t ····· GENERANDO NUEVO GRAFO ····· " << endl;
				apuntadorAVL->generarGrafo(arbolAVL);
			} else {
				cout << "\t***** VALOR INVÁLIDO/INEXISTENTE, FAVOR INTENTE DE NUEVO *****" << endl;
			}

			cout << "\n" << endl;
		} else if (integerOp == 4) {	//* Mostrar la estructura del árbol.
			printf("\033c");

				// Generador de grafo.
			cout << "\n\n\n\t ····· GENERANDO NUEVO GRAFO ····· " << endl;
			apuntadorAVL->generarGrafo(arbolAVL);

			cout << "\n" << endl;
		} else if (integerOp == 5) {	//* Leer archivos pdb.
			printf("\033c");

			apuntadorAVL->abrirPdb();

			cout << "\n" << endl;
		}else if (integerOp == 0){		//* Salida del programa
			printf("\033c");

			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;

			i++;
		} else {						//* Manejo de opciones inválidas
			printf("\033c");

			cout << "\t--------------------------------------------------" << endl;
			cout << "\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cout << "\t--------------------------------------------------\n" << endl;
		};
	} while (i<1);

	// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
	apuntadorAVL->~Arbol();
	delete arbolAVL;
	delete apuntadorAVL;
	arbolAVL = nullptr;
	apuntadorAVL = nullptr;
};

int main() {
	printf("\033c");
	// Se invoca la función menú
	menuIngresoArbol();
	return 0;
};