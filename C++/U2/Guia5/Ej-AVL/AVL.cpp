/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Arbol AVL.- ********************/

/*
* file AVL.cpp
*/

//* AVL.cpp - Definición de la clase AVL.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib> 	// <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <fstream>
#include <iostream>

using namespace std;
#include "AVL.hpp"


//* Prototipos de funciones
	// Ingreso de datos
NodoAVL* generarNodoArbol(NodoAVL*, string);
void ingresarDato(NodoAVL*&, bool&, string);
void abrirPdb();
	// Reestructurar árbol
void reestructuraIzqda(NodoAVL*&, bool&);
void reestructuraDerecha(NodoAVL*&, bool&);
	// Buscar dato
bool buscarDato(NodoAVL*, string);
	// Eliminar datos
void eliminarDato(NodoAVL &, bool&, string);
	// Generar grafo de la estructura Arbol AVL.
void recorrerArbol(NodoAVL*, ofstream &);
void generarGrafo(NodoAVL*);


// Método de Constructores - Destructores.
Arbol::Arbol(){
	bool isAltura{false};
};

Arbol::~Arbol(){

};

// Método que genera y crea un árbol binario.
NodoAVL* Arbol::generarNodoArbol(NodoAVL *nodoPadre, string idProteinas) {
	NodoAVL *tmp;
	tmp = new NodoAVL();

	tmp->datoAVL = idProteinas;
	tmp->ptIzqdo = tmp->ptDerecho = nullptr;
	// tmp->nodoPadre = nodoPadre;
	return tmp;
};

// Método que permite el ingreso de datos estructurados al árbol.
void Arbol::ingresarDato(NodoAVL *&avlTree, bool &isAltura, string idProteinas) {
	/* El siguiente fragmento de código permite
	insertar elementos en el arbol AVL */

	// Inicializar punteros, objetos y variables de manera local
	NodoAVL *Nodo = nullptr;
	NodoAVL *Nodo1 = nullptr;
	NodoAVL *Nodo2 = nullptr;

	Nodo = avlTree;

	if (Nodo != nullptr) {		// Si el nodo no apunta a NULL, entonces entra al árbol

		if (idProteinas < Nodo->datoAVL) {
			ingresarDato(Nodo->ptIzqdo, isAltura, idProteinas);	// Llamada recursiva al método

			if (isAltura == true) {		// Si el booleano es verdadero, entra a evaluar acciones dependiendo del Factor de Equilibrio (FE) del árbol

				switch(Nodo->FE) {
					case 1:		// Si el factor de equilibrio es 1
						Nodo->FE = {0};
						isAltura = {false};
						break;

					case 0:		// Si el factor de equilibrio es 0
						Nodo->FE = {-1};
						break;

					case -1:	// Si el factor de equilibrio es -1
						Nodo1 = Nodo->ptIzqdo;	// Se invoca variable auxiliar puntero "Nodo1"
						/** SE REESTRUCTURA EL ÁRBOL */
						if (Nodo1->FE <= 0) {
							/* ROTACIÓN II */
							Nodo->ptIzqdo = Nodo1->ptDerecho;
							Nodo1->ptDerecho = Nodo;
							Nodo->FE = {0};
							Nodo = Nodo1;
							// Termina la rotación II
						} else {
							/* ROTACIÓN ID */
							Nodo2 = Nodo1->ptDerecho;
							Nodo->ptIzqdo = Nodo2->ptDerecho;
							Nodo2->ptDerecho = Nodo;
							Nodo1->ptDerecho = Nodo2->ptIzqdo;
							Nodo2->ptIzqdo = Nodo1;

							if (Nodo2->FE == -1) {
								Nodo->FE = {1};
							} else {
								Nodo->FE = {0};
							}

							if (Nodo2->FE == 1) {
								Nodo1->FE = {-1};
							} else {
								Nodo1->FE = {0};
							}

							Nodo = Nodo2;
							// Termina la rotación ID
						}

						Nodo->FE = {0};
						isAltura = {false};
						break;
				}

			}
		} else {

			if (idProteinas > Nodo->datoAVL) {
				ingresarDato(Nodo->ptDerecho, isAltura, idProteinas);	// Se regresa y se hace una llamada recursiva a la función

				if (isAltura == true) {

					switch(Nodo->FE) {
						case -1: // Si el factor de equilibrio es -1
							Nodo->FE = 0;
							isAltura = false;
							break;

						case 0:		// Si el factor de equilibrio es 0
							Nodo->FE = 1;
							break;

						case 1:	// Si el factor de equilibrio es 1
							Nodo1 = Nodo->ptDerecho;	// Se invoca variable auxiliar puntero "Nodo1"
						/** SE REESTRUCTURA EL ÁRBOL */
						if (Nodo1->FE >= 0) {
							/* ROTACIÓN DD */
							Nodo->ptDerecho = Nodo1->ptIzqdo;
							Nodo1->ptIzqdo = Nodo;
							Nodo->FE = {0};
							Nodo = Nodo1;
							// Termina la rotación DD
						} else {
							/* ROTACIÓN DI */
							Nodo2 = Nodo1->ptIzqdo;
							Nodo->ptDerecho = Nodo2->ptIzqdo;
							Nodo2->ptIzqdo = Nodo;
							Nodo1->ptIzqdo = Nodo2->ptDerecho;
							Nodo2->ptDerecho = Nodo1;

							if (Nodo2->FE == 1) {
								Nodo->FE = -1;
							} else {
								Nodo->FE = 0;
							}

							if (Nodo2->FE == -1) {
								Nodo1->FE = 1;
							} else {
								Nodo1->FE = 0;
							}

							Nodo = Nodo2;
							// Termina la rotación DI
						}

						Nodo->FE = {0};
						isAltura = {false};
						break;
					}

				}

			} else {	// La información ya está insertada
				cout << "\t La información ya se encuentra en el árbol." << endl;
			}

		}

	} else {		// Si el nodo apunta a NULL, entonces no entra al árbol
		// vector<NodoAVL> Nodo;
		// Nodo = (struct _NodoAVL*) malloc (sizeof(NodoAVL));
		Nodo = new NodoAVL;
		Nodo->ptIzqdo = nullptr;
		Nodo->ptDerecho = nullptr;
		Nodo->datoAVL = idProteinas;
		Nodo->FE = 0;
		isAltura = true;
	}
	avlTree = Nodo;
};

// Método que abre los pdbfiless pdb.txt
void Arbol::abrirPdb() {
	ifstream pdbfiles;

	// Inicializar punteros, objetos y variables de manera local
	NodoAVL *arbol = nullptr;
	Arbol *apuntadorArbol = new Arbol();

	string valorOp{"\0"};
	int integerOp{0};
	string txtPDB{"\0"};
	int i{0};

	// Menú de opciones
	do{
		cout << "\t     ------------------------------------------------ " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES - ABRIR ARCHIVOS PDB ***** " << endl;
		cout << "\t     ------------------------------------------------ " << endl;
		cout << "\n  [1]  > Abrir archivo nro. 1  - « rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_000001-025000.txt »" << endl;
		cout << "  [2]  > Abrir archivo nro. 2  - « rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_025001-050000.txt »" << endl;
		cout << "  [3]  > Abrir archivo nro. 3  - « rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_050001-075000.txt »" << endl;
		cout << "  [4]  > Abrir archivo nro. 4  - « rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_075001-100000.txt »" << endl;
		cout << "  [5]  > Abrir archivo nro. 5  - « rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_100001-125000.txt »" << endl;
		cout << "  [6]  > Abrir archivo nro. 6  - « rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_125001-150000.txt »" << endl;
		cout << "  [7]  > Abrir archivo nro. 7  - « rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_150001-170597.txt »" << endl;
		cout << "  [0]  > Regresar al menú principal. " << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";
		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		// Acciones de cada opción de menú
		if (integerOp == 1) {			//* Abrir pdbfiles nro. 1.
			printf("\033c");

			pdbfiles.open("rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_000001-025000.txt", ios::in);
			if (pdbfiles.fail()) {
				cout << "\tError: No se pudo abrir el pdbfiles o este no existe. " << endl;
				cout << "\t		Favor, intente nuevamente. " << endl;
				exit(1);
			} while (!pdbfiles.eof()) {
				getline(pdbfiles, txtPDB);
				cout << txtPDB << endl;
			} pdbfiles.close();
			cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 2) {	//* Abrir pdbfiles nro. 2.
			printf("\033c");

			pdbfiles.open("rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_025001-050000.txt", ios::in);
			if (pdbfiles.fail()) {
				cout << "\tError: No se pudo abrir el pdbfiles o este no existe. " << endl;
				cout << "\t		Favor, intente nuevamente. " << endl;
				exit(1);
			} while (!pdbfiles.eof()) {
				getline(pdbfiles, txtPDB);
				cout << txtPDB << endl;
			} pdbfiles.close();
			cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 3) {	//* Abrir pdbfiles nro. 3.
			printf("\033c");

			pdbfiles.open("rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_050001-075000.txt", ios::in);
			if (pdbfiles.fail()) {
				cout << "\tError: No se pudo abrir el pdbfiles o este no existe. " << endl;
				cout << "\t		Favor, intente nuevamente. " << endl;
				exit(1);
			} while (!pdbfiles.eof()) {
				getline(pdbfiles, txtPDB);
				cout << txtPDB << endl;
			} pdbfiles.close();
			cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 4) {	//* Abrir pdbfiles nro. 4.
			printf("\033c");

			pdbfiles.open("rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_075001-100000.txt", ios::in);
			if (pdbfiles.fail()) {
				cout << "\tError: No se pudo abrir el pdbfiles o este no existe. " << endl;
				cout << "\t		Favor, intente nuevamente. " << endl;
				exit(1);
			} while (!pdbfiles.eof()) {
				getline(pdbfiles, txtPDB);
				cout << txtPDB << endl;
			} pdbfiles.close();
			cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 5) {	//* Abrir pdbfiles nro. 5.
			printf("\033c");

			pdbfiles.open("rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_100001-125000.txt", ios::in);
			if (pdbfiles.fail()) {
				cout << "\tError: No se pudo abrir el pdbfiles o este no existe. " << endl;
				cout << "\t		Favor, intente nuevamente. " << endl;
				exit(1);
			} while (!pdbfiles.eof()) {
				getline(pdbfiles, txtPDB);
				cout << txtPDB << endl;
			} pdbfiles.close();
			cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 6) {	//* Abrir pdbfiles nro. 6.
			printf("\033c");

			pdbfiles.open("rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_125001-150000.txt", ios::in);
			if (pdbfiles.fail()) {
				cout << "\tError: No se pudo abrir el pdbfiles o este no existe. " << endl;
				cout << "\t		Favor, intente nuevamente. " << endl;
				exit(1);
			} while (!pdbfiles.eof()) {
				getline(pdbfiles, txtPDB);
				cout << txtPDB << endl;
			} pdbfiles.close();
			cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 7) {	//* Abrir pdbfiles nro. 7.
			printf("\033c");

			pdbfiles.open("rcsb_pdb_ids_690ad8320d15fa2f01c3406df613189b_150001-170597.txt", ios::in);
			if (pdbfiles.fail()) {
				cout << "\tError: No se pudo abrir el pdbfiles o este no existe. " << endl;
				cout << "\t		Favor, intente nuevamente. " << endl;
				exit(1);
			} while (!pdbfiles.eof()) {
				getline(pdbfiles, txtPDB);
				cout << txtPDB << endl;
			} pdbfiles.close();
			cin.ignore();

			cout << "\n" << endl;
		} else if (integerOp == 0){		//* Salida del programa
			printf("\033c");
			i++;
		} else {						//* Manejo de opciones inválidas
			printf("\033c");
			cout << "\t--------------------------------------------------" << endl;
			cout << "\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *****" << endl;
			cout << "\t--------------------------------------------------\n" << endl;
		};
	} while (i<1);

	// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
	apuntadorArbol->~Arbol();
	delete arbol;
	delete apuntadorArbol;
	arbol = nullptr;
	apuntadorArbol = nullptr;
};

// Método que reestructura el árbol por la ptIzqdo.
void Arbol::reestructuraIzqda(NodoAVL *&avlTree, bool &isAltura) {
	/* Reestructura el árbol cuando la altura de la rama
	ptIzqdo disminuye y el FE del NodoAVL es igual a 1 */

	// Inicializar punteros, objetos y variables de manera local
	NodoAVL *Nodo = nullptr;
	NodoAVL *Nodo1 = nullptr;
	NodoAVL *Nodo2 = nullptr;

	Nodo = avlTree;

	if (isAltura == true) {		// Si el booleano es verdadero, entra a evaluar acciones dependiendo del Factor de Equilibrio (FE) del árbol
		switch(Nodo->FE) {
			case -1: // Si el factor de equilibrio es -1
				Nodo->FE = {0};
				break;

			case 0:		// Si el factor de equilibrio es 0
				Nodo->FE = {1};
				isAltura = false;
				break;

			case 1: 	// Si el factor de equilibrio es 1
				Nodo1 = Nodo->ptDerecho;	// Se invoca variable auxiliar puntero "Nodo1"
			/** SE REESTRUCTURA EL ÁRBOL */
				if (Nodo1->FE >= 0) {
					/* ROTACIÓN DD */
					Nodo->ptDerecho = Nodo1->ptIzqdo;
					Nodo1->ptIzqdo = Nodo;

					switch (Nodo1->FE) {
						case 0:		// Si el factor de equilibrio es 0
							Nodo->FE = {1};
							Nodo1->FE = {-1};
							isAltura = {false};
							break;

						case 1: 	// Si el factor de equilibrio es 1
							Nodo->FE = {0};
							Nodo1->FE = {0};
							break;
					}
					Nodo = Nodo1;
					// Termina la rotación DD
				} else {
					/* ROTACIÓN DI */
					Nodo2 = Nodo1->ptIzqdo;
					Nodo->ptDerecho = Nodo2->ptIzqdo;
					Nodo2->ptIzqdo = Nodo;
					Nodo1->ptIzqdo = Nodo2->ptDerecho;
					Nodo2->ptDerecho = Nodo1;

				if (Nodo2->FE == 1) {
					Nodo->FE = {-1};
				} else {
					Nodo->FE = {0};
				}

				if (Nodo2->FE == -1) {
					Nodo1->FE = {1};
				} else {
					Nodo1->FE = {0};
				}

				Nodo = Nodo2;
				Nodo2->FE = {0};
					// Termina la rotación DI
				}
				break;
		}
	}
	avlTree = Nodo;
};


// Método que reestructura el árbol por la ptDerecho.
void Arbol::reestructuraDerecha(NodoAVL *&avlTree, bool &isAltura) {
	/* Reestructura el árbol cuando la altura de la rama
	ptDerecho disminuye y el FE del NodoAVL es igual a -1 */

	// Inicializar punteros, objetos y variables de manera local
	NodoAVL *Nodo = nullptr;
	NodoAVL *Nodo1 = nullptr;
	NodoAVL *Nodo2 = nullptr;

	Nodo = avlTree;

	if (isAltura == true) {		// Si el booleano es verdadero, entra a evaluar acciones dependiendo del Factor de Equilibrio (FE) del árbol
		switch(Nodo->FE) {
			case 1: // Si el factor de equilibrio es 1
				Nodo->FE = {0};
				break;

			case 0:		// Si el factor de equilibrio es 0
				Nodo->FE = {-1};
				isAltura = false;
				break;

			case -1: 	// Si el factor de equilibrio es -1
				Nodo1 = Nodo->ptIzqdo;	// Se invoca variable auxiliar puntero "Nodo1"
			/** SE REESTRUCTURA EL ÁRBOL */
				if (Nodo1->FE <= 0) {
					/* ROTACIÓN II */
					Nodo->ptIzqdo = Nodo1->ptDerecho;
					Nodo1->ptDerecho = Nodo;

					switch (Nodo1->FE) {
						case 0:		// Si el factor de equilibrio es 0
							Nodo->FE = {-1};
							Nodo1->FE = {1};
							isAltura = {false};
							break;

						case -1: 	// Si el factor de equilibrio es 1
							Nodo->FE = {0};
							Nodo1->FE = {0};
							break;
					}
					Nodo = Nodo1;
					// Termina la rotación II
				} else {
					/* ROTACIÓN ID */
					Nodo2 = Nodo1->ptDerecho;
					Nodo->ptIzqdo = Nodo2->ptDerecho;
					Nodo2->ptDerecho = Nodo;
					Nodo1->ptDerecho = Nodo2->ptIzqdo;
					Nodo2->ptIzqdo = Nodo1;

				if (Nodo2->FE == -1) {
					Nodo->FE = {1};
				} else {
					Nodo->FE = {0};
				}

				if (Nodo2->FE == 1) {
					Nodo1->FE = {-1};
				} else {
					Nodo1->FE = {0};
				}

				Nodo = Nodo2;
				Nodo2->FE = {0};
					// Termina la rotación ID
				}
				break;
		}
	}
	avlTree = Nodo;
};

// Método que busca el dato específico en el árbol - FORMA BOOLEANA.
bool Arbol::buscarDato(NodoAVL *avlTree, string idProteinas) {
	if (avlTree == nullptr) {
		return false;
	} else if (avlTree->datoAVL == idProteinas) {
		return true;
	} else if (avlTree->datoAVL < idProteinas){
		return buscarDato(avlTree->ptDerecho, idProteinas);
	} else {
		return buscarDato(avlTree->ptIzqdo, idProteinas);
	}
};

// Método que elimina dato(s) en el árbol.
void Arbol::eliminarDato(NodoAVL *&avlTree, bool &isAltura, string idProteinas) {
	/* Elimina un elemento del árbol balanceado, utilizando las
	funciones: "reestructuraIzqda" y "reestructuraDerecha" */

	// Inicializar punteros, objetos y variables de manera local
	NodoAVL *Nodo = nullptr;
	NodoAVL *tmp = nullptr;
	NodoAVL *aux1 = nullptr;
	NodoAVL *aux2 = nullptr;

	Nodo = avlTree;

	if (Nodo != nullptr) {	// Si el nodo no apunta a NULL, entonces entra al árbol

		if (idProteinas < Nodo->datoAVL){
			eliminarDato(Nodo->ptIzqdo, isAltura, idProteinas);	// Llamada recursiva al método "eliminarDato()" y "reestructuraIzqda()"
			reestructuraIzqda(Nodo, isAltura);
		} else {
			if (idProteinas > Nodo->datoAVL) {
				eliminarDato(Nodo->ptDerecho, isAltura, idProteinas); // Llamada recursiva al método "eliminarDato()" y "reestructuraDerecha()"
				reestructuraDerecha(Nodo, isAltura);
			} else {
				tmp = Nodo;
				isAltura = {true};

				if (tmp->ptDerecho == nullptr) {
					Nodo = tmp->ptIzqdo;
				} else {
					if (tmp->ptIzqdo == nullptr) {
						Nodo = tmp->ptDerecho;
					} else {
						aux1 = Nodo->ptIzqdo;
						isAltura = {false};
					}

					while (aux1->ptDerecho != nullptr){
						aux2 = aux1;
						aux1 = aux1->ptDerecho;
						isAltura = {true};
					}

					Nodo->datoAVL = aux1->datoAVL;
					tmp = aux1;

					if (isAltura == true) {		// Si el booleano es verdadero, entra a ejecutar acciones según condiciones
						aux2->ptDerecho = aux1->ptIzqdo;
					} else {
						Nodo->ptIzqdo = aux1->ptIzqdo;
					}
					reestructuraDerecha(Nodo->ptIzqdo, isAltura);
				}
			}
			delete tmp;
		}
	} else {
		cout << "\t El nodo no se encuentra en el árbol. " << endl;
	}
};

// Método que recorre el árbol binario
void Arbol::recorrerArbol(NodoAVL* avlTree, ofstream &pdbfiles) {
	if (avlTree != nullptr){
		pdbfiles << "\n" << '"' << avlTree->datoAVL << '"' << "->" << '"' << avlTree->datoAVL << '"' << "[label=" << avlTree->FE << "]" << ";";

		if (avlTree->ptIzqdo != nullptr){
			pdbfiles << "\n" << '"' << avlTree->datoAVL << '"' << "->" << '"' << avlTree->ptIzqdo->datoAVL << '"' << ";";
		} else {
			pdbfiles << "\n" << '"' << avlTree->datoAVL << "i" << '"' << " [shape=point];";
			pdbfiles << "\n" << '"' << avlTree->datoAVL << '"' << "->" << '"' << avlTree->datoAVL << "i" << '"';
		}

		if (avlTree->ptDerecho != nullptr){
			pdbfiles << "\n" << '"' << avlTree->datoAVL << '"' << "->" << '"' << avlTree->ptDerecho->datoAVL << '"' << ";";
		} else {
			pdbfiles << "\n" << '"' << avlTree->datoAVL << "d" << '"' << " [shape=point];";
			pdbfiles << "\n" << '"' << avlTree->datoAVL << '"' << "->" << '"' << avlTree->datoAVL << "d" << '"';
		}

		recorrerArbol(avlTree->ptIzqdo, pdbfiles);
		recorrerArbol(avlTree->ptDerecho, pdbfiles);
	}
};

// Método que genera grafo de la estructura
void Arbol::generarGrafo(NodoAVL *avlTree) {
	pdbfiles.open("grafo.txt");

	if(pdbfiles.fail()){
		cout << "No se puedo abrir el pdbfiles";
		exit(1);
	}
	pdbfiles << "digraph G {" << endl;
	pdbfiles << "node [style=filled fillcolor=yellow];";
	recorrerArbol(avlTree, pdbfiles);
	pdbfiles << "}";
	pdbfiles.close();

	system("dot -Tpng -ografo.png grafo.txt &");
	system("eog grafo.png &");
};
