/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Arbol AVL.- ********************/

/*
* file AVL.hpp
*/

//* AVL.hpp - Declaración de la clase AVL.

#ifndef AVL_HPP
#define AVL_HPP

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib> 	// <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <fstream>
#include <iostream>

#define TRUE 1
#define FALSE 0

using namespace std;


// Estructura del (los) nodoAVL(s).
typedef struct _NodoAVL{
	string datoAVL{"\0"};			// Variable en donde se almacenará el ID, ya sea a partir de la extracción de los .txt o del ingreso de valores.
	int FE{};						// Variable que guardará el factor equilibrio.
	struct _NodoAVL* ptDerecho;		// Apunta a la derecha del nodo.
	struct _NodoAVL* ptIzqdo;		// Apunta a la izquierda del nodo.
	// struct _NodoAVL* nodoPadre;		// Identifica nodos Padres de cada estamento en el árbol
} NodoAVL;

// Clase Arbol: Genera árbol AVL y sus métodos (funciones).
class Arbol{
	// Miembros privados
	private:    // Atributos
		ofstream pdbfiles;
		bool isAltura{false};


	// Miembros protegidos
	protected:

	// Miembros públicos
	public:    // Constructores - Destructores y Métodos
	// Constructores - Destructores
		Arbol();   // Constructor por defecto
		~Arbol();  // Destructor

	// Métodos - Funciones
			// Ingreso de datos
		NodoAVL* generarNodoArbol(NodoAVL *nodoPadre, string idProteinas);
		void ingresarDato(NodoAVL *&avlTree, bool &isAltura, string idProteinas);
		void abrirPdb();
			// Reestructurar árbol
		void reestructuraIzqda(NodoAVL *&avlTree, bool &isAltura);
		void reestructuraDerecha(NodoAVL *&avlTree, bool &isAltura);
			// Buscar dato
		bool buscarDato(NodoAVL *avlTree, string idProteinas);
			// Eliminar datos
 		void eliminarDato(NodoAVL *&avlTree, bool &isAltura, string idProteinas);
			// Generar grafo de la estructura Arbol AVL.
		void recorrerArbol(NodoAVL *avlTree, ofstream &files);
		void generarGrafo(NodoAVL *avlTree);
};

#endif
