# Laboratorio nro.2 - Unidad I

El siguiente proyectos fue escrito en C/C++, y consiste en 1 programa cuya implementación se basa en el uso de arreglos y clases, añadiendo también las pilas.


## Historia

Escrito y desarrollado por el estudiante de Ing. Civil en Bioinformática Jorge Carrillo Silva, utilizando para ello el lenguaje de programación C/C++.


## Para empezar

_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos

Es requisito cerciorarse de tener instalado C/C++ y su compilador correspondiente en el equipo donde ejecutará el proyecto. Es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones).


### Instalación y ejecución

_Para ejecutar (sin instalar previamente) el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git)
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U1/Lab2", que contiene la carpeta "Lab2-U1", además del archivo 'README.md' y el Instructivo (Pauta) del proyecto.
4. Abrir una terminal en la carpeta contenedora (Lab2-U1).
3. Para lanzar el programa, deberá ingresar el (los) siguiente(s) comando(s) en su terminal

- Lab2-U1
```
./Lab2-U1
```


_Para instalar el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git)
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U1/Lab2", que contiene la carpeta "Lab2-U1", además del archivo 'README.md' y el Instructivo del proyecto.
3. Abrir una terminal (con permisos de Superusuario 'sudo ~ su') en la carpeta contenedora (Lab2-U1).
4. Para instalar el programa deberá ingresar el (los) siguiente(s) comando(s) en su terminal

```
make install
```

5. Para lanzar el programa, deberá ingresar el siguiente comando en su terminal

- Lab2-U1
```
./Lab2-U1
```


## Codificación

Soporta la codificación estándar UTF-8


## Construido con

* [Visual Studio Code](https://code.visualstudio.com) - IDE utilizado para el desarrollo del proyecto.


## Autores️

* **Jorge Cristóbal Carrillo Silva** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895)


## Licencia

Este proyecto está sujeto bajo la Licencia (GNU GPL v.3)
