/******************** PILA.- ********************/

/*
* file Pila.hpp
*/

#ifndef PILA_HPP
#define PILA_HPP

#include <iostream>

using namespace std;


class Pila{
    private:    // Atributos
        int maxTamanio{0};
        int topePila{0};
        int *array{NULL};
        bool isBand{false};

    public:    // Constructores y Métodos
    /* Constructores */
        Pila();
        Pila(int maxTamanio);
        void pila_vacia(int topePila, bool isBand);
        void pila_llena(int topePila, int maxTamanio, bool isBand);
        int push();
        int pop();
        void mostrarPila();
};
#endif
