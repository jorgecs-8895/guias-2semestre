/******************** LAB2-U1.- ********************/

/*
* g++ Instanciador.cpp -o Lab2-U1
* make Instanciador.cpp
*      Instanciador.o
*      Lab2-U1
*/

//Llamar e incluir librerías necesarias
#include <cstdlib>  // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>

using namespace std;
#include "Pila.cpp"


/* Creación de funciones de ingreso e impresión de datos */
void menuIngreso(){
	printf("\033c");
    cout << "\nAgregar/push  [1]" << endl;
    cout << "Remover/pop     [2]" << endl;
    cout << "Ver pila        [3]" << endl;
    cout << "Salir           [0]" << endl;
    cout << "-------------------" << endl;
	cout << "Opción: ";
};

/* Función principal */
int main(int argc, char **argv){
	printf("\033c");

	string maxTamanioSTR{"\0"}, valorOpcionSTR{"\0"};
	int maxTamanio{0}, valorOpcion = 0, i{0};

	cout << " Ingrese tamaño máximo de la pila: ";
	getline(cin, maxTamanioSTR);
	maxTamanio = atoi(maxTamanioSTR.c_str());

	Pila pila = Pila(maxTamanio);

	do{
		printf("\033c");
		cout << "\nAgregar/push    [1]" << endl;
		cout << "Remover/pop     [2]" << endl;
		cout << "Ver pila        [3]" << endl;
		cout << "Salir           [0]" << endl;
		cout << "-------------------" << endl;
		cout << "Opción: ";
		cin >> valorOpcionSTR;
		cin.ignore();
		// atoi convierte la variable string a su valor int asociado,
		// y así luego evalua en función del valor la opción de menú seleccionada
		valorOpcion = atoi(valorOpcionSTR.c_str());

		if (valorOpcion == 1){
			pila.push();
		} else if(valorOpcion == 2){
			pila.pop();
		} else if(valorOpcion == 3){
			pila.mostrarPila();
		} else if (valorOpcion == 0){
			printf("\033c");
			cout << "\n\t¡SALIDA EXITOSA!\n" << endl;
			break;
		} else{
			printf("\033c");
			cout << "\t--------------------------------------" << endl;
			cout << "\tVALOR INVÁLIDO, FAVOR INTENTE DE NUEVO" << endl;
			cout << "\t--------------------------------------\n" << endl;
		}
	} while(i<1);

	return 0;
};