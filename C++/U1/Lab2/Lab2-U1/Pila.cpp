/******************** PROGRAMA EJ3.- ********************/


//Llamar e incluir librerías necesarias
#include <cstdlib>  // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
                    // como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Pila.hpp"

// Variables globales
string datos{"\0"};
int datosEnteros{0}, aux{0};


/* Constructores */
// Constructor por defecto
Pila::Pila(){
    int maxTamanio{0};
    int tope{0};
    int *array{NULL};
    bool isBand{false};
};

// Constructor por paramétros
Pila::Pila(int maxTamanio){
    this->maxTamanio = maxTamanio;
    //this->tope = tope;
    this->array = new int[maxTamanio];
    //this->isBand = isBand;
};

// Configurar comportamiento cuando la pila está vacía
void Pila::pila_vacia(int topePila, bool isBand){
    if (topePila == 0){
        isBand = true;
    } else{
        isBand = false;
    }
};

// Configurar comportamiento cuando la pila está llena
void Pila::pila_llena(int topePila, int maxTamanio, bool isBand){
    if (topePila == maxTamanio){
        isBand = true;
    } else {
        isBand = false;
    }
};

// Ingreso de valores a la pila
int Pila::push(){
    pila_llena(topePila, maxTamanio, isBand);

    if (isBand == true){
        cout << "\n\t Desbordamiento, Pila llena " << endl;
    } else {
        while (aux < maxTamanio){
            cout << "\n Ingrese valor: ";
            getline(cin, datos);
            datosEnteros = atoi(datos.c_str());
            array[topePila] = datosEnteros;
            aux++;
        }
    }

    // Liberar espacios de memoria utilizados
    delete[] array;
    array = NULL;

    return 0;
};

int Pila::pop() {
    pila_vacia(topePila, isBand);

    if (isBand == true){
        cout << "\n\t Subdesbordamiento, Pila vacía " << endl;
    } else {
        datosEnteros = array[topePila];
        topePila = topePila - 1;
        cout << "\n Elemento eliminado: " << datosEnteros << endl;
    }

    // Liberar espacios de memoria utilizados
    delete[] array;
    array = NULL;

    return 0;
};

void Pila::mostrarPila(){
    for (aux=0; aux<maxTamanio; ++aux){
        cout << "\n|" << array[aux] << "|" << endl;
    }
};

