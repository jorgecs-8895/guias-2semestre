/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Ingredientes.- ********************/

/*
* file Ingredientes.hpp
*/

// Ingredientes.hpp - Declaración de la clase Ingredientes.

#ifndef INGREDIENTES_HPP
#define INGREDIENTES_HPP

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;


// Estructura del(los) nodo(s).
typedef struct _NodoIngredientes{
	string nombreIngredientes;
	struct _NodoIngredientes *sig;
} NodoIngredientes;

// Clase Ingredientes
class Ingredientes{
	// Miembros privados
	private:    // Atributos
		bool banderaEscogerIngredientes = false;
		NodoIngredientes *head = nullptr;
		NodoIngredientes *tail = nullptr;
		NodoIngredientes *elementoBorrar;
		NodoIngredientes *antecesorElemento = nullptr;

	// Miembros públicos
	public:    // Constructores - Destructores y Métodos
	// Constructores - Destructores
		Ingredientes();   // Constructor por defecto
		~Ingredientes();  // Destructor
	// Métodos
		void generarNodoIngredientes(NodoIngredientes *&listaIngredientes, string datoIngredientes);
		void borrarIngredientes(NodoIngredientes *&listaIngredientes, string datoIngredientes);
		void imprimirIngredientes(NodoIngredientes *listaIngredientes);
};

#endif
