/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA EJ2.- ********************/

/*
* g++ Main.cpp -o Ej2
* make Main.cpp
*      Main.o
*      Ej2
*/

// Main.cpp - Programa que utiliza la clase Postres.

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Postres.cpp"


// Método menú de Postres
void menuIngresoPostres() {
	// Inicializar punteros, objetos y variables de manera local
	NodoPostres *lista = nullptr;
	Postres *listaPostres = new Postres();

	string valorOp{"\0"};
	int integerOp{0};
	string nombrePostres{"\0"};
	int i{0};

	// Menú de opciones
	do{
		cout << "\t     -------------------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES - POSTRES ***** " << endl;
		cout << "\t     -------------------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de postres. " << endl;
		cout << "  [2]  > Mostrar lista de postre(s)." << endl;
		cout << "  [3]  > Seleccionar postre(s) de la lista." << endl;
		cout << "  [4]  > Borrar postre(s) de la lista." << endl;
		cout << "  [5]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";
		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		// Acciones de cada opción de menú
		if (integerOp == 1) {    // Ingreso de valores
			printf("\033c");
			cout << "  > Ingrese nombre(s): ";
			getline(cin, nombrePostres);
			listaPostres->generarNodoPostres(lista, nombrePostres);
			listaPostres->imprimirPostres(lista);
			// cin.ignore();
		} else if (integerOp == 2) {	// Impresión de lista
			printf("\033c");
			cout << "\t Lista de postres insertados en la lista. " << endl;
			listaPostres->imprimirPostres(lista);
		} else if (integerOp == 3) {	//  Seleccionar postres de la lista.
			printf("\033c");
			cout << "  > Ingrese postre a seleccionar: ";
			getline(cin, nombrePostres);
			listaPostres->seleccionarPostres(lista, nombrePostres);
			// cin.ignore();
		} else if (integerOp == 4) {	// Borrar postres de la lista
			printf("\033c");
			cout << "  > Ingrese elemento a borrar: ";
			getline(cin, nombrePostres);
			cout << "\t Pre-borrado de elementos. " << endl;
			listaPostres->imprimirPostres(lista);
			listaPostres->borrarPostres(lista, nombrePostres);
			cout << "\t Post-borrado de elementos. " << endl;
			listaPostres->imprimirPostres(lista);
		} else if (integerOp == 5){		// Salida del programa
			printf("\033c");
			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;
			i++;
		} else {						// Manejo de opciones inválidas
			printf("\033c");
			cout << "\t--------------------------------------" << endl;
			cout << "\tVALOR INVÁLIDO, FAVOR INTENTE DE NUEVO" << endl;
			cout << "\t--------------------------------------\n" << endl;
		}
	} while (i<1);

	// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
	listaPostres->~Postres();
	delete lista;
	delete listaPostres;
	lista = nullptr;
	listaPostres = nullptr;
};

int main() {
	printf("\033c");
	// Se invoca la función menú
	menuIngresoPostres();
	return 0;
};