/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Ingredientes EJ2.- ********************/

/*
* file Ingredientes.cpp
*/

// Ingredientes.cpp - Definición de los métodos de la clase Ingredientes.

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Ingredientes.hpp"


// Constructores - Destructores
Ingredientes::Ingredientes(){};

Ingredientes::~Ingredientes() {
	delete[] head;        // Libera la memoria reservada a cabeza
	delete[] tail;        // Libera la memoria reservada a cola
};

// Método de generación, ordenamiento y enlazamiento de nodos de la listaIngredientes enlazada
void Ingredientes::generarNodoIngredientes(NodoIngredientes *&listaIngredientes, string datoIngredientes) {
	NodoIngredientes *tmp;                  // Generamos puntero
	tmp = new NodoIngredientes();           // Reservamos espacio de memoria para el puntero (la listaIngredientes)
	tmp->nombreIngredientes = datoIngredientes;    // Asignamos dato(s) a insertar en la listaIngredientes

	NodoIngredientes *head = listaIngredientes;
	NodoIngredientes *tail;

	// Las condiciones generan que los elementos se inserten de manera ordenada en la listaIngredientes
	while ((head != nullptr) && (head->nombreIngredientes < datoIngredientes)) {
		tail = head;
		head = head->sig;
	} if (listaIngredientes == head) {
		listaIngredientes = tmp;
	} else {
		tail->sig = tmp;
	} tmp->sig = head;

	cout << "\n\t----- Se ha insertado el nombre:  < " << datoIngredientes << " >  a la listaIngredientes. -----" << endl;
};


// Método que imprime y muestra los datos ingresados por el usuario.
void Ingredientes::imprimirIngredientes(NodoIngredientes *listaIngredientes) {
	NodoIngredientes *tmp = new NodoIngredientes();
	tmp = listaIngredientes;

	while (tmp != nullptr) {
		cout << "[" << tmp->nombreIngredientes << "] - ";
		tmp = tmp->sig;
	} if (tmp == nullptr) {
		cout << "[NULL]";
	} cout << "\n\n" << endl;
};

// Método que borra los ingredientes ingresados por el usuario.
void Ingredientes::borrarIngredientes(NodoIngredientes *&listaIngredientes, string datoIngredientes) {
	if (listaIngredientes != nullptr) {
		elementoBorrar = listaIngredientes;

		while ((elementoBorrar != nullptr) && (elementoBorrar->nombreIngredientes != datoIngredientes)) {
			antecesorElemento = elementoBorrar;
			elementoBorrar = antecesorElemento->sig;
		}
		if (elementoBorrar == nullptr) {
			cout << "  Lo sentimos, el postre no existe." << endl;
		} else if (antecesorElemento == nullptr) {
			listaIngredientes = listaIngredientes->sig;
			delete elementoBorrar;
		} else {
			antecesorElemento->sig = elementoBorrar->sig;
			delete elementoBorrar;
		}
	}
};
