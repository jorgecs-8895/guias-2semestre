/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Postres.- ********************/

/*
* file Postres.cpp
*/

// Postres.cpp - Definición de los métodos de la clase Postres.

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Postres.hpp"
#include "Ingredientes.cpp"


// Constructores - Destructores
Postres::Postres(){};

Postres::~Postres() {
	delete[] head;        // Libera la memoria reservada a cabeza
	delete[] tail;        // Libera la memoria reservada a cola
};

// Método de generación, ordenamiento y enlazamiento de nodos de la lista enlazada
void Postres::generarNodoPostres(NodoPostres *&listaPostres, string datoPostres) {
	NodoPostres *tmp;                  // Generamos puntero
	tmp = new NodoPostres();           // Reservamos espacio de memoria para el puntero (la listaPostres)
	tmp->nombrePostres = datoPostres;    // Asignamos dato(s) a insertar en la lista

	NodoPostres *head = listaPostres;
	NodoPostres *tail;

	// Las condiciones generan que los elementos se inserten de manera ordenada en la listaPostres
	while ((head != nullptr) && (head->nombrePostres < datoPostres)) {
		tail = head;
		head = head->sig;
	}
	if (listaPostres == head) {
		listaPostres = tmp;
	} else {
		tail->sig = tmp;
	} tmp->sig = head;

	cout << "\n\t----- Se ha insertado el nombre:  < " << datoPostres << " >  a la listaPostres. -----" << endl;
};

// Método que busca y selecciona los postres.
void Postres::seleccionarPostres(NodoPostres *listaPostres, string datoPostres) {
	NodoPostres *tmp;                  // Generamos puntero
	tmp = new NodoPostres();           // Reservamos espacio de memoria para el puntero (la listaPostres)
	tmp = listaPostres;

	while ((tmp != nullptr) && (listaPostres->nombrePostres <= datoPostres)) {
		if (listaPostres->nombrePostres == datoPostres) {
			banderaEscogerPostres = true;
		} tmp = tmp->sig;
	}
	if (banderaEscogerPostres == true) {
		// Inicializar punteros, objetos y variables de manera local
		NodoIngredientes *lista = nullptr;
		Ingredientes *listaIngredientes = new Ingredientes();

		string valorOp{"\0"};
		int integerOp{0};
		string nombreIngredientes{"\0"};
		int i{0};

		// Menú de opciones - Ingredientes
		do{
			cout << "\t     --------------------------------------------- " << endl;
			cout << "\t     ***** MENÚ DE OPCIONES - INGREDIENTES ***** " << endl;
			cout << "\t     --------------------------------------------- " << endl;
			cout << "\n  [1]  > Ingreso de ingrediente(s). " << endl;
			cout << "  [2]  > Mostrar lista de ingrediente(s)." << endl;
			cout << "  [3]  > Borrar ingrediente(s) de la lista." << endl;
			cout << "  [4]  > Regresar al menú Postres." << endl;
			cout << "\n - Favor, elija una opción: " << endl << "  > ";
			cin >> valorOp;
			integerOp = atoi(valorOp.c_str());
			cin.ignore();

			// Acciones de cada opción de menú
			if (integerOp == 1) {    // Ingreso de valores
				printf("\033c");
				cout << "  >  Ingrese nombre(s): ";
				getline(cin, nombreIngredientes);
				listaIngredientes->generarNodoIngredientes(lista, nombreIngredientes);
				listaIngredientes->imprimirIngredientes(lista);
				// cin.ignore();
			} else if (integerOp == 2) {	// Impresión de lista
				printf("\033c");
				cout << "  > Lista de ingredientes insertados en la lista. " << endl;
				listaIngredientes->imprimirIngredientes(lista);
			} else if (integerOp == 3) {	// Borrar postres de la lista
				printf("\033c");
				cout << "  > Ingrese elemento a borrar: ";
				getline(cin, nombreIngredientes);
				cout << "\t Pre-borrado de elementos. " << endl;
				listaIngredientes->imprimirIngredientes(lista);
				listaIngredientes->borrarIngredientes(lista, nombreIngredientes);
				cout << "\t Post-borrado de elementos. " << endl;
				listaIngredientes->imprimirIngredientes(lista);
			} else if (integerOp == 4){		// Salida del programa
				printf("\033c");
				i++;
			} else {						// Manejo de opciones inválidas
				printf("\033c");
				cout << "\t--------------------------------------" << endl;
				cout << "\tVALOR INVÁLIDO, FAVOR INTENTE DE NUEVO" << endl;
				cout << "\t--------------------------------------\n" << endl;
			}
		} while (i<1);

		// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
		listaIngredientes->~Ingredientes();
		delete lista;
		delete listaIngredientes;
		lista = nullptr;
		listaIngredientes = nullptr;
	} else {
		cout << "\n\t El postre que busca no se encuentra en la lista. " << endl;
	}
};

// Método que imprime y muestra los datos ingresados por el usuario.
void Postres::imprimirPostres(NodoPostres *listaPostres) {
	NodoPostres *tmp = new NodoPostres();
	tmp = listaPostres;

	while (tmp != nullptr) {
		cout << "[" << tmp->nombrePostres << "] - ";
		tmp = tmp->sig;
	} if (tmp == nullptr) {
		cout << "[NULL]";
	} cout << "\n\n" << endl;
};

// Método que borra los datos ingresados por el usuario.
void Postres::borrarPostres(NodoPostres *&listaPostres, string datoPostres) {
	if (listaPostres != nullptr) {
		elementoBorrar = listaPostres;

		while ((elementoBorrar != nullptr) && (elementoBorrar->nombrePostres != datoPostres)) {
			antecesorElemento = elementoBorrar;
			elementoBorrar = antecesorElemento->sig;
		}
		if (elementoBorrar == nullptr) {
			cout << "  Lo sentimos, el postre no existe." << endl;
		} else if (antecesorElemento == nullptr) {
			listaPostres = listaPostres->sig;
			delete elementoBorrar;
		} else {
			antecesorElemento->sig = elementoBorrar->sig;
			delete elementoBorrar;
		}
	}
};
