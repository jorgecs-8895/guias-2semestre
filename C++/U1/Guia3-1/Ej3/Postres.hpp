/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Postres.- ********************/

/*
* file Postres.hpp
*/

// Postres.hpp - Declaración de la clase Postres.

#ifndef POSTRES_HPP
#define POSTRES_HPP

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;


// Estructura del(los) nodo(s).
typedef struct _NodoPostres{
	string nombrePostres;
	struct _NodoPostres *sig;
} NodoPostres;

// Clase Postre
class Postres{
	// Miembros privados
	private:    // Atributos
		bool banderaEscogerPostres = false;
		NodoPostres *head = nullptr;
		NodoPostres *tail = nullptr;
		NodoPostres *elementoBorrar;
		NodoPostres *antecesorElemento = nullptr;

	// Miembros públicos
	public:    // Constructores - Destructores y Métodos
	// Constructores - Destructores
		Postres();   // Constructor por defecto
		~Postres();  // Destructor
	// Métodos
		void generarNodoPostres(NodoPostres *&listaPostres, string datoPostres);
		void seleccionarPostres(NodoPostres *listaPostres, string datoPostres);
		void borrarPostres(NodoPostres *&listaPostres, string datoPostres);
		void imprimirPostres(NodoPostres *listaPostres);
};

#endif
