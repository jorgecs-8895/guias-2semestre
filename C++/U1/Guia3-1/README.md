# Guía 3-1 - Unidad I

Los siguientes ejercicios fueron escritos en C/C++, y consisten en 3 programas cuya implementación se basa en el uso de estructuras dinámicas, específicamente, de _linked lists_ (Listas enlazadas).
- **Ejercicio 1 (Ej1)**: Un programa que solicita números y los ingresa en una lista desordenada hasta que el usuario pulsa una tecla, en ese entonces, el programa dividirá en dos categorías los nnúmeros "Positivos" y "Negativos", las que se presentarán al usuario de manera ordenada.
- **Ejercicio 2 (Ej2)**: Es un programa que crea una lista simplemente enlazada ordenada al que le ingresarán valores del tipo *string*, mostrando su contenido constantemente .
- **Ejercicio 3 (Ej3)**: Se basa en la generación de dos menúes, un principal (Postre) y un secundario (Ingredientes). El usuario es capaz de interactuar con el programa y elegir ciertas opciones según su conveniencia.


## Historia

Escrito y desarrollado por el estudiante de Ing. Civil en Bioinformática Jorge Carrillo Silva, utilizando para ello el lenguaje de programación C/C++.


## Para empezar

_Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos

Es requisito cerciorarse de tener instalado C/C++ y su compilador correspondiente en el equipo donde ejecutará el proyecto. Es recomendable que el SO de su máquina corra sobre el kernel Linux (Debian, Ubuntu, entre otras distribuciones).


### Instalación y ejecución

_Para ejecutar (sin instalar previamente) el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git).
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U1/Guia3-1", que contiene las carpetas Ej1, Ej2 y Ej3, además del archivo 'README.md' y el Instructivo del proyecto.
3. Abrir una terminal en la carpeta contenedora de los programas (Ej1, Ej2 y/o Ej3).
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para lanzar el programa, deberá ingresar el (los) siguiente(s) comando(s) en su terminal

- Dentro de la carpeta Ej1
```
./Ej1
```

- Dentro de la carpeta Ej2
```
./Ej2
```

- Dentro de la carpeta Ej3
```
./Ej3
```


_Para instalar el software:_

1. Clonar el repositorio "Guias-2Semestre" en el directorio de su preferencia (el enlace HTTPS es https://gitlab.com/jorgecs-8895/guias-2semestre.git).
2. Entrar en la carpeta clonada llamada "guias-2semestre/C++/U1/Guia3-1", que contiene las carpetas Ej1, Ej2 y Ej3, además del archivo 'README.md' y el Instructivo del proyecto.
3. Abrir una terminal (con permisos de Superusuario 'sudo ~ su') en la carpeta contenedora de los programas (Ej1, Ej2 y/o Ej3 y/o Ej3).
4. Compilar el código fuente, ingresando para ello el siguiente comando en su terminal.

```
make
```

5. Para instalar el programa deberá ingresar el siguiente comando.

```
make install
```

6. Para lanzar el programa, deberá ingresar el siguiente comando en su terminal

- Ej1
```
./Ej1
```

- Ej2
```
./Ej2
```

- Ej3
```
./Ej3
```


## Codificación

Soporta la codificación estándar UTF-8


## Construido con

* [Visual Studio Code](https://code.visualstudio.com) - IDE utilizado para el desarrollo del proyecto.


## Autores️

* **Jorge Cristóbal Carrillo Silva** - *Desarrollador - Programador* - [jorgecs-8895](https://gitlab.com/jorgecs-8895)


## Licencia

Este proyecto está sujeto bajo la Licencia (GNU GPL v.3)

