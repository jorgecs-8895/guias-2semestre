/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Listas EJ2.- ********************/

/*
* file Listas.hpp
*/

// Listas.hpp - Declaración de la clase Listas.

#ifndef LISTAS_HPP
#define LISTAS_HPP

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;


// Estructura del(los) nodo(s).
typedef struct _Nodo{
	string nombreCosas;
	struct _Nodo *sig;
} Nodo;

// Clase Lista
class Listas{
	// Miembros privados
	private:    // Atributos
		Nodo *head = nullptr;
		Nodo *tail = nullptr;

	// Miembros públicos
	public:    // Constructores - Destructores y Métodos
	// Constructores - Destructores
		Listas();   // Constructor por defecto
		~Listas();  // Destructor
	// Métodos
		void generarNodo(Nodo *&lista, string datoNombres);
		void imprimirListas(Nodo *lista);
};

#endif
