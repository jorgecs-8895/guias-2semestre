/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA EJ1.- ********************/

/*
* g++ Main.cpp -o Ej1
* make Main.cpp
*      Main.o
*      Ej1
*/

// Main.cpp - Programa que utiliza la clase Listas.

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Listas.cpp"


// Métodos menú y main.
void menuIngreso() {
	// Inicializar punteros, objetos y variables de manera local
	Nodo *lista = nullptr;
	Nodo *listaPositiva = nullptr;
	Nodo *listaNegativa = nullptr;
	Listas *lista1 = new Listas();
	Listas *lista2 = new Listas();
	Listas *lista3 = new Listas();

	string valorOp{"\0"};
	int integerOp{0};
	long long numeros{0};
	int i{0};

	do{
		// Menú de opciones
		cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de números enteros. " << endl;
		cout << "  [2]  > Impresión de listas." << endl;
		cout << "  [3]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";
		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		// Acciones de cada opción de menú
		if (integerOp == 1) {    // Ingreso de valores
			printf("\033c");
			cout << "  > Ingrese dato numérico entero: ";
			cin >> numeros;
			if (numeros >= 0) {
				lista1->generarNodo(lista, numeros);
				lista2->nodosNuevasListas(listaPositiva, numeros);
			} else {
				lista1->generarNodo(lista, numeros);
				lista3->nodosNuevasListas(listaNegativa, numeros);
			}
			lista1->imprimirListas(lista);
			cin.ignore();
		} else if (integerOp == 2) {  // Impresiones de las listas
			printf("\033c");
			cout << "\t ----- Las listas de números, separados entre valores positivos y negativos son: -----" << endl;
			cout << "\n  > VALORES POSITIVOS: " << endl;
			lista2->imprimirListas(listaPositiva);
			cout << "--------------------------------" << endl;
			cout << "\n  > VALORES NEGATIVOS: " << endl;
			lista3->imprimirListas(listaNegativa);
		} else if (integerOp == 3) {     // Salida del programa
			printf("\033c");
			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;
			i++;
		} else {    // Manejo de opciones inválidas
			printf("\033c");
			cout << "\t--------------------------------------" << endl;
			cout << "\tVALOR INVÁLIDO, FAVOR INTENTE DE NUEVO" << endl;
			cout << "\t--------------------------------------\n" << endl;
		}
	} while (i<1);

	// Se llama al destructor y se liberan los espacios de memoria previamente utilizados
	lista1->~Listas();
	delete lista;
	delete listaPositiva;
	delete listaNegativa;
	delete lista1;
	delete lista2;
	delete lista3;
	lista = nullptr;
	listaPositiva = nullptr;
	listaNegativa = nullptr;
	lista1 = nullptr;
	lista2 = nullptr;
	lista3 = nullptr;
};

int main() {
	printf("\033c");
	// Se invoca la función menú
	menuIngreso();
	return 0;
};