/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Listas EJ1.- ********************/

/*
* file Listas.cpp
*/

// Listas.cpp - Definición de los métodos de la clase Listas.

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Listas.hpp"


// Constructores - Destructores
Listas::Listas(){};

Listas::~Listas() {
	delete[] head;        // Libera la memoria reservada a cabeza
	delete[] tail;        // Libera la memoria reservada a cola
};

// Método de generación, ordenamiento y enlazamiento de nodos de la lista enlazada
int Listas::generarNodo(Nodo *&lista, long long cifra) {
	Nodo *tmp;                  // Generamos puntero
	tmp = new Nodo();           // Reservamos espacio de memoria para el puntero (la lista)
	tmp->numEnteros = cifra;    // Asignamos dato(s) a insertar en la lista

	Nodo *head = lista;
	Nodo *tail;

	// Las condiciones generan que los elementos se inserten de manera desordenada en la lista
	while (head != nullptr) {
		tail = head;
		head = head->sig;
	} if (lista == head) {
		lista = tmp;
	} else {
		tail->sig = tmp;
	} tmp->sig = head;

	cout << "\n\t----- Se ha insertado el número entero  < " << cifra << " >  a la lista. -----" << endl;

	return 0;
};

// Métoso que define nodo de generación exclusiva de las posteriores listas ordenadas ascendentemente
int Listas::nodosNuevasListas(Nodo *&lista, long long cifra) {
	Nodo *tmp;                  // Generamos puntero
	tmp = new Nodo();           // Reservamos espacio de memoria para el puntero (la lista)
	tmp->numEnteros = cifra;    // Asignamos dato(s) a insertar en la lista

	Nodo *head = lista;
	Nodo *tail;

	// Las condiciones generan que los elementos se inserten de manera desordenada en la lista
	while ((head != nullptr) && (head->numEnteros < cifra)) {
		tail = head;
		head = head->sig;
	} if (lista == head) {
		lista = tmp;
	} else {
		tail->sig = tmp;
	} tmp->sig = head;

	return 0;
};

// Método que imprime y muestra los datos ingresados por el usuario.
void Listas::imprimirListas(Nodo *lista) {
	Nodo *tmp = new Nodo();
	tmp = lista;

	while (tmp != nullptr) {
		cout << "[" << tmp->numEnteros << "] -> ";
		tmp = tmp->sig;
	}
	cout << "\n\n" << endl;
};
