/******************** PROGRAMA EJ1.- ********************/

/*
* g++ OperatoriaCuadrados.cpp -o OperatoriaCuadrados
* make OperatoriaCuadrados.cpp
*      OperatoriaCuadrados.o
*      Ej1
*/

//Llamar e incluir librerías necesarias
#include <cstdlib>  // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <math.h>   // <math.h> Para elevar los números al cuadrado

using namespace std;
#include "OperatoriaCuadrados.hpp"


/* Creación de funciones de ingreso e impresión de datos */
int leer_numeros(){
	printf("\033c");
	// Inicializar puntero en NULL
	int* numEnteros = NULL;

	string tamanioArraySTR = "\0";
	int tamanioArray = 0;

	int cuadrado = 0;
	int sumaCuadrado = 0;
	int i = 0;

	cout << "\n  > Ingrese cantidad de números a evaluar: ";
	getline(cin, tamanioArraySTR);

	tamanioArray = atoi(tamanioArraySTR.c_str());

	// Declarar arreglo de tamanio dinamico (valor tamanioArraySTR convertido a int)
	numEnteros = new int[tamanioArray];

	for(i=0; i<tamanioArray; i++){
		printf("\033c");

		cout << "\n\tFavor, ingrese números enteros a calcular. " << endl;
		cout << "\n\t    ******* Ciclo nro. " << i + 1 << " ********:\n";
		cout << "  >  ";

		cin >> numEnteros[i];
		// Aplicar cuadrado a cada número ingresado por parte del usuario
		cuadrado = pow(numEnteros[i], 2);
		sumaCuadrado = sumaCuadrado + cuadrado;
	}
	cout << "\n  La suma de los cuadrados de los " << tamanioArray << " números enteros es: " << sumaCuadrado << "." << endl;

	// Liberar espacio de memoria almacenado por el puntero
	delete [] numEnteros;
	numEnteros = NULL;

	return 0;
};


/* Función principal */
int main(int argc, char **argv){
	printf("\033c");

	string valorOpcionSTR;
	int valorOpcionINT;
	int i = 0;

	// Función impresión de menú inicial del programa
	do{
		cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de datos." << endl;
		cout << "  [2]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: ";

		cin >> valorOpcionSTR;
		cin.ignore();
		// atoi convierte la variable string a su valor int asociado,
		// y así luego evalua en función del valor la opción de menú seleccionada
		valorOpcionINT = atoi(valorOpcionSTR.c_str());

		if (valorOpcionINT == 1){
			printf("\033c");
			leer_numeros();
			i++;
		} else if (valorOpcionINT == 2){
			printf("\033c");
			cout << "\n\t¡SALIDA EXITOSA!\n" << endl;
			break;
		} else{
			printf("\033c");
			cout << "\t--------------------------------------" << endl;
			cout << "\tVALOR INVÁLIDO, FAVOR INTENTE DE NUEVO" << endl;
			cout << "\t--------------------------------------\n" << endl;
		}
	} while(i<1);

	return 0;
};
