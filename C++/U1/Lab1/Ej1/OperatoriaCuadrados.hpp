/******************** PROGRAMA EJ1.- ********************/

/*
* file OperatoriaCuadrados.hpp
*/

#ifndef OPERATORIACUADRADOS_HPP
#define OPERATORIACUADRADOS_HPP

#include <cstdlib>  // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)

#include <math.h>   // <math.h> Para elevar los números al cuadrado
#include <iostream>
#include <string>

using namespace std;


class Programa{
	private:    // Atributos
		int* numEnteros = NULL;
		int i = 0;
		string tamanioArraySTR = "\0";
		int tamanioArray = 0;
		int cuadrado = 0;
		int sumaCuadrado = 0;

	public:    // Constructores y Métodos
	/* Constructores */
		Programa();

	/* Métodos */
		int leer_numeros();

	/* Métodos getter's & setter's" */
		// SETTER'S

		// GETTER'S

};

#endif
