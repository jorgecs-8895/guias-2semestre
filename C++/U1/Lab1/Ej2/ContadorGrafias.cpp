/******************** PROGRAMA EJ2.- ********************/


//Llamar e incluir librerías necesarias
#include <cctype>
#include <cstdlib>  // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>

using namespace std;
#include "ContadorGrafias.hpp"

ContadorGrafias::ContadorGrafias(){};

void ContadorGrafias::leer_letras_frases(){
	char* frases = NULL;
	int tamanioArray = 0;
	int letraMayusculas = 0;
	int letraMinusculas = 0;
	int bandera = 0;
	int i = 0;

	cout << "\n  > Defina cantidad de frases (de al menos un caracter) que serán evaluadas:  ";
	getline(cin, tamanioArraySTR);

	tamanioArray = atoi(tamanioArraySTR.c_str());

	frases = new char[tamanioArray];

	for(i=0; i<tamanioArray; i++){
		printf("\033c");
		cout << "\n\t    ******* Ciclo nro. " << i + 1 << " ********:\n";

		cout << "\n\tFavor, ingrese frases (de al menos un caracter) a evaluar. " << endl;
		cout << "  >  ";
		cin >> frases[i];

		if(islower(frases[i])){
			letraMinusculas++;
		}
		else if(isupper(frases[i])){
			letraMayusculas++;
		}
	};

	cout << "\n  La cantidad de letras minúsculas de " << tamanioArray << " frases es: " << letraMinusculas << "." << endl;
	cout << "  La cantidad de letras mayúsculas de " << tamanioArray << " frases es: " << letraMayusculas << "." << endl;

	delete [] frases;
	frases = NULL;

};