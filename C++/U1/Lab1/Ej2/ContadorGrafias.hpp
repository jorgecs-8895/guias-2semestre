/******************** PROGRAMA EJ2.- ********************/

/*
* file ContadorGrafias.hpp
*/

#ifndef CONTADORGRAFIAS_HPP
#define CONTADORGRAFIAS_HPP

#include <iostream>

using namespace std;


class ContadorGrafias{
	private:    // Atributos
	string tamanioArraySTR = "\0";

	public:    // Constructores y Métodos
	/* Constructores */
	ContadorGrafias();
	void leer_letras_frases();
		/* Métodos getter's & setter's" */
			// SETTER'S

			// GETTER'S
};

#endif
