/******************** PROGRAMA EJ2.- ********************/

/*
* g++ InstanciadorCG.cpp -o InstanciadorCG
* make InstanciadorCG.cpp ContadorGrafias.cpp
*      InstanciadorCG.o ContadorGrafias.o
*      Ej2
*/

//Llamar e incluir librerías necesarias
#include <cstdlib>  // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>

using namespace std;
#include "ContadorGrafias.cpp"


/* Creación de funciones de ingreso e impresión de datos */
void menuIngreso(){
	printf("\033c");

	ContadorGrafias counter = ContadorGrafias();

	string valorOpcionSTR;
	int valorOpcionINT;
	int i = 0;

	// Función impresión de menú inicial del programa
	do{
		cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de frases." << endl;
		cout << "  [2]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: ";

		cin >> valorOpcionSTR;
		cin.ignore();
		// atoi convierte la variable string a su valor int asociado,
		// y así luego evalua en función del valor la opción de menú seleccionada
		valorOpcionINT = atoi(valorOpcionSTR.c_str());

		if (valorOpcionINT == 1){
			printf("\033c");
			counter.leer_letras_frases();
			i++;
		} else if (valorOpcionINT == 2){
			printf("\033c");
			cout << "\n\t¡SALIDA EXITOSA!\n" << endl;
			break;
		} else{
			printf("\033c");
			cout << "\t--------------------------------------" << endl;
			cout << "\tVALOR INVÁLIDO, FAVOR INTENTE DE NUEVO" << endl;
			cout << "\t--------------------------------------\n" << endl;
		}
	} while(i<1);
}

/* Función principal */
int main(int argc, char **argv){
	printf("\033c");
	menuIngreso();
	return 0;
};
