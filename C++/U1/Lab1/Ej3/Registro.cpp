/******************** PROGRAMA EJ3.- ********************/


//Llamar e incluir librerías necesarias
#include <cstdlib>  // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
                    // como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Registro.hpp"

/* Constructores */
Registro::Registro(){
    string nombreClientes = "\0";
    string telefonoClientes = "\0";
    double saldoClientes = 0;
    bool isMoroso = false;
};

Registro::Registro(string nombreClientes, string telefonoClientes, double saldoClientes, bool isMoroso){
    this->nombreClientes = nombreClientes;
    this->telefonoClientes = telefonoClientes;
    this->saldoClientes = saldoClientes;
    this->isMoroso = isMoroso;
};

void Registro::datos_clientes(){
    // Bloque de variables
    string tamanioArraySTR = "\0";
    int tamanioArray = 0;
    int limite = 0;
    int i = 0;

    // Bloque de entrada de variable tamanio de array y asignacion de tamanio
    cout << "  > Defina cantidad de clientes (y datos) a ingresar:  ";
    getline(cin, tamanioArraySTR);
    tamanioArray = atoi(tamanioArraySTR.c_str());
    // Se genera array y metodo de recorrer de un elemento a la vez
    Registro datosClientes[tamanioArray];
    limite = (sizeof(datosClientes)/sizeof(datosClientes[tamanioArray]));

    // Ciclo para recorrer el array, ingresar datos y luego presentar estado de salud financiera de los clientes
    while(i<tamanioArray){
        printf("\033c");
        cout << "\t ******* Datos de Cliente(s): " << i + 1 << ". ********\n" << endl;
        cout << "  >  Favor, ingrese nombre de cliente(s): ";
        getline(cin, nombreClientes);
        datosClientes[i].set_nombreClientes(nombreClientes);

        cout << "  >  Ingrese número de telefono de cliente(s): ";
        getline(cin, telefonoClientes);
        datosClientes[i].set_telefonoClientes(telefonoClientes);

        cout << "  >  Ingrese saldo de cliente(s) (Si el valor es negativo, se tratará como 'Deuda'): ";
        cin >> saldoClientes;
        datosClientes[i].set_saldoClientes(saldoClientes);
        cin.ignore();

        // Ciclo que automatiza la clasificación de salud financiera del cliente (MOROSO, NO MOROSO)
        for(int i = 0; i<limite; i++) {
            cout << "\n  >  Calculando automáticamente estado financiero de cliente(s): " << datosClientes[i].get_nombreClientes() << endl;
            if(datosClientes[i].get_saldoClientes() < 0){
                datosClientes[i].set_isMoroso(true);
                cout << "\t  ESTADO FINANCIERO DE CLIENTE: [" << datosClientes[i].get_isMoroso() << "] = MOROSO." << endl;
            } else if(datosClientes[i].get_saldoClientes() >= 0){
                datosClientes[i].set_isMoroso(false);
                cout << "\t  ESTADO FINANCIERO DE CLIENTE: [" << datosClientes[i].get_isMoroso() << "] = NO MOROSO. " << endl;
            }
        }
        i++;
    }



}