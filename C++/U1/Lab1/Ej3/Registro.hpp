/******************** PROGRAMA EJ3.- ********************/

/*
* file Registro.hpp
*/

#ifndef REGISTRO_HPP
#define REGISTRO_HPP

#include <iostream>

using namespace std;


class Registro{
    private:    // Atributos
        string nombreClientes = "\0";
        string telefonoClientes = "\0";
        double saldoClientes = 0;
        bool isMoroso = false;
    public:    // Constructores y Métodos
    /* Constructores */
        Registro();
        Registro(string nombreClientes, string telefonoClientes, double saldoClientes, bool isMoroso);
        void datos_clientes();

    /* Métodos getter's & setter's" */
        // GETTER'S
        string get_nombreClientes(){
            return this->nombreClientes;
        }
        string get_telefonoClientes(){
            return this->telefonoClientes;
        }
        double get_saldoClientes(){
            return this->saldoClientes;
        }
        bool get_isMoroso(){
            return this->isMoroso;
        };

        // SETTER'S
        void set_nombreClientes(string nombreClientes){
            this->nombreClientes = nombreClientes;
        }
        void set_telefonoClientes(string telefonoClientes){
            this->telefonoClientes = telefonoClientes;
        }
        void set_saldoClientes(double saldoClientes){
            this->saldoClientes = saldoClientes;
        }
        void set_isMoroso(bool isMoroso){
            this->isMoroso = isMoroso;
        };
};
#endif
