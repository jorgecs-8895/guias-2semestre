/******************** PROGRAMA ********************/

/*
* g++ Programa.cpp -o Programa
* make Programa.cpp Proteina.cpp Cadena.cpp Aminoacido.cpp Atomo.cpp Coordenada.cpp
*      Programa.o Proteina.o Cadena.o Aminoacido.o Atomo.o Coordenada.o
*      Protein-Database
*/

//Llamar e incluir librerías necesarias
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
// <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
// como manera de controlar ingreso erróneo de tipo de dato)
using namespace std;
#include "Programa.hpp"


/* Creación de funciones de ingreso e impresión de datos de proteínas */
void imprimir_datos_proteinas(list<Proteina> proteinas){
    printf("\033c");

    cout << "\t --------------------------------------" << endl;
    cout << "\t ***** Información de Proteína(s) ***** " << endl;
    cout << "\t -------------------------------------- " << endl;

    for(Proteina prote: proteinas){
        cout << "\n     ----- Datos de Proteína(s) -----\n";
        cout << "\n  >  Nombre de proteína(s): " << prote.get_nombre() << endl;
        cout << "  >  ID de proteína(s): " << prote.get_id() << endl;

        for(Cadena chains: prote.get_cadenas()){
            cout << "\n     ----- Datos de Cadena(s) -----\n";
            cout << "\n  >  Letra de cadena(s): " << chains.get_letra() << endl;

            for(Aminoacido aminoac: chains.get_aminoacidos()){
                cout << "\n     ----- Datos de Aminoacido(s) -----\n";
                cout << "\n  >  Nombre de aminoácido(s): " << aminoac.get_nombre() << endl;
                cout << "  >  Número de aminoácido(s): " << aminoac.get_numero() << endl;

                for(Atomo atomos: aminoac.get_atomos()){
                    cout << "\n     ----- Datos de Atomo(s) -----\n";
                    cout << "\n  >  Nombre de átomo(s): " << atomos.get_nombre() << endl;
                    cout << "  >  Número de átomo(s): " << atomos.get_numero() << endl;

                        cout << "\n     ----- Datos de Coordenadas(s) -----\n";
                        cout << "\n  >  Coordenadas 'X': " << atomos.get_coordenada().get_x() << endl;
                        cout << "  >  Coordenadas 'Y': " << atomos.get_coordenada().get_y() << endl;
                        cout << "  >  Coordenadas 'Z': " << atomos.get_coordenada().get_z() << endl;
                        cout << "\n" << endl;
                }
            }
        }
    }
};

int leer_datos_proteina(){
    // Se instancia list<Proteina> proteinas, para que el usuario
    // ingrese los valores correspondientes a cada uno de los parámetros predefinidos
    // (Proteínas, Cadenas, Aminoacidos, Átomos, Coordenadas)
    list<Proteina> proteinas;

    // Instanciar variables
    string cantListasSTR;
    int cantListasINT;
    int i = 0;

    string valorOpcionSTR;
    int valorOpcionINT;
    int bandera = 0;

    string nombreProteinas= "\0", idProteinas= "\0";
    string letraCadenas= "\0";
    string nombreAminoacidos= "\0";
    string nombreAtomos= "\0";
    int numeroAminoacidos= 0;
    int numeroAtomos= 0;
    float x= 0, y= 0, z= 0;

    // Inicializar constructores
    Proteina prote = Proteina(nombreProteinas, idProteinas);
    Cadena chains = Cadena(letraCadenas);
    Aminoacido aminoac = Aminoacido(nombreAminoacidos, numeroAminoacidos);
    Atomo atomos = Atomo(nombreAtomos,numeroAtomos);
    Coordenada ejes = Coordenada(x, y, z);


    cout << "\n  Ingrese cantidad de lista de proteínas (e información asociada) que desee ingresar: ";
    cin >> cantListasSTR;
    cin.ignore();

    cantListasINT = atoi(cantListasSTR.c_str());

    for(i = 0; i < cantListasINT; i++){
        printf("\033c");

        cout << "\n\t   ******* Lista nro. " << i + 1 << " ******** \n" << endl;

        // Bloque de datos de Proteína (lineas 102 ~ 108)
        cout << "     ----- Datos de Proteína(s) -----\n";
        cout << "\n  >  Favor, ingrese nombre de proteína(s): ";
        getline(cin, nombreProteinas);
        prote.set_nombre(nombreProteinas);
        cout << "  >  Ingrese ID de proteína(s): ";
        getline(cin, idProteinas);
        prote.set_id(idProteinas);

        // Bloque de datos de Cadena (lineas 111 ~ 114)
        cout << "\n\n\n     ----- Datos de Cadena(s) -----\n";
        cout << "\n  >  Ingrese letra(s) de cadena(s): ";
        getline(cin, letraCadenas);
        chains.set_letra(letraCadenas);

        // Bloque de datos de Aminoacidos (lineas 117 ~ 124)
        cout << "\n\n\n     ----- Datos de Aminoácidos(s) -----\n";
        cout << "\n  >  Ingrese nombre(s) de aminoácido(s): ";
        getline(cin, nombreAminoacidos);
        aminoac.set_nombre(nombreAminoacidos);
        cout << "  >  Ingrese número(s) de aminoácido(s): ";
        cin >> numeroAminoacidos;
        aminoac.set_numero(numeroAminoacidos);
        cin.ignore();

        // Bloque de datos de Atomo (lineas 125 ~ 134)
        cout << "\n\n\n     ----- Datos de Átomo(s) -----\n";
        cout << "\n  >  Ingrese nombre(s) de átomo(s): ";
        getline(cin, nombreAtomos);
        atomos.set_nombre(nombreAtomos);
        cout << "  >  Ingrese número(s) de átomo(s): ";
        cin >> numeroAtomos;
        atomos.set_numero(numeroAtomos);
        cin.ignore();

        // Bloque de datos de Coordenadas (lineas 137 ~ 149)
        cout << "\n\n\n     ----- Datos de Coordenadas(s) -----\n";
        cout << "\n  >  Ingrese valor numérico de coordenada 'X': ";
        cin >> x;
        ejes.set_x(x);
        cin.ignore();
        cout << "  >  Ingrese valor numérico de coordenada 'Y': ";
        cin >> y;
        ejes.set_y(y);
        cin.ignore();
        cout << "  >  Ingrese valor numérico de coordenada 'Z': ";
        cin >> z;
        ejes.set_z(z);
        cin.ignore();
    }
    atomos.set_coordenada(ejes);
    aminoac.add_atomos(atomos);
    chains.add_aminoacidos(aminoac);
    prote.add_cadenas(chains);
    proteinas.push_back(prote);

    // Función impresión de menú secundario del programa
    do{
        printf("\033c");
        cout << "\n\t ---------------------------- " << endl;
        cout << "\t ***** MENÚ DE OPCIONES ***** " << endl;
        cout << "\t ---------------------------- " << endl;
        cout << "\n  [1]  > Impresión de datos." << endl;
        cout << "  [2]  > Salir del programa." << endl;
        cout << "\n - Favor, elija una opción: " << endl;

        cin >> valorOpcionSTR;
        cin.ignore();
        // atoi convierte la variable string a su valor int asociado,
        // y así luego evalua en función del valor la opción de menú seleccionada
        valorOpcionINT = atoi(valorOpcionSTR.c_str());

        if (valorOpcionINT == 1){
            printf("\033c");
            imprimir_datos_proteinas(proteinas);
            bandera++;
        }
        else if (valorOpcionINT == 2){
            printf("\033c");
            cout << "\n\tSALIDA EXITOSA\n" << endl;
            break;
        }
        else{
            cout << "\n\n\n\t--------------------------------" << endl;
            cout << "\tVALOR INVÁLIDO, INTENTE DE NUEVO" << endl;
            cout << "\t--------------------------------\n" << endl;
        }
    } while(i<1);

    return 0;
};


/* Función principal */
int main(int argc, char **argv){
    printf("\033c");
    // valorOpcionSTR & valorOpcionINT son los nombres de las
    // variables en donde se guarda el dato ingresado por el usuario
    string valorOpcionSTR;
    int valorOpcionINT;
    int i = 0;

    // Función impresión de menú inicial del programa
    do{
        cout << "\t ---------------------------- " << endl;
        cout << "\t ***** MENÚ DE OPCIONES ***** " << endl;
        cout << "\t ---------------------------- " << endl;
        cout << "\n  [1]  > Ingreso de datos." << endl;
        cout << "  [2]  > Salir del programa." << endl;
        cout << "\n - Favor, elija una opción: " << endl;

        cin >> valorOpcionSTR;
        cin.ignore();
        // atoi convierte la variable string a su valor int asociado,
        // y así luego evalua en función del valor la opción de menú seleccionada
        valorOpcionINT = atoi(valorOpcionSTR.c_str());

        if (valorOpcionINT == 1){
            printf("\033c");
            leer_datos_proteina();
            i++;
        }
        else if (valorOpcionINT == 2){
            printf("\033c");
            cout << "\n\t¡SALIDA EXITOSA!\n" << endl;
            break;
        }
        else{
            cout << "\n\n\n\t--------------------------------" << endl;
            cout << "\tVALOR INVÁLIDO, FAVOR INTENTE DE NUEVO" << endl;
            cout << "\t--------------------------------\n" << endl;
        }
    } while(i<1);
    return 0;
};
