/******************** AMINOACIDO.CPP ********************/

/*
* g++ Aminoacido.cpp -o Aminoacido
*/

//Llamar e incluir librerías necesarias
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
#include "Aminoacido.hpp"


// INSTANCIACIÓN DEL HEADER "AMINOACIDO.HPP"
/* Constructores */
Aminoacido::Aminoacido(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}

/* Metodos getter's & setter's */
    // SETTER'S
    void Aminoacido::set_nombre(string nombre){
        this->nombre = nombre;
    }
    void Aminoacido::set_numero(int numero){
        this->numero = numero;
    }
    void Aminoacido::add_atomos(Atomo atomo){
        this->atomos.push_back(atomo);
    }
    // GETTER'S
    string Aminoacido::get_nombre(){
        return this->nombre;
    }
    int Aminoacido::get_numero(){
        return this->numero;
    }
    list<Atomo> Aminoacido::get_atomos(){
        return this->atomos;
    }


