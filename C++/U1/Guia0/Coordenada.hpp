/******************** COORDENADA ********************/

/*
* file Coordenada.hpp
*/

#ifndef COORDENADA_HPP
#define COORDENADA_HPP

#include <iostream>
// using namespace std;


class Coordenada{
    private:    // Atributos
        float x = 0;
        float y = 0;
        float z = 0;

    public:    // Constructores y Métodos
    /* Constructores */
        Coordenada();
        Coordenada(float x, float y, float z);

    /* Métodos getter's & setter's" */
        // SETTER'S
        void set_x(float x);
        void set_y(float y);
        void set_z(float z);
        // GETTER'S
        float get_x();
        float get_y();
        float get_z();

};
#endif
