/******************** ATOMO.CPP ********************/

/*
* g++ Atomo.cpp -o Atomo
*/

//Llamar e incluir librerías necesarias
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
#include "Atomo.hpp"


// INSTANCIACIÓN DEL HEADER "ATOMO.H"
/* Constructores */
Atomo::Atomo(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}

/* Metodos getter's & setter's */
    // SETTER'S
    void Atomo::set_nombre(string nombre){
        this->nombre = nombre;
    }
    void Atomo::set_numero(int numero){
        this->numero = numero;
    }
    void Atomo::set_coordenada(Coordenada coordenada){
        this->coordenada = coordenada;
    }
    // GETTER'S
    string Atomo::get_nombre(){
        return this->nombre;
    }
    int Atomo::get_numero(){
        return this->numero;
    }
    Coordenada Atomo::get_coordenada(){
        return this->coordenada;
    }

