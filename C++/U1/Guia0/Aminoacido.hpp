/******************** AMINOACIDO ********************/

/*
* file Aminoacido.hpp
*/

#ifndef AMINOACIDO_HPP
#define AMINOACIDO_HPP

#include <iostream>
#include <list>
#include <string>
using namespace std;
#include "Atomo.hpp"


class Aminoacido{
    private:    // Atributos
        string nombre = "\0";
        int numero = 0;
        list<Atomo> atomos;

    public:    // Constructores y Métodos
    /* Constructores */
        Aminoacido(string nombre, int numero);

    /* Métodos getter's & setter's" */
        // SETTER'S
        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_atomos(Atomo atomo);
        // GETTER'S
        string get_nombre();
        int get_numero();
        list<Atomo> get_atomos();

};
#endif
