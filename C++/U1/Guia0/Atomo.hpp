/******************** ATOMO ********************/

/*
* file Atomo.hpp
*/

#ifndef ATOMO_HPP
#define ATOMO_HPP

#include <iostream>
#include <string>
using namespace std;
#include "Coordenada.hpp"


class Atomo{
    private:    // Atributos
        string nombre = "\0";
        int numero = 0;
        Coordenada coordenada;

    public:     // Constructores y Métodos
    /* Constructores */
        Atomo(string nombre, int numero);

    /* Métodos getter's & setter's" */
        // SETTER'S
        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coordenada(Coordenada coordenada);
        // GETTER'S
        string get_nombre();
        int get_numero();
        Coordenada get_coordenada();

};
#endif
