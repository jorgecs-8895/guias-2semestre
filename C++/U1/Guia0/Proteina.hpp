/******************** PROTEINA ********************/

/*
* file Proteina.hpp
*/

#ifndef PROTEINA_HPP
#define PROTEINA_HPP

#include <iostream>
#include <list>
#include <string>
using namespace std;
#include "Cadena.hpp"


class Proteina{
    private:    // Atributos
        string nombre = "\0";
        string id = "\0";
        list<Cadena> cadenas;

    public:    // Constructores y Métodos
    /* Constructores */
        Proteina(string id, string nombre);

    /* Métodos getter's & setter's" */
        // SETTER'S
        void set_nombre(string nombre);
        void set_id(string id);
        void add_cadenas(Cadena cadena);
        // GETTER'S
        string get_id();
        string get_nombre();
        list<Cadena> get_cadenas();
};
#endif
