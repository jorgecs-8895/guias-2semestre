/******************** CADENA.CPP ********************/

/*
* g++ Cadena.cpp -o Cadena
*/

//Llamar e incluir librerías necesarias
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
#include "Cadena.hpp"


// INSTANCIACIÓN DEL HEADER "CADENA.HPP"
/* Constructores */
Cadena::Cadena(string letra){
    this->letra = letra;
}

/* Metodos getter's & setter's */
    // SETTER'S
    void Cadena::set_letra(string letra){
        this->letra = letra;
    }
    void Cadena::add_aminoacidos(Aminoacido aminoacido) {
        this->aminoacidos.push_back(aminoacido);
    }
    // GETTER'S
    string Cadena::get_letra(){
        return this->letra;
    }
    list<Aminoacido> Cadena::get_aminoacidos(){
        return this->aminoacidos;
    }

