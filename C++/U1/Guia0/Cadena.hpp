/******************** CADENA ********************/

/*
* file Cadena.hpp
*/

#ifndef CADENA_HPP
#define CADENA_HPP

#include <iostream>
#include <list>
#include <string>
using namespace std;
#include "Aminoacido.hpp"


class Cadena{
    private:    // Atributos
        string letra = "\0";
        list<Aminoacido> aminoacidos;

    public:    // Constructores y Métodos
    /* Constructores */
        Cadena(string letra);

    /* Métodos getter's & setter's" */
        // SETTER'S
        void set_letra(string letra);
        void add_aminoacidos(Aminoacido aminoacido);
        // GETTER'S
        string get_letra();
        list<Aminoacido> get_aminoacidos();
};
#endif
