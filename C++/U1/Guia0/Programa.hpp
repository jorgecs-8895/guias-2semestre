/******************** PROGRAMA ********************/

/*
* file Programa.hpp
*/

#ifndef PROGRAMA_HPP
#define PROGRAMA_HPP

#include <iostream>
#include <list>
#include <string>
using namespace std;
#include "Programa.hpp"
#include "Proteina.hpp"
#include "Cadena.hpp"
#include "Aminoacido.hpp"
#include "Atomo.hpp"
#include "Aminoacido.hpp"


class Programa{
    public:    // Constructores y Métodos
        list<Proteina> proteinas;

    /* Constructores */
    Programa();

    /* Métodos getter's & setter's" */
        // SETTER'S
        void add_proteinas(Proteina proteina);
        // GETTER'S
        list<Proteina> get_proteinas();
};

#endif
