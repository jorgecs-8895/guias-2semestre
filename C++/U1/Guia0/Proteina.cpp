/******************** PROTEINA.CPP ********************/

/*
* g++ Proteina.cpp -o Proteina.
*/

//Llamar e incluir librerías necesarias
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
#include "Proteina.hpp"


// INSTANCIACIÓN DEL HEADER "PROTEINA.HPP"
/* Constructores */
Proteina::Proteina(string id, string nombre){
    this->id = id;
    this->nombre = nombre;
}

/* Metodos getter's & setter's */
    // SETTER'S
    void Proteina::set_nombre(string nombre) {
        this->nombre = nombre;
    }
    void Proteina::set_id(string id) {
        this->id = id;
    }
    void Proteina::add_cadenas(Cadena cadena) {
        this->cadenas.push_back(cadena);
    }
    // GETTER'S
    string Proteina::get_nombre(){
        return this->nombre;
    }
    string Proteina::get_id(){
        return this->id;
    }
    list<Cadena> Proteina::get_cadenas(){
        return this->cadenas;
    }

