/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA EJ2.- ********************/

/*
* g++ Main.cpp -o Ej2
* make Main.cpp
*      Main.o
*      Ej2
*/

// Main.cpp - Programa que utiliza la clase Listas.

// Llamar e incluir librerías necesarias
#include <cstdlib> // <cstdlib> Para funcionalidad "atoi" (Convertir 'string' a 'int'
					// como manera de controlar ingreso erróneo de tipo de dato)
#include <iostream>
#include <stdio.h>

using namespace std;
#include "Listas.cpp"


/* Métodos menú y main. */
void menuIngreso() {
	// Inicializar punteros, objetos y variables de manera local
	Nodo *l1 = NULL;
	Nodo *l2 = NULL;
	Nodo *l3 = NULL;
	Listas *lista1 = new Listas();
	Listas *lista2 = new Listas();
	Listas *lista3 = new Listas();

	string valorOp{"\0"};
	int integerOp{0};
	long long numeros{0};
	int i{0};

	do{
		// Menú de opciones
		cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingreso de números enteros - Lista 1. " << endl;
		cout << "  [2]  > Ingreso de números enteros - Lista 2. " << endl;
		cout << "  [3]  > Imprimir datos de las listas. " << endl;
		cout << "  [4]  > Salir del programa." << endl;
		cout << "\n - Favor, elija una opción: " << endl << "  > ";
		cin >> valorOp;
		integerOp = atoi(valorOp.c_str());
		cin.ignore();

		// Acciones de cada opción de menú
		if (integerOp == 1) {   // Ingreso de valores
			printf("\033c");
			cout << "  > Ingrese dato numérico entero: ";
			cin >> numeros;
			cout << "\n\t----- Se ha insertado el número entero  < " << numeros << " >  a la lista 1. -----" << endl;
			lista1->generarNodo(l1, numeros);
			lista1->imprimirListas(l1);
			lista3->mixListas(l3, numeros);
			cin.ignore();
		} else if (integerOp == 2) {    // Ingreso de valores
			printf("\033c");
			cout << "  > Ingrese dato numérico entero: ";
			cin >> numeros;
			cout << "\n\t----- Se ha insertado el número entero  < " << numeros << " >  a la lista 2. -----" << endl;
			lista2->generarNodo(l2, numeros);
			lista2->imprimirListas(l2);
			lista3->mixListas(l3, numeros);
			cin.ignore();
		} else if (integerOp == 3) {    // Impresion de datos de las listas.
			printf("\033c");
			cout << "\t ----- La mezcla de valores (ordenados) entre Lista 1 y Lista 2 son: -----" << endl;
			lista3->imprimirListas(l3);
		} else if (integerOp == 4) {     // Salida del programa
			printf("\033c");
			cout << "\t----------------------------" << endl;
			cout << "\t***** ¡SALIDA EXITOSA! *****" << endl;
			cout << "\t----------------------------\n" << endl;
			i++;
		} else {    // Manejo de opciones inválidas
			printf("\033c");
			cout << "\t------------------------------------------" << endl;
			cout << "\t* VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO *" << endl;
			cout << "\t------------------------------------------\n" << endl;
		}
	} while (i<1);

	// Se llama al destructor y se liberan los espacios de memoria previamente utilizados.
	lista1->~Listas();
	lista2->~Listas();
	lista3->~Listas();
	delete l1;
	delete l2;
	delete l3;
	delete lista1;
	delete lista2;
	delete lista3;
	l1 = NULL;
	l2 = NULL;
	l3 = NULL;
	lista1 = NULL;
	lista2 = NULL;
	lista3 = NULL;
};

int main() {
	printf("\033c");
	// Se invoca la función menú
	menuIngreso();
	return 0;
};