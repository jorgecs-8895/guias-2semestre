#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define LIMITE_SUPERIOR 5000
#define LIMITE_INFERIOR 1

void imprimir_vector(int a[], int n) {
  int i;

  for (i=0; i<n; i++) {
    printf ("a[%d]=%d ", i, a[i]);
  }
  printf("\n\n");
}

void ordena_seleccion(int a[], int n) {
  int i, menor, k, j;
  
  for (i=0; i<=n-2; i++) {
    menor = a[i];
    k = i;
    
    for (j=i+1; j<=n-1; j++) {
      if (a[j] < menor) {
        menor = a[j];
        k = j;
      }
    }
    
    a[k] = a[i];
    a[i] = menor;
  }
}

/* principal */
int main(int argc, char *argv[]) {
  int n = atoi(argv[1]);
  int a[n];
  int i;
  int elemento;

  clock_t t1, t2;
  double secs;
    
  // llena el vector.
  srand (time(NULL));
  for (i=0; i<n; i++) {
    elemento = (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
    a[i] = elemento;
  }

  imprimir_vector(a, n);
  
  // llama al método de ordenamiento.
  t1 = clock();
  ordena_seleccion(a, n);
  t2 = clock();

  imprimir_vector(a, n);

  secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
  printf("%.0f milisegundos\n",secs * 1000.0);

  return 0;
}
