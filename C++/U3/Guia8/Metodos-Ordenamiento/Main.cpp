/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA.- ********************/

/*
 * file Main.cpp
 * g++ Main.cpp -o Ordenamiento
 * make Main.cpp
 *      Main.o
 *      Ordenamiento
*/

//* Main.cpp - Programa que utiliza la clase Ordenamiento.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>

typedef std::chrono::high_resolution_clock Clock;

using namespace std;
#include "Ordenamiento.cpp"


//* Main
int main(int argc, char *argv[]) {
	printf("\033c");

	if (argc != 3) {	// Si el programa no recibe parámetros válidos (el nombre de archivo + valor que corresponde a cant. de nodos + ítem que determina impresión de elementos del arreglo), entonces:
		cout << "\t ----------------------------------------------------------------------------" << endl;
		cout << "\t ***** PARÁMETROS INSUFICIENTES Y/O INVÁLIDOS. FAVOR INTENTE NUEVAMENTE *****" << endl;
		cout << "\t ----------------------------------------------------------------------------\n" << endl;
		return -1;
	} else {		// De lo contrario, si se recibe la cantidad de parámetros válidos, el segundo se asume como la cantidad de nodos y se inserta en el programa automáticamente
		try {
				// Inicializar punteros, objetos y variables de manera local
			int numeroDatos{stoi(argv[1])};		// Almacena el valor del 2do parámetro (Número de elementos)
			string imprimirDatos{argv[2]};		// Guarda el valor del 3er parámetro (Imprimir o no, los vectores de elementos)

			if (numeroDatos <= 0){		// Si la cantidad de elementos y el tercer parámetro no son válidos, entonces se controla:
				cout << "\t ----------------------------------------------------------------------------" << endl;
				cout << "\t ***** NO HAY ELEMENTOS O ESTOS SON NEGATIVOS, FAVOR INTENTE NUEVAMENTE *****" << endl;
				cout << "\t ----------------------------------------------------------------------------\n" << endl;
				return -1;
			} if (imprimirDatos != "S" && imprimirDatos != "s" && imprimirDatos != "N" && imprimirDatos != "n") {
				cout << "\t ----------------------------------------------------------------------------------------------" << endl;
				cout << "\t ***** DEBE INGRESAR UN CÁRACTER VÁLIDO (‘S’ - ‘s‘ - ‘N‘ - ’n‘), FAVOR INTENTE NUEVAMENTE *****" << endl;
				cout << "\t ----------------------------------------------------------------------------------------------\n" << endl;
				return -1;
			} else {					// De lo contrario, se inicializa el programa

				cout << "\t ────────── MÉTODOS DE ORDENAMIENTO INTERNO ──────────\n" << endl;

					// Inicializar punteros, objetos y variables de manera local
				Ordenamiento *apuntadorOrd = new Ordenamiento();	// Instanciador de métodos
				int i{}, j{}, aux;		// Variables bandera - auxiliares
				int *vectorOriginal;	// Vector original
				vectorOriginal = new int [numeroDatos];				// Se le pasa el valor ingresado por el usuario al arreglo de punteros.

					// Generación de números aleatorios.
				for (i = 0; i < numeroDatos; i++) {
					vectorOriginal[i] = (rand()%numeroDatos) + 1;
				}

					// Se asigna el valor del arreglo original a las copias.
				auto vectorBurbujaMenor = vectorOriginal;
				auto vectorBurbujaMayor = vectorOriginal;
				auto vectorInsercion = vectorOriginal;
				auto vectorIBinaria = vectorOriginal;
				auto vectorSeleccion = vectorOriginal;
				auto vectorShell = vectorOriginal;
				auto vectorQuicksort = vectorOriginal;

					// Si el tercer paramétro es 'S' o 's', entonces se imprime el arreglo original
				if (imprimirDatos == "S" || imprimirDatos == "s") {
					cout << "\t   ----- ARREGLO ORIGINAL -----" << endl;
					apuntadorOrd->imprimirVector(vectorOriginal, numeroDatos);
					cout << endl;
				}

					// Tabla de tiempos de ejecución de cada algoritmo.
				cout << "\t   ----- TABLA DE TIEMPOS DE EJECUCIÓN -----" << endl;
				cout << "---------------------------------------------" << endl;
				cout << " Método              | Tiempo" << endl;
				cout << "---------------------------------------------" << endl;
				apuntadorOrd->menorBurbuja(vectorBurbujaMenor, numeroDatos);
				apuntadorOrd->mayorBurbuja(vectorBurbujaMayor, numeroDatos);
				apuntadorOrd->ordInsercion(vectorInsercion, numeroDatos);
				apuntadorOrd->binariaInsercion(vectorIBinaria, numeroDatos);
				apuntadorOrd->ordSeleccion(vectorSeleccion, numeroDatos);
				apuntadorOrd->ordShell(vectorShell, numeroDatos);
				apuntadorOrd->ordQuicksort(vectorQuicksort, numeroDatos);
				cout << "---------------------------------------------\n" << endl;

					// Si el tercer paramétro es 'S' o 's', entonces se imprimen los arreglos ordenados
				if (imprimirDatos == "S" || imprimirDatos == "s") {
					cout << "\t   ««««« Burbuja Menor »»»»»";
					apuntadorOrd->imprimirVector(vectorBurbujaMenor, numeroDatos);
					cout << "\n\t   ««««« Burbuja Mayor »»»»»";
					apuntadorOrd->imprimirVector(vectorBurbujaMayor, numeroDatos);
					cout << "\n\t   ««««« Inserción »»»»»";
					apuntadorOrd->imprimirVector(vectorInsercion, numeroDatos);
					cout << "\n\t   ««««« Inserción Binaria »»»»»";
					apuntadorOrd->imprimirVector(vectorIBinaria, numeroDatos);
					cout << "\n\t   ««««« Selección »»»»»";
					apuntadorOrd->imprimirVector(vectorSeleccion, numeroDatos);
					cout << "\n\t   ««««« Shell »»»»»";
					apuntadorOrd->imprimirVector(vectorShell, numeroDatos);
					cout << "\n\t   ««««« Quicksort »»»»»";
					apuntadorOrd->imprimirVector(vectorQuicksort, numeroDatos);
					cout << endl;
				}

					// Se liberan los espacios de memoria previamente utilizados
				delete apuntadorOrd;
				apuntadorOrd = nullptr;
			}
		} catch (const std::exception& e) {
				cout << "\t --------------------------------------------------------------------------------------" << endl;
				cout << "\t ***** EL SEGUNDO PARÁMETRO DEBE SER UN NÚMERO POSITIVO, FAVOR INTENTE NUEVAMENTE *****" << endl;
				cout << "\t --------------------------------------------------------------------------------------\n" << endl;
		}
	}

	return 0;
};