/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Métodos de Ordenamiento Interno.- ********************/

/*
 * file Ordenamiento.hpp
*/

//* Ordenamiento.hpp - Declaración de la clase Ordenamiento.

// Guardian de inclusión múltiple
#ifndef ORDENAMIENTO_HPP
#define ORDENAMIENTO_HPP

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>

typedef std::chrono::high_resolution_clock Clock;

using namespace std;


// Clase Ordenamiento: Genera los Métodos de Ordenamiento y sus funciones.
class Ordenamiento {
	// Miembros privados
	private:    // Atributos
		clock_t tiempoEjecucion;

	// Miembros protegidos
	protected:
		int numeroDatos{};				// Variable que contiene la cantidad de elementos a ordenar.

	// Miembros públicos
	public:

	/* Constructores - Destructores */
		// Constructores - Destructores
	Ordenamiento();   // Constructor por defecto
	~Ordenamiento();  // Destructor

	/* Métodos - Funciones */
		// Método que imprime los vectores
	void imprimirVector(int*& vector, int numeroDatos);

			//** MÉTODOS DIRECTOS */
		// Método que implementa Algoritmo de Intercambio - Burbuja (Método Directo O(n²))
	void menorBurbuja(int* vector, int numeroDatos);
	void mayorBurbuja(int* vector, int numeroDatos);

		// Método que implementa Algoritmo de Inserción (Método Directo O(n²))
	void ordInsercion(int* vector, int numeroDatos);

		// Método que implementa Algoritmo de Inserción Binaria (Método Directo O(n²))
	void binariaInsercion(int* vector, int numeroDatos);

		// Método que implementa Algoritmo de Selección Directa (Método Directo O(n²))
	void ordSeleccion(int* vector, int numeroDatos);

			//** MÉTODOS LOGARÍTMICOS */
		// Método que implementa Algoritmo de Shell (Método Logarítmico O(n × log(n)))
	void ordShell(int* vector, int numeroDatos);

		// Método que implementa Algoritmo de Quicksort - Ordenamiento por partición (Método Logarítmico O(n × log(n)))
	void reduceQuicksort(int* vector, int ini, int fin, int &pos);
	void ordQuicksort(int* vector, int numeroDatos);
};

#endif
