/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Métodos de Ordenamiento Interno.- ********************/

/*
 * file Ordenamiento.cpp
*/

//* Ordenamiento.cpp - Definición de la clase Ordenamiento.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>

typedef std::chrono::high_resolution_clock Clock;

using namespace std;
#include "Ordenamiento.hpp"


//* Prototipos de funciones
	// Imprimir vectores.
void imprimirVector(int*&, int );
		/** MÉTODOS DIRECTOS */
	// Algoritmo de Intercambio - Burbuja (Método Directo O(n²))
void menorBurbuja(int*, int );
void mayorBurbuja(int*, int );
	// Algoritmo de Inserción (Método Directo O(n²))
void ordInsercion(int*, int );
	// Algoritmo de Inserción Binaria (Método Directo O(n²))
void binariaInsercion(int*, int );
	// Algoritmo de Selección Directa (Método Directo O(n²))
void ordSeleccion(int*, int );
		/** MÉTODOS LOGARÍTMICOS */
	// Algoritmo de Shell (Método Logarítmico O(n × log(n)))
void ordShell(int*, int );
	// Algoritmo de Quicksort - Ordenamiento por partición (Método Logarítmico O(n × log(n)))
void reduceQuicksort(int*, int, int, int&);
void ordQuicksort(int*, int );

// Constructor
Ordenamiento::Ordenamiento() {};

// Destructor
Ordenamiento::~Ordenamiento() {};

	// Método que imprime los vectores
void Ordenamiento::imprimirVector(int*& vector, int numeroDatos) {
	/** La siguiente definición de método permite
	 * imprimir los elementos aleatorios del vector */

		// Inicializar punteros, objetos y variables de manera local
	int i;			// Variable contador

	cout << endl;
	for (i = 0; i < numeroDatos; i++) {
		cout << "a[" << i << "]= " << vector[i] << "  ";
	} cout << endl;
};

		//* MÉTODOS DIRECTOS
	// Método que implementa Algoritmo de Intercambio - Burbuja (Método Directo O(n²))
void Ordenamiento::menorBurbuja(int* vector, int numeroDatos) {
	/**  Algoritmo de Intercambio - Burbuja: En cada ciclo
	 * el menor elemento se desplaza a la izquierda del arreglo */

		// Inicializar punteros, objetos y variables de manera local
	tiempoEjecucion = clock();	//* Inicia cronómetro
	double timeBurbujaMenor;
	int i, j;					// Variable contador
	int aux;					// Variable auxiliar

	for (i = 1; i < numeroDatos; i++) {
		for (j = (numeroDatos - 1); j >= i; j--) {
			if (vector[j-1] > vector[j]) {
				aux = vector[j-1];
				vector[j-1] = vector[j];
				vector[j] = aux;
			}
		}
	}
	tiempoEjecucion = clock() - tiempoEjecucion;	//* Cierra cronómetro
	timeBurbujaMenor = double(tiempoEjecucion)*1000.0/CLOCKS_PER_SEC;
	cout << " Burbuja Menor       | " << timeBurbujaMenor << " milisegundos" << endl;
};

void Ordenamiento::mayorBurbuja(int* vector, int numeroDatos) {
	/**  Algoritmo de Intercambio - Burbuja: En cada ciclo
	 * el elemento más grande desplaza a la derecha del arreglo */

		// Inicializar punteros, objetos y variables de manera local
	tiempoEjecucion = clock();	//* Inicia cronómetro
	double timeBurbujaMayor;
	int i, j;					// Variable contador
	int aux;					// Variable auxiliar

	for (i = (numeroDatos - 1); i > 1; i--) {
		for (j = 0; j < i; j++) {
			if (vector[j] > vector[j+1]) {
				aux = vector[j];
				vector[j] = vector[j+1];
				vector[j+1] = aux;
			}
		}
	}
	tiempoEjecucion = clock() - tiempoEjecucion;	//* Cierra cronómetro
	timeBurbujaMayor = double(tiempoEjecucion)*1000.0/CLOCKS_PER_SEC;
	cout << " Burbuja Mayor       | " << timeBurbujaMayor << " milisegundos" << endl;
};

	// Método que implementa Algoritmo de Inserción (Método Directo O(n²))
void Ordenamiento::ordInsercion(int* vector, int numeroDatos) {
	/**  Algoritmo de Inserción: Ingresa los elementos del arreglo
	 * en su parte izquierda, previamente ordenada, desde el segundo
	 * hasta el n-ésimo elemento */

		// Inicializar punteros, objetos y variables de manera local
	tiempoEjecucion = clock();	//* Inicia cronómetro
	double timeInsercion;
	int i, k;			// Variable contador
	int aux;			// Variable auxiliar

	for (i = 1; i < numeroDatos; i++) {
		aux = vector[i];
		k = (i-1);

		while ((k >= 0) && (aux < vector[k])) {
			vector[k+1] = vector[k];
			k -=  1;
		}
		vector[k+1] = aux;
	}
	tiempoEjecucion = clock() - tiempoEjecucion;	//* Cierra cronómetro
	timeInsercion = double(tiempoEjecucion)*1000.0/CLOCKS_PER_SEC;
	cout << " Inserción           | " << timeInsercion << " milisegundos" << endl;

};

	// Método que implementa Algoritmo de Inserción Binaria (Método Directo O(n²))
void Ordenamiento::binariaInsercion(int* vector, int numeroDatos) {
	/** Algoritmo de Inserción Binaria: Es una mejora del método de inserción
	 * directa, ya que realiza una búsqueda binaria en lugar de una busqueda
	 * secuencial para insertar los elementos */

		// Inicializar punteros, objetos y variables de manera local
	tiempoEjecucion = clock();	//* Inicia cronómetro
	double timeInsercionBinaria;
	int i, j, m;			// Variable contador
	int aux, izq, der;		// Variable auxiliar - de posición

	for (i = 1; i < numeroDatos; i++) {
		aux = vector[i];
		izq = 0;
		der = i-1;

		while (izq <= der) {
			m = ((izq + der) / 2);

			if (aux <= vector[m]) {
				der = m - 1;
			} else {
				izq = m + 1;
			}
		} j = i-1;

		while (j >= izq) {
			vector[j+1] = vector[j];
			j = j - 1;
		} vector[izq] = aux;
	}
	tiempoEjecucion = clock() - tiempoEjecucion;	//* Cierra cronómetro
	timeInsercionBinaria = double(tiempoEjecucion)*1000.0/CLOCKS_PER_SEC;
	cout << " Inserción Binaria   | " << timeInsercionBinaria << " milisegundos" << endl;
};

	// Método que implementa Algoritmo de Selección Directa (Método Directo O(n²))
void Ordenamiento::ordSeleccion(int* vector, int numeroDatos) {
	/** Algoritmo de Selección Directa: Es el más eficiente entre los métodos directos
	 * de búsqueda, y busca el menor elemento para insertar en la primera posición,
	 *  luego el siguiente menor e inserta en la segunda posición y así sucesivamente
	 * hasta que todos los elementos hayan sido ordenados */

		// Inicializar punteros, objetos y variables de manera local
	tiempoEjecucion = clock();	//* Inicia cronómetro
	double timeSeleccion;
	int i, j, k;			// Variable contador
	int menor;				// Variable auxiliar

	for (i = 0; i <= (numeroDatos - 1); i++) {
		menor = vector[i];
		k = i;

		for (j = (i + 1); j < numeroDatos; j++) {
			if (vector[j] < menor) {
				menor = vector[j];
				k = j;
			}
		}
		vector[k] = vector[i];
		vector[i] = menor;
	}
	tiempoEjecucion = clock() - tiempoEjecucion;	//* Cierra cronómetro
	timeSeleccion = double(tiempoEjecucion)*1000.0/CLOCKS_PER_SEC;
	cout << " Selección           | " << timeSeleccion << " milisegundos" << endl;
};

		//* MÉTODOS LOGARÍTMICOS
	// Método que implementa Algoritmo de Shell (Método Logarítmico O(n × log(n)))
void Ordenamiento::ordShell(int* vector, int numeroDatos) {
	/** Algoritmo de Shell: Es una mejora del método de inserción directa, y se
	 * caracteriza por ordenar en cada ciclo, iterando de manera decreciente, lo
	 * que es más rápido */

		// Inicializar punteros, objetos y variables de manera local
	tiempoEjecucion = clock();	//* Inicia cronómetro
	double timeShell;
	int i;					// Variable contador
	int salto, aux;			// Variable auxiliar
	bool isBand;				// Variable booleana

	salto = numeroDatos;
	while (salto > 0) {
		salto = (salto / 2);
		isBand = true;

		while (isBand) {
			isBand = false;
			i = 0;

			while ((i + salto) < numeroDatos) {
				if (vector[i] > vector[i+salto]) {
					aux = vector[i];
					vector[i] = vector[i+salto];
					vector[i+salto] = aux;
					isBand = true;
				} i += 1;
			}
		}
	}
	tiempoEjecucion = clock() - tiempoEjecucion;	//* Cierra cronómetro
	timeShell = double(tiempoEjecucion)*1000.0/CLOCKS_PER_SEC;
	cout << " Shell               | " << timeShell << " milisegundos" << endl;
};

	// Método que implementa Algoritmo de Quicksort - Ordenamiento por partición (Método Logarítmico O(n × log(n)))
void Ordenamiento::reduceQuicksort(int* vector, int ini, int fin, int &pos) {
	/** Algoritmo de Quicksort - Ordenamiento por partición: Es una mejora del método de intercambio directo
	 * y consiste en tomar un elemento "X" del arreglo, ubicarlo en la posición correcta y luego ordenar
	 * de manera que a la izquierda de "X" se ubiquen aquellos elementos que son iguales o menores, y a la
	 * derecha se posicionan los elementos mayores o iguales. */

		// Inicializar punteros, objetos y variables de manera local
	int izq, der, aux;			// Variables auxiliares
	bool isBand;				// Variable booleana

	izq = ini;
	der = fin;
	pos = ini;
	isBand = true;

	while (isBand) {
		while ((vector[pos] <= vector[der]) && (pos != der)) {
			der -= 1;
		}

		if (pos == der) {
			isBand = false;
		} else {
			aux = vector[pos];
			vector[pos] = vector[der];
			vector[der] = aux;
			pos = der;

			while ((vector[pos] >= vector[izq]) && (pos != izq)) {
				izq += 1;
			}

			if (pos == izq) {
				isBand = false;
			} else {
				aux = vector[pos];
				vector[pos] = vector[izq];
				vector[izq] = aux;
				pos = izq;
			}
		}
	}
};

void Ordenamiento::ordQuicksort(int* vector, int numeroDatos) {
	/** Algoritmo de Quicksort - Ordenamiento por partición: Es una mejora del método de intercambio directo
	 * y consiste en tomar un elemento "X" del arreglo, ubicarlo en la posición correcta y luego ordenar
	 * de manera que a la izquierda de "X" se ubiquen aquellos elementos que son iguales o menores, y a la
	 * derecha se posicionan los elementos mayores o iguales. */

		// Inicializar punteros, objetos y variables de manera local
	tiempoEjecucion = clock();	//* Inicia cronómetro
	double timeQuicksort;
	int tope{0}, ini, fin, pos;								// Variables auxiliares
	int menorPila[numeroDatos], mayorPila[numeroDatos];		// Arreglos que funcionan como pilas

	menorPila[tope] = 0;
	mayorPila[tope] = numeroDatos - 1;

	while (tope >= 0) {
		ini = menorPila[tope];
		fin = mayorPila[tope];
		tope -= 1;

		reduceQuicksort(vector, ini, fin, pos);

		if (ini < (pos - 1)) {
			tope += 1;
			menorPila[tope] = ini;
			mayorPila[tope] = pos - 1;
		} if (fin > (pos + 1)) {
			tope += 1;
			menorPila[tope] = pos + 1;
			mayorPila[tope] = fin;
		}
	}
	tiempoEjecucion = clock() - tiempoEjecucion;	//* Cierra cronómetro
	timeQuicksort = double(tiempoEjecucion)*1000.0/CLOCKS_PER_SEC;
	cout << " Quicksort           | " << timeQuicksort << " milisegundos" << endl;
};