/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Métodos de Búsqueda ****************************- ********************/

/*
 * file Busqueda.hpp
*/

//* Busqueda.hpp - Declaración de la clase Busqueda.

// Guardian de inclusión múltiple.
#ifndef BUSQUEDA_HPP
#define BUSQUEDA_HPP

// Llamar e incluir librerías necesarias.
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

using namespace std;


// Estructura que genera la tabla de dispersión y un arreglo de punteros que la contiene.
struct HashTable {
	int keyClaves;
	int dataRegistros;
};

struct HashTable *arregloElementos = nullptr;


// Estructura que genera nodos para el método de encadenamiento
typedef struct _encNodo {
	int datoNumero;
	struct _encNodo *Liga;
} Nodo;


// Clase Busqueda: Genera los Métodos de Busqueda y sus funciones.
class Busqueda {
	/* Atributos */
		// Miembros privados.
	private:
		int tamanioHash;

		// Miembros protegidos.
	protected:

		// Miembros públicos.
	public:

	/* Constructores - Destructores */
		// Constructores - Destructores.
	Busqueda();   // Constructor por defecto.
	~Busqueda();  // Destructor.

	/* Métodos - Funciones */
			//* FUNCIONES GENERALES DEL PROGRAMA.
		// Método que genera función Hash.
	int dispersionHash(int tamanioArray, int& keyClaves);
	int sgdaDispersion(int tamanioArray, int& index);

		// Método que guarda espacio de memoria para el programa.
	void inicializarArreglo(int tamanioArray);

		// Método que asigna el nro primo más cercano al tamaño del arreglo para reducir probabilidad de colisiones.
	int nroprimoCercano(int tamanioArray);

		// Método que verifica si se asigna correctamente el número primo.
	int checkPrimo(int tamanioArray);

		// Método que ingresa los registros y sus key’s en el arreglo, además de resolver colisiones.
	void ingresoRegistros(string controlColisiones, int tamanioArray, int& keyClaves, int& dataRegistros);

		// Método que busca la clave específica en el arreglo.
	void buscarRegistros(int tamanioArray, int& keyClaves);

		// Método que borra claves del arreglo.
	void borrarRegistros(int tamanioArray, int& keyClaves);

		// Método que imprime el arreglo.
	void imprimirHashTable(int tamanioArray, int& keyClaves);

		// Método que recupera el tamaño del HashTable.
	void tamanioHashTable();

			//* FUNCIONES ESPECÍFICAS DE RESOLUCIÓN DE COLISIONES.
		/** PRUEBA LINEAL */
		// Método que implementa Reasignación Prueba Lineal (L).
	void pruebaLineal(int tamanioArray, int& keyClaves);
	void desplazamientoLineal(int tamanioArray, int& keyClaves);

		/** PRUEBA CUADRÁTICA */
		// Método que implementa Reasignación Prueba Cuadrática (C).
	void pruebaCuadratica(int tamanioArray, int& keyClaves);
	void desplazamientoCuadratica(int tamanioArray, int& keyClaves);

		/** REASIGNACIÓN DOBLE DIRECCIÓN HASH */
		// Método que implementa Reasignación Doble Dirección Hash (D).
	void dobleDireccion(int tamanioArray, int& keyClaves);
	void desplazamientoDobleDireccion(int tamanioArray, int& keyClaves);

		// Método que implementa Encadenamiento (E).
	void encadenamientoListas(int tamanioArray, int& keyClaves);
	void copiaArreglo(int tamanioArray, Nodo* arrayV[]);
};

#endif
