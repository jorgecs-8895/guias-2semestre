/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** Métodos de Busqueda Interno.- ********************/

/*
 * file Busqueda.cpp
*/

//* Busqueda.cpp - Definición de la clase Busqueda.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include "Busqueda.hpp"
using namespace std;


//* Prototipos de funciones
		//* FUNCIONES GENERALES DEL PROGRAMA.
	// Método que genera función Hash.
int dispersionHash(int, int&);
int sgdaDispersion(int, int&);
	// Método que guarda espacio de memoria para el programa.
void inicializarArreglo(int);
	// Método que asigna el nro primo más cercano al tamaño del arreglo para reducir probabilidad de colisiones.
int nroprimoCercano(int);
	// Método que verifica si se asigna correctamente el número primo.
int checkPrimo(int);
	// Método que ingresa los registros y sus key’s en el arreglo, además de resolver colisiones.
void ingresoRegistros(string, int, int&, int&);
	// Método que busca la clave específica en el arreglo.
void buscarRegistros(int, int&);
	// Método que borra claves del arreglo.
void borrarRegistros(int, int&);
	// Método que imprime el arreglo.
void imprimirHashTable(int, int&);
	// Método que recupera el tamaño del HashTable.
void tamanioHashTable();

		//* FUNCIONES ESPECÍFICAS DE RESOLUCIÓN DE COLISIONES.
	//** PRUEBA LINEAL */
	// Método que implementa Reasignación Prueba Lineal (L).
void pruebaLineal(int, int&);
void desplazamientoLineal(int, int&);
	//** PRUEBA CUADRÁTICA */
	// Método que implementa Reasignación Prueba Cuadrática (C).
void pruebaCuadratica(int, int&);
void desplazamientoCuadratica(int, int&);
	//** REASIGNACIÓN DOBLE DIRECCIÓN HASH */
	// Método que implementa Reasignación Doble Dirección Hash (D).
void dobleDireccion(int, int&);
void desplazamientoDobleDireccion(int, int&);
	// Método que implementa Encadenamiento (E).
void encadenamientoListas(int, int&);
void copiaArreglo(int, Nodo*[]);


// Constructor
Busqueda::Busqueda() {};

// Destructor
Busqueda::~Busqueda() {};

	// Método que genera función Hash
int Busqueda::dispersionHash(int tamanioArray, int& keyClaves) {
	/** Este método genera la función hash
	 * para almacenar los registros */

		// Inicializar punteros, objetos y variables de manera local
	int tamanioActualizado, funcionHash;

	tamanioActualizado = nroprimoCercano(tamanioArray);
	funcionHash = ((keyClaves % tamanioActualizado) + 1);

	return funcionHash;
};

	// Método que genera función Hash
int Busqueda::sgdaDispersion(int tamanioArray, int& D) {
	/** Segunda función hash dedicada a la
	 * Reasignación Doble Dirección Hash */

		// Inicializar punteros, objetos y variables de manera local
	int dobleDireccion;

	dobleDireccion = ((D + 1) % tamanioArray);

	return dobleDireccion;
};


	// Método que guarda espacio de memoria para el arreglo
void Busqueda::inicializarArreglo(int tamanioArray) {
	/** Función que reserva espacio en
	 * memoria para el HashTable */

		// Inicializar punteros, objetos y variables de manera local
	int i{0};			// Variable contador

	tamanioArray = nroprimoCercano(tamanioArray);	// El tamaño del arreglo se reconvierte en el nro primo mayor y más cercano al ingresado por el usuario.
	arregloElementos = new HashTable();				// Reservamos espacio de memoria para el arreglo.

	while (i < tamanioArray) {		// Ciclo que asigna valores vacios a cada espacio de arregloElementos
		arregloElementos[i].keyClaves = 0;
		arregloElementos[i].dataRegistros = 0;
		i++;
	}
};


	// Método que asigna el nro primo más cercano al tamaño del arreglo
int Busqueda::nroprimoCercano(int tamanioArray) {
	/** La siguiente definición asigna el nro primo
	 * más cercano al tamaño del arreglo para reducir
	 * probabilidad de colisiones */

	if (tamanioArray % 2 == 0) {
		tamanioArray++;
	}

	while (!checkPrimo(tamanioArray)) {
		tamanioArray += 2;
	}

	return tamanioArray;
};

	// Método que verifica si se asigna correctamente el número primo
int Busqueda::checkPrimo(int tamanioArray) {
	/** Este algoritmo verifica la correcta
	 * asignación de números primos */

		// Inicializar punteros, objetos y variables de manera local
	int i;			// Variable contador

	if (tamanioArray == 1 || tamanioArray == 0) {
		return 0;
	}

	for (i = 2; i < (tamanioArray / 2); i++) {
		if (tamanioArray % i == 0) {
			return 0;
		}
	}

	return 1;
};

	// Método que ingresa los registros y sus key’s en el arreglo
void Busqueda::ingresoRegistros(string controlColisiones, int tamanioArray, int& keyClaves, int& dataRegistros) {
	/** La siguiente definición permite el ingreso de
	 * los registros y las claves en el arreglo */

		// Inicializar punteros, objetos y variables de manera local
	int index{dispersionHash(tamanioArray, keyClaves)};		// Asignamos el indice mediante la función hash por división (H(K) = (K mod N) + 1).

	// Condicionales de ingreso de datos y control de colisiones.
	if (arregloElementos[index].dataRegistros == 0) {
		arregloElementos[index].keyClaves = keyClaves;
		arregloElementos[index].dataRegistros = dataRegistros;
			tamanioHash++;
		cout << "\n\t····· La clave [" << keyClaves << "] y su registro asociado han sido insertados exitosamente en el arreglo. ·····\n";
	} else if (arregloElementos[index].keyClaves == keyClaves) {
		arregloElementos[index].dataRegistros = dataRegistros;
		cout << "\n\t····· La clave [" << keyClaves << "] y su registro asociado ya existían y han sido actualizados exitosamente en el arreglo. ·····\n";
	} else {
		cout << "\n\t····· Ha ocurrido una colisión. ·····\n";

			// Si ocurre una colisión, una estructura selectiva determina a que resolución de colisiones debe invocar, basándose en lo ingresado por el usuario.
		if (controlColisiones == "L" || controlColisiones == "l") {
			desplazamientoLineal(tamanioArray, keyClaves);
		} else if (controlColisiones == "C" || controlColisiones == "c") {
			desplazamientoCuadratica(tamanioArray, keyClaves);
		} else if (controlColisiones == "D" || controlColisiones == "d") {
			desplazamientoDobleDireccion(tamanioArray, keyClaves);
		} else if (controlColisiones == "E" || controlColisiones == "e") {
			encadenamientoListas(tamanioArray, keyClaves);
		}
	}
};

	// Método que busca clave en el Hash Table
void Busqueda::buscarRegistros(int tamanioArray, int& keyClaves) {
	/** El método permite buscar La información en el HashTable
	 * mediante consultas de existencia de clave. */

		// Inicializar punteros, objetos y variables de manera local
	int index{dispersionHash(tamanioArray, keyClaves)};		// Asignamos el indice mediante la función hash por división (H(K) = (K mod N) + 1).
	int i;			// Variable contador

	// Ciclo que busca la key en el Hash Table.
	for (i = 0; i < tamanioArray; i++) {
		if (arregloElementos[index].keyClaves == keyClaves) {
			cout << "\n────────────────────────────────\n";
			cout << "| Key     |     Hash Value     |";
			cout << "\n────────────────────────────────";
			cout << "\n  " << arregloElementos[index].keyClaves << "        " << arregloElementos[index].dataRegistros;
			cout << "\n\n\t····· La clave [" << keyClaves << "] y su registro asociado han sido encontrados exitosamente en el arreglo. ·····\n";
			break;
		} else if (arregloElementos[index].keyClaves != keyClaves) {
			cout << "\n\t····· La clave [" << keyClaves << "] NO existe en el arreglo. Favor, intente de nuevo. ·····\n";
			break;
		}
	}
};

	// Método que borra las clave y su registro del arreglo
void Busqueda::borrarRegistros(int tamanioArray, int& keyClaves) {
	/** Función que elimina las claves y
	 * sus respectivos registros del arreglo */

		// Inicializar punteros, objetos y variables de manera local
	int index{dispersionHash(tamanioArray, keyClaves)};		// Asignamos el indice mediante la función hash por división (H(K) = (K mod N) + 1).

	if (arregloElementos[index].dataRegistros != 0) {
		arregloElementos[index].keyClaves = 0;
		arregloElementos[index].dataRegistros = 0;
			tamanioHash--;
		cout << "\n\t····· La clave [" << keyClaves << "] y su registro asociado han sido eliminados exitosamente del arreglo. ·····\n";
	} else {
		cout << "\n\t····· La clave [" << keyClaves << "] NO existe en el arreglo. Favor, intente de nuevo. ·····\n";
	}
};

	// Método que imprime el Hash Table en el arreglo
void Busqueda::imprimirHashTable(int tamanioArray, int& keyClaves) {
	/** Función que recorre el Hash Table
	 * e imprime los datos registrados */

		// Inicializar punteros, objetos y variables de manera local
	int i{0};			// Variable contador

	cout << "─────────────────────────────────────────────\n";
	cout << "| Index  |     Key     |     Hash Value     |";
	cout << "\n─────────────────────────────────────────────";
	while (i < tamanioArray) {
		if (arregloElementos[i].dataRegistros == 0) {
			cout << "\n " << i << "        " << "N/D" << "           " << "N/D";
		} else {
			cout << "\n " << i << "        " << arregloElementos[i].keyClaves << "             " << arregloElementos[i].dataRegistros;
		} i++;
	} tamanioHashTable();
};

		// Método que recupera el tamaño del HashTable
void Busqueda::tamanioHashTable() {
	/** Función que retorna el tamaño del HashTable */

	cout << "\n\n\n\t····· El tamaño de la Tabla de Dispersión (Hash Table) es: [" << tamanioHash << "]. ·····\n";
};


		//* FUNCIONES DE REASIGNACIÓN.

	//** PRUEBA LINEAL */
	// Métodos que implementa Reasignación Prueba Lineal (L)
void Busqueda::pruebaLineal(int tamanioArray, int& keyClaves){
	/** Este algoritmo busca al dato con clave K en el arreglo
	 * unidimensional V de N elementos. Resuelve el problema de
	 * las colisiones por medio del método de Prueba Lineal.
	 * index y aux son variables de tipo entero */

		// Inicializar punteros, objetos y variables de manera local
	int D, DX;

	D = dispersionHash(tamanioArray, keyClaves);	// Asignamos el valor D mediante la función hash por división (H(K) = (K mod N)).

	if ((arregloElementos[D].keyClaves != 0) && (arregloElementos[D].keyClaves == keyClaves)) {
		cout << "\n\t····· La información está en la posición: [" << D << "]. ·····\n";
	} else {
		DX = D + 1;

		while ((DX < tamanioArray) && (arregloElementos[DX].keyClaves != 0) && (arregloElementos[DX].keyClaves != keyClaves) && (DX != D)) {
			DX += 1;

			if (DX == tamanioArray) {
				DX = 0;
			}
		}

		if ((arregloElementos[DX].keyClaves == 0) || (DX == D)) {
			cout << "\n\t····· La información NO se encuentra en el arreglo. ·····\n";
		} else {
			cout << "\n\t····· La información está en la posición: [" << DX << "]. ·····\n";
		}
	}
};

void Busqueda::desplazamientoLineal(int tamanioArray, int& keyClaves) {
	/** Función asociada a la Prueba Lineal que determina
	 * las nuevas posiciones de los elementos */

		// Inicializar punteros, objetos y variables de manera local
	int D, DX;

	D = dispersionHash(tamanioArray, keyClaves);	// Asignamos el valor D mediante la función hash por división (H(K) = (K mod N)).

	if ((arregloElementos[D].keyClaves != 0) && (arregloElementos[D].keyClaves == keyClaves)) {
		cout << "\n\t····· La información está en la posición: [" << D << "]. ·····\n";
	} else {
		DX = D + 1;

		while ((DX < tamanioArray) && (arregloElementos[DX].keyClaves != 0) && (arregloElementos[DX].keyClaves != keyClaves) && (DX != D)) {
			DX += 1;

			if (DX == tamanioArray) {
				DX = 0;
			}
		}

		if ((arregloElementos[DX].keyClaves == 0)) {
			arregloElementos[DX].keyClaves = keyClaves;
			cout << "\n\t····· La información se ha redistribuido a la posición: [" << DX << "]. ·····\n";
				tamanioHash++;
		}
	} // imprimirHashTable(tamanioArray, DX);
};


	//** PRUEBA CUADRÁTICA */
	// Métodos que implementa Reasignación Prueba Cuadrática (C)
void Busqueda::pruebaCuadratica(int tamanioArray, int& keyClaves){
	/** Este algoritmo busca al dato con clave K en el arreglo
	 * unidimensional V de N elementos. Resuelve el problema
	 * de las colisiones por medio de la Prueba Cuadrática.
	 * D y aux son variables de tipo entero */

		// Inicializar punteros, objetos y variables de manera local
	int D, DX, I;

	D = dispersionHash(tamanioArray, keyClaves);

	if ((arregloElementos[D].keyClaves != 0) && (arregloElementos[D].keyClaves == keyClaves)) {
		cout << "\n\t····· La información está en la posición: [" << D << "]. ·····\n";
	} else {
		I = 0;
		DX = (D + (I * I));

		while ((arregloElementos[DX].keyClaves != 0) && (arregloElementos[DX].keyClaves != keyClaves)) {
			I += 1;
			DX = (D + (I * I));

			if (DX > tamanioArray) {
				I = -1;
				DX = 1;
				D = 0;
			}
		}
	}

	if (arregloElementos[DX].keyClaves == 0) {
		cout << "\n\t····· La información NO se encuentra en el arreglo. ·····\n";
	} else {
		cout << "\n\t····· La información está en la posición: [" << DX << "]. ·····\n";
	}
};

void Busqueda::desplazamientoCuadratica(int tamanioArray, int& keyClaves) {
	/** Función asociada a la Prueba Cuadrática que determina
	 * las nuevas posiciones de los elementos */

		// Inicializar punteros, objetos y variables de manera local
	int D, DX, I;

	D = dispersionHash(tamanioArray, keyClaves);

	if ((arregloElementos[D].keyClaves != 0) && (arregloElementos[D].keyClaves == keyClaves)) {
		cout << "\n\t····· La información está en la posición: [" << D << "]. ·····\n";
	} else {
		I = 0;
		DX = (D + (I * I));

		while ((arregloElementos[DX].keyClaves != 0) && (arregloElementos[DX].keyClaves != keyClaves)) {
			I += 1;
			DX = (D + (I * I));

			if (DX > tamanioArray) {
				I = -1;
				DX = 1;
				D = 0;
			}
		}
	}

	if (arregloElementos[DX].keyClaves == 0) {
		arregloElementos[DX].keyClaves = keyClaves;
		cout << "\n\t····· La información se ha redistribuido a la posición: [" << DX << "]. ·····\n";
			tamanioHash++;
	} else {
		cout << "\n\t····· La información está en la posición: [" << DX << "]. ·····\n";
	} // imprimirHashTable(tamanioArray, DX);
};


	//** DOBLE DIRECCIÓN */
	// Método que implementa Reasignación Doble Dirección Hash (D)
void Busqueda::dobleDireccion(int tamanioArray, int& keyClaves) {
	/** Este algoritmo busca al dato con clave K en el arreglo
	 * unidimensional V de N elementos. Resuelve el problema
	 * de las colisiones por medio de la Doble Dirección Hash.
	 * D y aux son variables de tipo entero */

		// Inicializar punteros, objetos y variables de manera local
	int D, DX;

	D = dispersionHash(tamanioArray, keyClaves);

	if ((arregloElementos[D].keyClaves != 0) && (arregloElementos[D].keyClaves == keyClaves)) {
		cout << "\n\t····· La información está en la posición: [" << D << "]. ·····\n";
	} else {
		DX = sgdaDispersion(tamanioArray, D);

		while ((DX < tamanioArray) && (arregloElementos[DX].keyClaves != 0) && (arregloElementos[DX].keyClaves != keyClaves) && (DX != D)) {
			DX = sgdaDispersion(tamanioArray, DX);

			if (DX >= tamanioArray) {
				DX = 0;
			}
		}

		if ((arregloElementos[DX].keyClaves == 0) || (arregloElementos[DX].keyClaves != keyClaves)) {
			cout << "\n\t····· La información NO se encuentra en el arreglo. ·····\n";
		} else {
			cout << "\n\t····· La información está en la posición: [" << DX << "]. ·····\n";
		}
	}
};

void Busqueda::desplazamientoDobleDireccion(int tamanioArray, int& keyClaves) {
	/** Función asociada a la Doble Dirección Hash que
	 * determina las nuevas posiciones de los elementos */

		// Inicializar punteros, objetos y variables de manera local
	int D, DX;

	D = dispersionHash(tamanioArray, keyClaves);

	if ((arregloElementos[D].keyClaves != 0) && (arregloElementos[D].keyClaves == keyClaves)) {
		cout << "\n\t····· La información está en la posición: [" << D << "]. ·····\n";
	} else {
		DX = sgdaDispersion(tamanioArray, D);

		while ((DX <= tamanioArray) && (arregloElementos[DX].keyClaves != 0) && (arregloElementos[DX].keyClaves != keyClaves) && (DX != D)) {
			DX = sgdaDispersion(tamanioArray, DX);

			if (DX >= tamanioArray) {
				DX = 0;
			}
		}

		if ((arregloElementos[DX].keyClaves == 0) || (arregloElementos[DX].keyClaves != keyClaves)) {
			arregloElementos[DX].keyClaves = keyClaves;

			if (DX != tamanioArray) {
				cout << "\n\t····· La información se ha redistribuido a la posición: [" << DX << "]. ·····\n";
					tamanioHash++;
			}
		} else {
			cout << "\n\t····· La información está en la posición: [" << DX << "]. ·····\n";
		} // imprimirHashTable(tamanioArray, DX);
	}
};


	//** ENCADENAMIENTO */
	// Métodos que implementa Encadenamiento (E)
void Busqueda::encadenamientoListas(int tamanioArray, int& keyClaves) {
	/** Este algoritmo busca al dato con clave K en el arreglo
	 * unidimensional V de N elementos. Resuelve las colisiones
	 * por medio de encadenamiento en listas ligadas.
	 * SIG e INFO son los campos de cada nodo de la lista. */

		// Inicializar punteros, objetos y variables de manera local
	int D;
	Nodo* Q, *V[tamanioArray];
	copiaArreglo(tamanioArray, V);

	D = dispersionHash(tamanioArray, keyClaves);

	if ((arregloElementos[D].keyClaves != 0) && (arregloElementos[D].keyClaves == keyClaves)) {
		cout << "\n\t····· La información está en la posición: [" << D << "]. ·····\n";
	} else {
		Q = V[D]->Liga;

		while ((Q != nullptr) && (Q->datoNumero == keyClaves)) {
			Q = Q->Liga;
		}

		if (Q == nullptr) {
			cout << "\n\t····· La información NO se encuentra en la lista. ·····\n";
		} else {
			cout << "\n\t····· La información se encuentra en la lista. ·····\n";
		}
	}
};

void Busqueda::copiaArreglo(int tamanioArray, Nodo* arrayV[]) {
	/** Función asociada al Encadenamiento que
	 * genera copias del arreglo original al Nodo */


	for (int i = 0; i < tamanioArray; i++) {
		arrayV[i]->datoNumero = 0;
		arrayV[i]->Liga = nullptr;
	}
};