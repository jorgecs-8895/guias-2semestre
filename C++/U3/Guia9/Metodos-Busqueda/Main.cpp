/*
 * Copyright (C) 2021 Jorge Carrillo Silva <jcarrillo20@alumnos.utalca.cl>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
*/


/******************** PROGRAMA.- ********************/

/*
 * file Main.cpp
 * g++ Main.cpp -o Busqueda
 * make Main.cpp
 *      Main.o
 *      Hash
*/

//* Main.cpp - Programa que utiliza la clase Busqueda.

// Llamar e incluir librerías necesarias
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include "Busqueda.cpp"
using namespace std;


//* Menú
void menuIngreso(string controlColisiones) {
	printf("\033c");

		// Inicializar punteros, objetos y variables de manera local
	Busqueda *apuntadorBusq = new Busqueda();	// Instanciador de métodos
	string valorOp{"\0"};						// Variables auxiliares de selección de opciones
	int integerOp{0};							// Variables auxiliares de selección de opciones
	string tamanioArraySTR{"\0"};				// Variable que contiene la cantidad de elementos del arreglo.
	int tamanioArray;							// Variable que contiene la cantidad de elementos del arreglo.
	int i{0};									// Variable(s) bandera(s)
	int keyClaves, dataRegistros;

		// Ingreso de tamaño del arreglo
	do {
		cout << "\t\t────────── MÉTODOS DE BÚSQUEDA: FUNCIONES HASH ────────── \n\n";
		cout << " » Ingrese tamaño N del arreglo: ";
		getline(cin, tamanioArraySTR);
		tamanioArray = stoi(tamanioArraySTR);
		if (tamanioArray <= 0) {
			printf("\033c");
			cout << "\t\t-------------------------------------------------- \n";
			cout << "\t\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO ***** \n";
			cout << "\t\t-------------------------------------------------- \n\n";
		} cout << "\n\n";
	} while (tamanioArray <= 0);

	apuntadorBusq->inicializarArreglo(tamanioArray);

		// Menú de opciones
	do {
		cout << "\t------------------------------------------------------------------ \n";
		cout << "\t***** MENÚ DE OPCIONES - MÉTODOS DE BÚSQUEDA: FUNCIONES HASH ***** \n";
		cout << "\t------------------------------------------------------------------ \n";
		cout << "\n  [1]  > Ingresar datos (Clave - Registro). \n";
		cout << "  [2]  > Buscar clave en el arreglo. \n";
		cout << "  [3]  > Borrar clave y registro asociado del arreglo. \n";
		cout << "  [4]  > Imprimir Hash Table. \n";
		cout << "  [0]  > Salir del programa. \n";
		cout << "\n - Favor, elija una opción: \n  » ";
		cin >> valorOp;
		integerOp = stoi(valorOp);
		cin.ignore();

		// Acciones de cada opción de menú
		if (integerOp == 1) {			//* Ingreso de valores
			printf("\033c");

			cout << "\t  ----- INGRESO DE DATOS -----\n" << endl;
			cout << " » Favor, ingrese clave a insertar: ";
			cin >> keyClaves;
			cout << " » Ingrese el registro: ";
			cin >> dataRegistros;
			apuntadorBusq->ingresoRegistros(controlColisiones, tamanioArray, keyClaves, dataRegistros);

			cout << "\n\n";
		} else if (integerOp == 2) {	//* Buscar elemento en el registro
			printf("\033c");

			cout << "\t  ----- BÚSQUEDA DE DATOS -----\n" << endl;
			cout << " » Favor, ingrese clave a buscar: ";
			cin >> keyClaves;
			apuntadorBusq->buscarRegistros(tamanioArray, keyClaves);

			cout << "\n\n";
		} else if (integerOp == 3) {	//* Borrar valores
			printf("\033c");

			cout << "\t  ----- ELIMINAR DATOS -----\n" << endl;
			cout << " » Favor, ingrese clave a eliminar: ";
			cin >> keyClaves;
			apuntadorBusq->borrarRegistros(tamanioArray, keyClaves);

			cout << "\n\n";
		} else if (integerOp == 4) {	//* Imprimir elementos del arreglo
			printf("\033c");

			cout << "\t  ----- HASH TABLE -----\n" << endl;
			apuntadorBusq->imprimirHashTable(tamanioArray, keyClaves);

			cout << "\n\n";
		} else if (integerOp == 0){		//* Salida del programa
			printf("\033c");

			cout << "\t\t---------------------------- \n";
			cout << "\t\t***** ¡SALIDA EXITOSA! ***** \n";
			cout << "\t\t---------------------------- \n\n";
			i++;

		} else {						//* Manejo de opciones inválidas
			printf("\033c");

			cout << "\t\t-------------------------------------------------- \n";
			cout << "\t\t***** VALOR INVÁLIDO, FAVOR INTENTE DE NUEVO ***** \n";
			cout << "\t\t-------------------------------------------------- \n\n";
		};
	} while (i < 1);

		// Se liberan los espacios de memoria previamente utilizados
	delete apuntadorBusq;
	apuntadorBusq = nullptr;
};

//* Main
int main(int argc, char *argv[]) {
	printf("\033c");

	if (argc != 2) {	// Si el programa no recibe parámetros válidos (el nombre de archivo + ítem que determina elección de métodos de resolución de Colisiones), entonces:
		cout << "\t ---------------------------------------------------------------------------- \n";
		cout << "\t ***** PARÁMETROS INSUFICIENTES Y/O INVÁLIDOS. FAVOR INTENTE NUEVAMENTE ***** \n";
		cout << "\t ---------------------------------------------------------------------------- \n\n";
		exit(1);
	} else {		// De lo contrario, si se recibe la cantidad de parámetros válidos, el segundo se asume como la cantidad de nodos y se inserta en el programa automáticamente
			// Inicializar punteros, objetos y variables de manera local
		string controlColisiones{argv[1]};		// Guarda el valor del 2do parámetro (Elegir método de rosolución de Colisiones)

		if ((controlColisiones != "L" && controlColisiones != "l") && (controlColisiones != "C" && controlColisiones != "c")
			&& (controlColisiones != "D" && controlColisiones != "d") && (controlColisiones != "E" && controlColisiones != "e")) {
			cout << "\t ---------------------------------------------------------------------------------------------- \n";
			cout << "\t ***** DEBE INGRESAR UN CÁRACTER VÁLIDO ('L' - 'C' - 'D' - 'E'), FAVOR INTENTE NUEVAMENTE ***** \n";
			cout << "\t ---------------------------------------------------------------------------------------------- \n\n";
			exit(1);

		} else {					// De lo contrario, se inicializa el programa
			menuIngreso(controlColisiones);
		}
	}

	return 0;
};